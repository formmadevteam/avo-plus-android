/**
 * Automatically generated file. DO NOT MODIFY
 */
package calendarmanager.activities;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "calendarmanager.activities";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 3;
  public static final String VERSION_NAME = "";
}
