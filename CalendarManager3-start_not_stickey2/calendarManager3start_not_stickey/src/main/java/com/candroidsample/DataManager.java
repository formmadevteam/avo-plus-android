package com.candroidsample;

import hirondelle.date4j.DateTime;

import java.util.Calendar;

public class DataManager {
	public boolean[][][] listaCuDate;
	public final int yearOffset = 2000;

	private static DataManager instance = null;

	private DataManager() {
		listaCuDate = new boolean[50][13][32];
	}

	public static synchronized DataManager getInstance() {
		if (instance == null) {
			instance = new DataManager();
		}
		return instance;
	}

	public void add(Calendar cal) {
		instance.listaCuDate[cal.get(Calendar.YEAR) - yearOffset][cal
				.get(Calendar.MONTH) + 1][cal.get(Calendar.DAY_OF_MONTH)] = true;
	}

	public boolean isEvent(Calendar cal) {
		return instance.listaCuDate[cal.get(Calendar.YEAR) - yearOffset][cal
				.get(Calendar.MONTH)][cal.get(Calendar.DAY_OF_MONTH)];
	}

	public boolean isEvent(DateTime dt) {
		return instance.listaCuDate[dt.getYear() - yearOffset][dt.getMonth()][dt
				.getDay()];
	}

}
