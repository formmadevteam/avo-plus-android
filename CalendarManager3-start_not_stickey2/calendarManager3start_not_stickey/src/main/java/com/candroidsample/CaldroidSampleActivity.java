package com.candroidsample;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import calendaradapter.structures.SedintaCurenta;
import calendarmanager.activities.ListaDosareActivity;
import calendarmanager.activities.MainActivity;
import calendarmanager.activities.R;
import calendarmanager.helpers.HelperSqlFunctions;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

@SuppressLint("SimpleDateFormat")
public class CaldroidSampleActivity extends FragmentActivity {
	private boolean undo = false;
	private CaldroidFragment caldroidFragment;
	private CaldroidFragment dialogCaldroidFragment;
	public HashMap<String, Object> hasMap = new HashMap<String, Object>();

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.e("er", "Bum");
		return super.onOptionsItemSelected(item);
	}

	public void selectThisMonth() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		Date firstDate = cal.getTime();
		cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH,
				cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date maxDate = cal.getTime();
		caldroidFragment.setMinDate(firstDate);
		caldroidFragment.setMaxDate(maxDate);

	}

	private ProgressDialog dialog2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		setContentView(R.layout.activity_main2);
		setTitle("");

		dialog2 = new ProgressDialog(CaldroidSampleActivity.this);

		TextView tv = (TextView) findViewById(R.id.title_list_dos);
		TextView tv1 = (TextView) findViewById(R.id.info_gen_licenta);

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");

		tv.setTypeface(tf);
		tv1.setTypeface(tf);

		ImageView imm = (ImageView) findViewById(R.id.back_dosar_pic);
		imm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		// ActionBar actionBar = getActionBar();
		// actionBar.setDisplayHomeAsUpEnabled(true);
		//
		// actionBar.setIcon(R.drawable.stanga_data);
		//
		// actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		// actionBar.setDisplayShowHomeEnabled(true);

		final SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
		TaskGetDosaree ta = new TaskGetDosaree();
		ta.execute();
		// Setup caldroid fragmentTaskGetDosaree
		// **** If you want normal CaldroidFragment, use below line ****
		// caldroidFragment = new CaldroidFragment();

		// //////////////////////////////////////////////////////////////////////
		// **** This is to show customized fragment. If you want customized
		// version, uncomment below line ****
		caldroidFragment = new CaldroidSampleCustomFragment(getAssets());

		// Setup arguments

		// If Activity is created after rotation
		// if (savedInstanceState != null) {
		// caldroidFragment.restoreStatesFromKey(savedInstanceState,
		// "CALDROID_SAVED_STATE");
		// }
		// // If activity is created from fresh
		// else {
		Bundle args = new Bundle();
		Calendar cal = Calendar.getInstance();
		args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
		args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
		args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
		args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, false);
		args.putInt(CaldroidFragment.START_DAY_OF_WEEK, CaldroidFragment.MONDAY);
		args.putBoolean(CaldroidFragment.SHOW_NAVIGATION_ARROWS, false);
		args.putBoolean(CaldroidFragment.ENABLE_CLICK_ON_DISABLED_DATES, false);
		caldroidFragment.setArguments(args);
		// }
		caldroidFragment.setCaldroidListener(listener);

		// setCustomResourceForDates();

		// Attach to the activity

		// Setup listener

	}

	final CaldroidListener listener = new CaldroidListener() {

		@Override
		public void onSelectDate(Date date, View view) {

			// Toast.makeText(getApplicationContext(),
			// formatter.format(date),
			// Toast.LENGTH_SHORT).show();

			Calendar currentDate = Calendar.getInstance();
			currentDate = Calendar.getInstance();
			currentDate.setTime(date);
			currentDate.set(Calendar.HOUR_OF_DAY,
					currentDate.getActualMinimum(Calendar.HOUR_OF_DAY));
			currentDate.set(Calendar.MINUTE,
					currentDate.getActualMinimum(Calendar.MINUTE));
			currentDate.set(Calendar.SECOND,
					currentDate.getActualMinimum(Calendar.SECOND));
			currentDate.set(Calendar.MILLISECOND,
					currentDate.getActualMinimum(Calendar.MILLISECOND));

			MainActivity.currentDate = currentDate;
			finish();

		}

		@Override
		public void onChangeMonth(int month, int year) {
			// String text = "month: " + month + " year: " + year;
			// Toast.makeText(getApplicationContext(), text,
			// Toast.LENGTH_SHORT).show();

			System.out.println("month: " + month + "year: " + year);

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, month - 1);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.set(Calendar.YEAR, year);

			Date firstDate = cal.getTime();
			cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, month - 1);
			cal.set(Calendar.DAY_OF_MONTH,
					cal.getActualMaximum(Calendar.DAY_OF_MONTH));
			Date maxDate = cal.getTime();
			caldroidFragment.setMinDate(firstDate);
			caldroidFragment.setMaxDate(maxDate);
//
//			 TaskGetDosaree t = new TaskGetDosaree();
//			 t.execute();

		}

		@Override
		public void onLongClickDate(Date date, View view) {
			// Toast.makeText(getApplicationContext(),
			// "Long click " + formatter.format(date),
			// Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onCaldroidViewCreated() {
			if (caldroidFragment.getLeftArrowButton() != null) {
				// Toast.makeText(getApplicationContext(),
				// "Caldroid view is created", Toast.LENGTH_SHORT)
				// .show();
			}
		}

	};

	// Setup Caldroid

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		final SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
		TaskGetDosaree ta = new TaskGetDosaree();
		ta.execute();

		caldroidFragment = new CaldroidSampleCustomFragment(getAssets());

		Bundle args = new Bundle();
		Calendar cal = Calendar.getInstance();
		args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
		args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
		args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
		args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, false);
		args.putInt(CaldroidFragment.START_DAY_OF_WEEK, CaldroidFragment.MONDAY);
		args.putBoolean(CaldroidFragment.SHOW_NAVIGATION_ARROWS, false);
		args.putBoolean(CaldroidFragment.ENABLE_CLICK_ON_DISABLED_DATES, false);
		
		caldroidFragment.setArguments(args);

		caldroidFragment.setCaldroidListener(listener);

	}

	/**
	 * Save current states of the Caldroid here
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);

		if (caldroidFragment != null) {
			caldroidFragment.saveStatesToKey(outState, "CALDROID_SAVED_STATE");
		}

		if (dialogCaldroidFragment != null) {
			dialogCaldroidFragment.saveStatesToKey(outState,
					"DIALOG_CALDROID_SAVED_STATE");
		}
	}

	private class TaskGetDosaree extends AsyncTask<Void, Void, Void> {
		ArrayList<SedintaCurenta> liss;

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			liss = new ArrayList<SedintaCurenta>();


			HelperSqlFunctions h = new HelperSqlFunctions(
					getApplicationContext());
			Calendar cal = Calendar.getInstance();
			ArrayList<Calendar> lista = h.getSedinteByDateWithoutAmanate(cal);

			DataManager dm = DataManager.getInstance();
			int i = 0;
			for (Calendar cale : lista) {
				dm.add(cale);
			}
			hasMap.put("ready", true);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					
					caldroidFragment.setExtraData(hasMap);
					selectThisMonth();
					caldroidFragment.refreshView();

					FragmentTransaction t = getSupportFragmentManager()
							.beginTransaction();
					try{
						t.replace(R.id.calendar1, caldroidFragment);
						t.commit();
					}catch(Exception e){
						e.printStackTrace();
					}
					
					Log.e("debug", "m-am comis!");
					
					dialog2.dismiss();
				}
			});

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					dialog2.setMessage("Se incarca datele...");
					dialog2.setCancelable(false);
					dialog2.show();
				}
			});

		}
	}
	
	
	
	
	
	
	
	
	
	
	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	private class TaskGetDosaree extends AsyncTask<Void, Void, Void> {
//		ArrayList<SedintaCurenta> liss;
//
//		@Override
//		protected Void doInBackground(Void... params) {
//			// TODO Auto-generated method stub
//
//			liss = new ArrayList<SedintaCurenta>();
//
//			// Log.d("debug",
//			// "Data curenta:" + currentDate.get(Calendar.DAY_OF_MONTH)
//			// + "." + (currentDate.get(Calendar.MONTH) + 1) + "."
//			// + currentDate.get(Calendar.YEAR));
//
//			HelperSqlFunctions h = new HelperSqlFunctions(
//					getApplicationContext());
//			Calendar cal = Calendar.getInstance();
////			ArrayList<Calendar> lista = h.getSedinteByDateWithoutAmanate(cal);
//			ArrayList<Calendar> lista = h.getSedinteByDate(cal);
//			
////			System.out.println("Lista::::: " + lista.size());
//			DataManager dm = DataManager.getInstance();
//			for (Calendar cale : lista) {
//				dm.add(cale);
//			}
//			hasMap.put("ready", true);
//
//			return null;
//		}
//
//		@Override
//		protected void onPostExecute(Void result) {
//			// TODO Auto-generated method stub
//			super.onPostExecute(result);
//			// Log.e("debug", "Size: " + liss.size());
//			caldroidFragment.setExtraData(hasMap);
//			selectThisMonth();
//			caldroidFragment.refreshView();
//			FragmentTransaction t = getSupportFragmentManager()
//					.beginTransaction();
//			t.replace(R.id.calendar1, caldroidFragment);
//			t.commit();
//		}
//
//		@Override
//		protected void onPreExecute() {
//			// TODO Auto-generated method stub
//			super.onPreExecute();
//
//		}
//	}
//	
//	
//	
//	
//	
//	
//	
//	
//	
//
//	
//	
//	private class TaskGetDosaree extends AsyncTask<Void, Void, Void> {
//		ArrayList<SedintaCurenta> liss;
//
//		@Override
//		protected Void doInBackground(Void... params) {
//			// TODO Auto-generated method stub
//
//			liss = new ArrayList<SedintaCurenta>();
//
//			// Log.d("debug",
//			// "Data curenta:" + currentDate.get(Calendar.DAY_OF_MONTH)
//			// + "." + (currentDate.get(Calendar.MONTH) + 1) + "."
//			// + currentDate.get(Calendar.YEAR));
//
//			HelperSqlFunctions h = new HelperSqlFunctions(
//					getApplicationContext());
//			Calendar cal = Calendar.getInstance();
//			ArrayList<Calendar> lista = h.getSedinteByDate(cal);
////			System.out.println("Lista::::: " + lista.size());
//			DataManager dm = DataManager.getInstance();
//			for (Calendar cale : lista) {
//				dm.add(cale);
//			}
//			hasMap.put("ready", true);
//			
//			return null;
//		}
//
//		@Override
//		protected void onPostExecute(Void result) {
//			// TODO Auto-generated method stub
//			super.onPostExecute(result);
//			// Log.e("debug", "Size: " + liss.size());
//			caldroidFragment.setExtraData(hasMap);
//			selectThisMonth();
//			caldroidFragment.refreshView();
//			FragmentTransaction t = getSupportFragmentManager()
//					.beginTransaction();
//			t.replace(R.id.calendar1, caldroidFragment);
//			t.commit();
//			
//		}
//
//		@Override
//		protected void onPreExecute() {
//			// TODO Auto-generated method stub
//			super.onPreExecute();
//
//		}
//	}
//	
//	
//	
//	
//	
//	
	
}
