package com.candroidsample;

import hirondelle.date4j.DateTime;

import java.util.HashMap;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import calendarmanager.activities.R;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidGridAdapter;

public class CaldroidSampleCustomAdapter extends CaldroidGridAdapter {

	Typeface tf;
	HashMap<String, Object> exData = new HashMap<String, Object>();
	public DataManager dataManager = DataManager.getInstance();
	

	public CaldroidSampleCustomAdapter(Context context, int month, int year,
			HashMap<String, Object> caldroidData,
			HashMap<String, Object> extraData, AssetManager asset) {
		super(context, month, year, caldroidData, extraData);
		tf = Typeface.createFromAsset(asset, "fonts/OpenSans-Light.ttf");

		exData = extraData;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View cellView = convertView;

		// For reuse
		if (convertView == null) {
			cellView = inflater.inflate(R.layout.custom_cell, null);
		}

		int topPadding = cellView.getPaddingTop();
		int leftPadding = cellView.getPaddingLeft();
		int bottomPadding = cellView.getPaddingBottom();
		int rightPadding = cellView.getPaddingRight();

		TextView tv1 = (TextView) cellView.findViewById(R.id.tvDate);
		ImageView bulinutaBlue = (ImageView) cellView
				.findViewById(R.id.bulinutaAlbastra);
		ImageView bulinutaRed = (ImageView) cellView
				.findViewById(R.id.bulinutaRosie);
		ImageView bulinutaOrange = (ImageView) cellView
				.findViewById(R.id.bulinutaPortocalie);

		tv1.setTextColor(Color.BLACK);
		tv1.setTypeface(tf);
		// Get dateTime of this cell
		DateTime dateTime = this.datetimeList.get(position);
		Resources resources = context.getResources();
		// Set color of the dates in previous / next month

		boolean shouldResetDiabledView = false;
		boolean shouldResetSelectedView = false;

		// Customize for disabled dates and date outside min/max dates
		if ((minDateTime != null && dateTime.lt(minDateTime))
				|| (maxDateTime != null && dateTime.gt(maxDateTime))
				|| (disableDates != null && disableDates.indexOf(dateTime) != -1)) {

			// tv1.setTextColor(CaldroidFragment.disabledTextColor);
			tv1.setTextColor(resources.getColor(R.color.uiCalendarDisabledText));
			cellView.setBackgroundColor(resources
					.getColor(R.color.uiCalendarDisabledBackground));

			if (dateTime.equals(getToday())) {
				cellView.setBackgroundColor(resources.getColor(R.color.uiCalendarTodayBackground));
			}

		} else {
			shouldResetDiabledView = true;
		}

		// Customize for selected dates
		if (selectedDates != null && selectedDates.indexOf(dateTime) != -1) {
			if (CaldroidFragment.selectedBackgroundDrawable != -1) {
				cellView.setBackgroundResource(CaldroidFragment.selectedBackgroundDrawable);
			} else {
				cellView.setBackgroundColor(resources.getColor(R.color.blue));

			}

			tv1.setTextColor(CaldroidFragment.selectedTextColor);

		} else {
			shouldResetSelectedView = true;
		}

		if (shouldResetDiabledView && shouldResetSelectedView) {
			tv1.setTextColor(resources.getColor(R.color.uiCalendarActiveText));
			// Customize for today
			cellView.setBackgroundColor(resources
					.getColor(R.color.uiCalendarActiveBackground));
			if(dateTime.equals(getToday())) {
				cellView.setBackgroundColor(resources.getColor(R.color.uiCalendarTodayBackground));
			}
		}

		tv1.setText("" + dateTime.getDay());
//		Log.e("adapter", "Data pentru bulina: "+dateTime.getDay() + "." + dateTime.getMonth() + " ");
//		
//		if(dateTime.getMonth()==7) {
//			bulinutaBlue.setVisibility(View.VISIBLE);
//		}
		
		if(dataManager.isEvent(dateTime)) {
			Log.e("adapter", "Data pentru bulina: "+dateTime.getDay() + "." + dateTime.getMonth() + " ");
			Log.e("debug", "Aicicici intrule");
			bulinutaBlue.setVisibility(View.VISIBLE);
		} 
		

		cellView.setPadding(leftPadding, topPadding, rightPadding,
				bottomPadding);

		// Set custom color if required
		// setCustomResources(dateTime, cellView, tv1);

		return cellView;
	}

}
