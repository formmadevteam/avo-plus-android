package com.candroidsample;

import android.content.res.AssetManager;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidGridAdapter;


public class CaldroidSampleCustomFragment extends CaldroidFragment {
	
    AssetManager asset;
    
	public CaldroidSampleCustomFragment(AssetManager ass) {
		asset = ass;
	}
	@Override
	public CaldroidGridAdapter getNewDatesGridAdapter(int month, int year) {
		// TODO Auto-generated method stub
		return new CaldroidSampleCustomAdapter(getActivity(), month, year,
				getCaldroidData(), extraData,asset);
	}

}
