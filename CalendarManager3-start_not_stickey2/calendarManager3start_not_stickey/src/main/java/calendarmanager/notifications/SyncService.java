package calendarmanager.notifications;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

public class SyncService extends Service{

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onCreate() {
		UberNotificationManager uberNM = UberNotificationManager.getInstance(getApplicationContext());
		uberNM.notifySyncNotification(getApplicationContext());
		uberNM.finishSyncNotification(getApplicationContext());
		super.onCreate();
	}

}
