package calendarmanager.notifications;

import calendarmanager.helpers.GenericHelpers;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore.Audio.Genres;
import android.widget.Toast;

public class NotificationReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		try {
		     Bundle bundle = intent.getExtras();
		     String message = bundle.getString("EXTRA_MESSAGE");
		     int flags = bundle.getInt("EXTRA_FLAGS");
		     int time = bundle.getInt("EXTRA_TIME");
		     UberNotificationManager unm = UberNotificationManager.getInstance(context);
		     UberNotification un = new UberNotification(context, "Avocatii Event", message + " f"+flags, false);
		     if(time==10) {
		    	 un.dnotify.setContentText(message + " f"+flags + " t10min");
		     }
		     if(time==60) {
		    	 un.dnotify.setContentText(message + " f"+flags + " t1hour");
		     }
		     unm.notify(un);
	    }catch (Exception e) {
		     Toast.makeText(context, "There was an error somewhere, but we still received an alarm", Toast.LENGTH_LONG).show();
		     e.printStackTrace();
		 
		    }
		
	}

}
