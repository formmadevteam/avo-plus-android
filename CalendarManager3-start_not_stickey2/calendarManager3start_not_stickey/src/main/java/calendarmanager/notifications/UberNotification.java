package calendarmanager.notifications;

import java.util.Calendar;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import calendarmanager.activities.DosarActivity;
import calendarmanager.activities.MainActivity;
import calendarmanager.activities.R;

public class UberNotification {
	public int notificationNumber;
	public int updateableNumber = 1;
	public NotificationCompat.Builder dnotify;
	public UberNotification(Context context, String title, String text,boolean mainSure) {
		dnotify = new NotificationCompat.Builder(context);
		dnotify.setSmallIcon(R.drawable.ic_launcher);
		dnotify.setContentText(text);
		dnotify.setContentTitle(title);
		if(mainSure==false) {
			UberNotificationManager unm = UberNotificationManager.getInstance(context);
			notificationNumber = unm.getNewNotificationNumber();
		}else {
			notificationNumber = 80084;
		}
		
	}
	
	
	public void setIntentNotif(Context context, int id) {
		System.out.println("ID: "+id);
		
		dnotify = new NotificationCompat.Builder(context);
		dnotify.setSmallIcon(R.drawable.ic_launcher);
		Intent intent = new Intent().setClass(context, DosarActivity.class);
		intent.putExtra("id_dos", id);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
//		PendingIntent pi = PendingIntent.getActivity(context, 0, intent, PendingIntent.CREATOR);
		dnotify.setContentIntent(PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT));
//		pi = null;
	}
	
	public void setIntent(Context context, Intent intent) {
		PendingIntent pi = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		dnotify.setContentIntent(pi);
	}
	public void setIntentForTodayScreen(Context context) {
		Intent intent = new Intent(context, MainActivity.class);
		intent.putExtra(MainActivity.EXTRAS_TODAY, true);
		PendingIntent pi = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		dnotify.setContentIntent(pi); 
	}
	public void setIntentForXDayScreen(Context context,Calendar cal) {
		Intent intent = new Intent(context, MainActivity.class);
		intent.putExtra(MainActivity.EXTRAS_TODAY, false);
		intent.putExtra(MainActivity.EXTRAS_MILLIS, cal.getTimeInMillis());
		PendingIntent pi = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		dnotify.setContentIntent(pi);
	}
	public void addAction(Context context,Intent intent, int icon, String text) {
		dnotify.addAction(icon, text, PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));
	}
	
	
	
}
