package calendarmanager.notifications;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import calendarmanager.activities.R;
import calendarmanager.helpers.GenericHelpers;
import calendarmanager.helpers.UpdateService;

public class UberNotificationManager {
	private static UberNotificationManager instance = null;

	public static int notificationSyncId = 80083;
	public static int norificationFSycId = 80084;
	public static int notificationMainId = 80085;
	public static int notificationBaseNumber = 80086;
	public static int notificationNewNumber = 80087;
	public static int notificationCount = 0;

	public static final int FLAG_1HOUR = 1;
	public static final int FLAG_10MIN = 2;
	public static final int FLAG_OTHER_CITY = 4;
	public UberNotification mainNotification;
	public UberNotification syncNotification;
	public UberNotification finishedSyncNotification;
	// public UberNotification newSyncNotification;

	// public SyncService syncService;
	public NotificationManager dNotificationManager;

	private Context context;

	private UberNotificationManager(Context context) {
		dNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
//
//		mainNotification = new UberNotification(context, "Avocatii Title",
//				"Avocatii nu au nevoie de text", true);
//		mainNotification.dnotify
//				.setContentText(mainNotification.notificationNumber
//						+ " Notification ID MAIN");
//		mainNotification.notificationNumber = notificationMainId;
//		mainNotification.dnotify.setPriority(Notification.PRIORITY_HIGH);
//		mainNotification.dnotify.setAutoCancel(false);
//		mainNotification.dnotify.setOngoing(true);
//		mainNotification.setIntentForTodayScreen(context);
//		mainNotification.dnotify.addAction(R.drawable.ic_action_refresh,
//				"Sync", PendingIntent.getService(context, 0, new Intent(
//						context, UpdateService.class),
//						PendingIntent.FLAG_UPDATE_CURRENT));
//
//		// mainNotification.dnotify.setStyle(new
//		// NotificationCompat..setSummaryText("+3 more"));
//
//		syncNotification = new UberNotification(context,
//				"Se sincronizeaza cu portalul", "", true);
//		syncNotification.dnotify.setContentText("portal.just.ro");
//		syncNotification.notificationNumber = notificationSyncId;
//		syncNotification.dnotify.setSmallIcon(R.drawable.ic_action_refresh);
//		syncNotification.dnotify.setPriority(Notification.PRIORITY_HIGH);
//		syncNotification.dnotify.setAutoCancel(false);
//		syncNotification.dnotify.setProgress(0, 0, true);

//		finishedSyncNotification = new UberNotification(context,
//				"Finished Sync", "Avocatii nu au nevoie de text", true);
//		finishedSyncNotification.notificationNumber = norificationFSycId;
//		finishedSyncNotification.dnotify
//				.setSmallIcon(R.drawable.ic_action_refresh);
//		finishedSyncNotification.dnotify
//				.setPriority(Notification.PRIORITY_DEFAULT);
//		finishedSyncNotification.dnotify.setAutoCancel(true);

		// finishedSyncNotification.setIntentForTodayScreen(context);

		this.context = context;

	}

	public static synchronized UberNotificationManager getInstance(
			Context context) {
		if (instance == null) {
			instance = new UberNotificationManager(context);
		}
		return instance;
	}

	public int getNewNotificationNumber() {
		notificationCount++;
		return notificationBaseNumber + notificationCount;
	}

	/*--- Main Notificaiton Functions ---*/
	public void notifyMain() {
		dNotificationManager.notify(notificationMainId,
				mainNotification.dnotify.build());
	}

	public void updateMainWithNumber(int x) {
		mainNotification.dnotify.setNumber(x);
	}

	public void updateMainWithNextNumber() {
		mainNotification.updateableNumber++;
		mainNotification.dnotify.setNumber(mainNotification.updateableNumber);
	}

	public void cancelMainNotification() {
		dNotificationManager.cancel(notificationMainId);
	}

	/*--- Sync Notification Functions ---*/
	public void startSync(Context context) {

		// Intent il = new Intent(context, UpdateService.class);
		context.startService(new Intent(context, UpdateService.class));
		// context.startService(new Intent(context, SyncService.class));

		// context.startService(il);

	}

	public void notifySyncNotification(Context context) {
		dNotificationManager.notify(notificationSyncId,
				syncNotification.dnotify.build());
	}

	public void newSyncNotification(String numar, int id) {
//		DosarActivity.id = id;
		
		UberNotification newSyncNotification = new UberNotification(context, "A aparut o modificare",
				"Update la dosarul: #" + numar, true);
		newSyncNotification.notificationNumber = id;
		newSyncNotification.dnotify.setSmallIcon(R.drawable.ic_action_refresh);
		newSyncNotification.dnotify.setPriority(Notification.PRIORITY_DEFAULT);
		newSyncNotification.dnotify.setAutoCancel(true);
		newSyncNotification.setIntentNotif(context, id);
		
//		newSyncNotification.dnotify.addAction(R.drawable.ic_action_refresh,
//				"Vizualizare dosar", PendingIntent.getActivity(context, 0, new Intent(
//						context, DosarActivity.class),
//						PendingIntent.FLAG_UPDATE_CURRENT));
		
//		cancelSyncNotification();
		notifyId(newSyncNotification, id);
//		context.stopService(new Intent(context, UpdateService.class));
	}

	public void finishSyncNotification(Context context) {
		cancelSyncNotification();
		notify(finishedSyncNotification);
		// context.stopService(new Intent(context, SyncService.class));
	}

	public void cancelSyncNotification() {
		dNotificationManager.cancel(notificationSyncId);
	}

	/*--- Time based Notification Functions---*/
	public void showToastAt(Context context, Calendar cal, String text) {
		Intent intent = new Intent(context, NotificationReceiver.class);
		intent.putExtra("EXTRA_MESSAGE", text);
		PendingIntent sender = PendingIntent.getBroadcast(context, 118932,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);

		AlarmManager am = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), sender);
	}

	public void showTimedNotification(Context context, Calendar time,
			int idDosar, int flags) {
		Intent intentMin = new Intent(context, NotificationReceiver.class);
		intentMin.putExtra("EXTRA_MESSAGE", "Event " + idDosar);
		intentMin.putExtra("EXTRA_FLAGS", flags);
		intentMin.putExtra("EXTRA_TIME", 10);

		Intent intentHour = new Intent(context, NotificationReceiver.class);
		intentHour.putExtra("EXTRA_MESSAGE", "Event " + idDosar);
		intentHour.putExtra("EXTRA_FLAGS", flags);
		intentHour.putExtra("EXTRA_TIME", 60);

		PendingIntent senderMin = PendingIntent.getBroadcast(context, 118932,
				intentMin, PendingIntent.FLAG_UPDATE_CURRENT);
		PendingIntent senderHour = PendingIntent.getBroadcast(context, 118933,
				intentHour, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager am = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);

		if (GenericHelpers.bitWiseSet(flags, FLAG_10MIN)) {
			time.add(Calendar.MINUTE, -10);
			// am.setExact(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(),
			// senderMin);
			time.add(Calendar.MINUTE, 10);
		}
		if (GenericHelpers.bitWiseSet(flags, FLAG_1HOUR)) {
			time.add(Calendar.HOUR, -1);
			// am.setExact(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(),
			// senderHour);
			time.add(Calendar.HOUR, 1);
		}
	}

	/*--- Generic Notification Functions ---*/
	public void cancel(int notificationNumber) {
		dNotificationManager.cancel(notificationNumber);
	}

	public void cancel(UberNotification un) {
		dNotificationManager.cancel(un.notificationNumber);
	}

	public void cancelAllNotifications() {
		dNotificationManager.cancelAll();
	}

	public void notify(UberNotification un) {
		dNotificationManager.notify(un.notificationNumber, un.dnotify.build());
	}
	public void notifyId(UberNotification un, int id) {
		dNotificationManager.notify(id, un.dnotify.build());
	}
}
