package calendarmanager.helpers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import calendaradapter.structures.DosarObservatii;
import calendaradapter.structures.DosarSimplif;
import calendaradapter.structures.SedintaCurenta;
import ecris.data.types.Dosar;
import ecris.data.types.DosarCaleAtac;
import ecris.data.types.DosarParte;
import ecris.data.types.DosarSedinta;

public class HelperSqlFunctions {

	Context context;

	public HelperSqlFunctions(Context context) {
		this.context = context;
	}

	public void clearDatabase() {
		context.deleteDatabase("CalendarManagerTesta.db");
	}

	public int dosarExists(String numar){
		int ok=1;
		int i=0;
		
		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getWritableDatabase();

		
		String[] COLUMNS = { "id" };

		Cursor cursor = sqlTable.query("dosar", COLUMNS, "numar='"+numar+"'", null, null,
				null, null);
		// cursor.moveToFirst();

		if (cursor != null) {
			while (cursor.moveToNext()) {
				i++;	
			}
		}

		cursor.close();
		sqlTable.close();
		sqlHelper.close();
		
		
		if(i!=0)
			ok=0;
		return ok;
		
	}
	
	public String convertDateToString(Calendar cal) {
		if (cal != null)
			return String.valueOf(cal.get(Calendar.YEAR) + "."
					+ (cal.get(Calendar.MONTH) + 1) + "."
					+ cal.get(Calendar.DAY_OF_MONTH));
		else
			return "";
	}

	public Calendar convertStringToDate(String cal) {
		Calendar cale = Calendar.getInstance();

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
			cale.setTime(sdf.parse(cal));
		} catch (Exception e) {
		}

		return cale;
	}
	
	public void deleteDatabase(){
		context.deleteDatabase("CalendarManagsereabutaoana.db");		
	}

	public void deleteAll(int id) {
		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getWritableDatabase();

		ContentValues val = new ContentValues();
		val.put("obiect", "Un obiect aiurea");
		val.put("nume", "Bula");
		val.put("observatii", "OB pro");

		sqlTable.update("dosar", val, "id='" + id + "'", null);

		sqlTable.close();
		sqlHelper.close();

	}

	public void deleteDosar(int id) {
		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();
		sqlTable.delete("dosar", "id=" + id, null);
		sqlTable.delete("dosarparte", "id_dosar=" + id, null);
		sqlTable.delete("dosarsedinta", "id_dosar=" + id, null);
		sqlTable.delete("dosarcaleatac", "id_dosar=" + id, null);

		sqlTable.close();
		sqlHelper.close();
	}

	public int insertDosar(Dosar dosar) {
		int id;
		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();

		 Log.e("debug", "Data modificare: "+dosar.getDataModificare().toString());

		ContentValues val = new ContentValues();
		val.put("numar", dosar.getNumar() + "");
		val.put("numarVechi", dosar.getNumarVechi() + "");
		val.put("nume", "");
		val.put("observatii", "");

		
		val.put("data", dosar.getData().get(Calendar.YEAR) + "."
				+ (dosar.getData().get(Calendar.MONTH) + 1) + "."
				+ dosar.getData().get(Calendar.DAY_OF_MONTH));
		val.put("institutie", dosar.getInstitutie() + "");
		val.put("departament", dosar.getDepartament() + "");
		val.put("categorieCaz", dosar.getCategorieCaz() + "");
		val.put("stadiuProcesual", dosar.getStadiuProcesual() + "");
		val.put("obiect", dosar.getObiect() + "");
		val.put("dataModificare",
				convertDateToString(dosar.getDataModificare()));
		sqlTable.insertOrThrow("dosar", null, val);

		String[] COLUMNS = { "id" };

		Cursor cursor = sqlTable.query("dosar", COLUMNS, null, null, null,
				null, null);

		if (cursor != null && cursor.moveToLast()) {
			id = Integer.valueOf(cursor.getString(0));
		} else {
			return -1;
		}
		if (dosar.getParti() != null) {
			for (int i = 0; i < dosar.getParti().size(); i++) {
				ContentValues val2 = new ContentValues();
				val2.put("id_dosar", id);
				val2.put("nume", dosar.getParti().get(i).getNume());
				val2.put("calitateParte", dosar.getParti().get(i)
						.getCalitateParte());
				sqlTable.insertOrThrow("dosarparte", null, val2);
			}
		}
		if (dosar.getSedinte() != null) {
			for (int i = 0; i < dosar.getSedinte().size(); i++) {
				ContentValues val3 = new ContentValues();
				val3.put("id_dosar", id);
				val3.put("complet", dosar.getSedinte().get(i).getComplet());
				val3.put("data", convertDateToString(dosar.getSedinte().get(i)
						.getData()));
				val3.put("ora", dosar.getSedinte().get(i).getOra());
				val3.put("solutie", dosar.getSedinte().get(i).getSolutie());
				val3.put("solutieSumar", dosar.getSedinte().get(i)
						.getSolutieSumar());
				val3.put("dataPronuntare", convertDateToString(dosar
						.getSedinte().get(i).getDataPronuntare()));
				val3.put("documentSedinta", dosar.getSedinte().get(i)
						.getDocumentSedinta());
				val3.put("numarDocument", dosar.getSedinte().get(i)
						.getNumarDocument());
				val3.put("dataDocument", convertDateToString(dosar.getSedinte()
						.get(i).getDataDocument()));
				sqlTable.insertOrThrow("dosarsedinta", null, val3);
			}
		}
		if (dosar.getCaiAtac() != null) {
			for (int i = 0; i < dosar.getCaiAtac().size(); i++) {
				ContentValues val3 = new ContentValues();
				val3.put("id_dosar", id);
				val3.put("dataDeclarare", convertDateToString(dosar
						.getCaiAtac().get(i).getDataDeclarare()));
				val3.put("parteDeclaratoare", dosar.getCaiAtac().get(i)
						.getParteDeclaratoare());
				val3.put("tipCaleAtac", dosar.getCaiAtac().get(i)
						.getTipCaleAtac());
				sqlTable.insertOrThrow("dosarcaleatac", null, val3);
			}
		}

		cursor.close();
		sqlTable.close();
		sqlHelper.close();

		return id;

	}
	
	

	public void insertDosarOnId(Dosar dosar, int id, String nume,
			String observatii) {
		// int id;
		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();

		// Log.d("debug", dosar.getDataModificare().toString());

		ContentValues val = new ContentValues();

		val.put("id", id + "");

		val.put("numar", dosar.getNumar() + "");
		val.put("numarVechi", dosar.getNumarVechi() + "");
		val.put("nume", nume);
		val.put("observatii", observatii);

		val.put("data", dosar.getData().get(Calendar.YEAR) + "."
				+ (dosar.getData().get(Calendar.MONTH) + 1) + "."
				+ dosar.getData().get(Calendar.DAY_OF_MONTH));
		val.put("institutie", dosar.getInstitutie() + "");
		val.put("departament", dosar.getDepartament() + "");
		val.put("categorieCaz", dosar.getCategorieCaz() + "");
		val.put("stadiuProcesual", dosar.getStadiuProcesual() + "");
		val.put("obiect", dosar.getObiect() + "");
		
		
		val.put("dataModificare",
				convertDateToString(dosar.getDataModificare()));
		sqlTable.insertOrThrow("dosar", null, val);
		//
		// String[] COLUMNS = { "id" };
		//
		// Cursor cursor = sqlTable.query("dosar", COLUMNS, null, null, null,
		// null, null);
		//
		// if (cursor != null && cursor.moveToLast()) {
		// // id = Integer.valueOf(cursor.getString(0));
		// } else {
		// System.out.println("aci butu frenkie");
		//
		// return;
		//
		// }

		for (int i = 0; i < dosar.getParti().size(); i++) {
			ContentValues val2 = new ContentValues();
			val2.put("id_dosar", id);
			val2.put("nume", dosar.getParti().get(i).getNume());
			val2.put("calitateParte", dosar.getParti().get(i)
					.getCalitateParte());
			sqlTable.insertOrThrow("dosarparte", null, val2);
		}

		for (int i = 0; i < dosar.getSedinte().size(); i++) {
			ContentValues val3 = new ContentValues();
			val3.put("id_dosar", id);
			val3.put("complet", dosar.getSedinte().get(i).getComplet());
			val3.put("data", convertDateToString(dosar.getSedinte().get(i)
					.getData()));
			val3.put("ora", dosar.getSedinte().get(i).getOra());
			val3.put("solutie", dosar.getSedinte().get(i).getSolutie());
			val3.put("solutieSumar", dosar.getSedinte().get(i)
					.getSolutieSumar());
			val3.put("dataPronuntare", convertDateToString(dosar.getSedinte()
					.get(i).getDataPronuntare()));
			val3.put("documentSedinta", dosar.getSedinte().get(i)
					.getDocumentSedinta());
			val3.put("numarDocument", dosar.getSedinte().get(i)
					.getNumarDocument());
			val3.put("dataDocument", convertDateToString(dosar.getSedinte()
					.get(i).getDataDocument()));
			sqlTable.insertOrThrow("dosarsedinta", null, val3);
		}

		if (dosar.getCaiAtac() != null) {
			for (int i = 0; i < dosar.getCaiAtac().size(); i++) {
				ContentValues val3 = new ContentValues();
				val3.put("id_dosar", id);
				val3.put("dataDeclarare", convertDateToString(dosar
						.getCaiAtac().get(i).getDataDeclarare()));
				val3.put("parteDeclaratoare", dosar.getCaiAtac().get(i)
						.getParteDeclaratoare());
				val3.put("tipCaleAtac", dosar.getCaiAtac().get(i)
						.getTipCaleAtac());
				sqlTable.insertOrThrow("dosarcaleatac", null, val3);
			}
		}

		sqlTable.close();
		sqlHelper.close();

		// return id;

	}

	public Dosar getDosar(int id) {
		Dosar dosar = new Dosar();

		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();

		String[] COLUMNS = { "numar", "numarVechi", "data", "institutie",
				"departament", "categorieCaz", "stadiuProcesual", "obiect",
				"dataModificare", "nume", "observatii" };

		Cursor cursor = sqlTable.query("dosar", COLUMNS, "id = '" + id + "'",
				null, null, null, null);

		if (cursor == null)
			return null;

		if (cursor != null && cursor.moveToFirst()) {
			// Log.d("debug", cursor.getString(0));
			// Log.d("debug", cal2.get(Calendar.YEAR) + " <-an");

			Calendar cal = Calendar.getInstance();
			Calendar cal2 = Calendar.getInstance();

			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
				cal.setTime(sdf.parse(cursor.getString(2)));
				cal2.setTime(sdf.parse(cursor.getString(8)));
			} catch (Exception e) {
			}

			dosar.setId(id);
			dosar.setNumar(cursor.getString(0));
			dosar.setNumarVechi(cursor.getString(1));
			dosar.setData(cal);
			dosar.setInstitutie(cursor.getString(3));
			dosar.setDepartament(cursor.getString(4));
			dosar.setCategorieCaz(cursor.getString(5));
			dosar.setStadiuProcesual(cursor.getString(6));
			dosar.setObiect(cursor.getString(7));
			dosar.setDataModificare(cal2);

			// 9 10

			DosarObservatii d = new DosarObservatii();
			d.setNume(cursor.getString(9));
			d.setObservatii(cursor.getString(10));

			dosar.setDosarObservatii(d);

			// Log.e("get", "Numar doasr: " + dosar.getNumar());

		} else {
			Log.d("debug", "ninci-general");
		}

		String[] COLUMNS2 = { "nume", "calitateParte" };

		Cursor cursor2 = sqlTable.query("dosarparte", COLUMNS2, "id_dosar = '"
				+ id + "'", null, null, null, null);

		ArrayList<DosarParte> lista_parti = new ArrayList<DosarParte>();

		if (cursor2 != null) {
			while (cursor2.moveToNext()) {
				DosarParte d = new DosarParte();
				d.setNume(cursor2.getString(0));
				d.setCalitateParte(cursor2.getString(1));
				lista_parti.add(d);
			}
		} else {
			Log.d("debug", "ninci-parti");
		}

		dosar.setParti(lista_parti);

		String[] COLUMNS3 = { "complet", "data", "ora", "solutie",
				"solutieSumar", "dataPronuntare", "documentSedinta",
				"numarDocument", "dataDocument" };

		Cursor cursor3 = sqlTable.query("dosarsedinta", COLUMNS3,
				"id_dosar = '" + id + "'", null, null, null, null);

		ArrayList<DosarSedinta> lista_sedinte = new ArrayList<DosarSedinta>();

		if (cursor3 != null) {
			while (cursor3.moveToNext()) {
				DosarSedinta d = new DosarSedinta();
				d.setComplet(cursor3.getString(0));
				d.setData(convertStringToDate(cursor3.getString(1)));
				d.setOra(cursor3.getString(2));
				d.setSolutie(cursor3.getString(3));
				d.setSolutieSumar(cursor3.getString(4));
				d.setDataPronuntare(convertStringToDate(cursor3.getString(5)));
				d.setDocumentSedinta(cursor3.getString(6));
				d.setNumarDocument(cursor3.getString(7));
				d.setDataDocument(convertStringToDate(cursor3.getString(8)));
				lista_sedinte.add(d);
			}
		} else {
			Log.d("debug", "ninci-sedinte");
		}

		dosar.setSedinte(lista_sedinte);

		String[] COLUMNS4 = { "dataDeclarare", "parteDeclaratoare",
				"tipCaleAtac" };

		ArrayList<DosarCaleAtac> lista_caiAtac = new ArrayList<DosarCaleAtac>();

		Cursor cursor4 = sqlTable.query("dosarcaleatac", COLUMNS4,
				"id_dosar = '" + id + "'", null, null, null, null);

		if (cursor4 != null) {
			while (cursor4.moveToNext()) {
				DosarCaleAtac d = new DosarCaleAtac();
				d.setDataDeclarare(convertStringToDate(cursor4.getString(0)));
				d.setParteDeclaratoare(cursor4.getString(1));
				d.setTipCaleAtac(cursor4.getString(2));
				lista_caiAtac.add(d);
			}
		} else {
			Log.d("debug", "ninci-cai-atac");
		}

		dosar.setCaiAtac(lista_caiAtac);

		
		cursor.close();
		cursor2.close();
		cursor3.close();
		cursor4.close();
		
		sqlTable.close();
		sqlHelper.close();

		return dosar;
	}

	public ArrayList<DosarSimplif> getListaDosareSimplif() {
		ArrayList<DosarSimplif> lista = new ArrayList<DosarSimplif>();

		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();

		String[] COLUMNS = { "id", "numar", "institutie", "nume" };

		Cursor cursor = sqlTable.query("dosar", COLUMNS, "nume!=''", null, null,
				null, "nume ASC");
//		Cursor cursor = sqlTable.query("dosar", COLUMNS, null, null, null,
//				null, "nume ASC");
		
		// cursor.moveToFirst();

		if (cursor != null) {
			while (cursor.moveToNext()) {
				DosarSimplif d = new DosarSimplif();
				d.setNumar(cursor.getString(1));
				d.setNume(cursor.getString(3));
				d.setId(Integer.valueOf(cursor.getString(0)));
				d.setLocatie(cursor.getString(2));

				ArrayList<DosarParte> lista_p = new ArrayList<DosarParte>();

				lista_p.clear();

				String[] COLUMNS2 = { "nume" };

				Cursor cursor2 = sqlTable.query("dosarParte", COLUMNS2,
						"id_dosar='" + cursor.getString(0) + "'", null, null,
						null, null);
				if (cursor2 != null) {
					while (cursor2.moveToNext()) {
						DosarParte dosarParte = new DosarParte();
						dosarParte.setNume(cursor2.getString(0));
						lista_p.add(dosarParte);
					}
				}
				cursor2.close();
				d.setLista_parti(lista_p);
				lista.add(d);
			}
		}
		
		cursor.close();
		
		Cursor cursor3 = sqlTable.query("dosar", COLUMNS, "nume=''", null, null,
				null, "id DESC");
		// cursor.moveToFirst();

		if (cursor3 != null) {
			while (cursor3.moveToNext()) {
				DosarSimplif d = new DosarSimplif();
				d.setNumar(cursor3.getString(1));
				d.setNume(cursor3.getString(3));
				d.setId(Integer.valueOf(cursor3.getString(0)));
				d.setLocatie(cursor3.getString(2));

				ArrayList<DosarParte> lista_p = new ArrayList<DosarParte>();

				lista_p.clear();

				String[] COLUMNS2 = { "nume" };

				Cursor cursor4 = sqlTable.query("dosarParte", COLUMNS2,
						"id_dosar='" + cursor3.getString(0) + "'", null, null,
						null, null);
				if (cursor4 != null) {
					while (cursor4.moveToNext()) {
						DosarParte dosarParte = new DosarParte();
						dosarParte.setNume(cursor4.getString(0));
						lista_p.add(dosarParte);
					}
				}
				
				cursor4.close();
				
				d.setLista_parti(lista_p);
				lista.add(d);
			}
		}

		cursor.close();
		cursor3.close();

		sqlTable.close();
		sqlHelper.close();

		return lista;
	}

	public ArrayList<Dosar> getListaDosare() {
		ArrayList<Dosar> lista = new ArrayList<Dosar>();

		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();

		String[] COLUMNS = { "id" };

		Cursor cursor = sqlTable.query("dosar", COLUMNS, null, null, null,
				null, null);
		// cursor.moveToFirst();

		if (cursor != null) {
			while (cursor.moveToNext()) {
				Dosar d = new Dosar();
				d = getDosar(Integer.valueOf(cursor.getString(0)));
				lista.add(d);
			}
		}
		
		cursor.close();

		sqlTable.close();
		sqlHelper.close();

		return lista;
	}

	public ArrayList<DosarSedinta> getListaDosarSedinta(int id) {
		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();

		String[] COLUMNS = { "complet", "data", "ora", "solutie",
				"solutieSumar", "dataPronuntare", "documentSedinta",
				"numarDocument", "dataDocument" };

		Cursor cursor3 = sqlTable.query("dosarsedinta", COLUMNS, "id_dosar = '"
				+ id + "'", null, null, null, null);

		ArrayList<DosarSedinta> lista_sedinte = new ArrayList<DosarSedinta>();

		if (cursor3 != null) {
			while (cursor3.moveToNext()) {

				DosarSedinta d = new DosarSedinta();
				d.setComplet(cursor3.getString(0));
				d.setData(convertStringToDate(cursor3.getString(1)));
				d.setOra(cursor3.getString(2));
				d.setSolutie(cursor3.getString(3));
				d.setSolutieSumar(cursor3.getString(4));
				d.setDataPronuntare(convertStringToDate(cursor3.getString(5)));
				d.setDocumentSedinta(cursor3.getString(6));
				d.setNumarDocument(cursor3.getString(7));
				d.setDataDocument(convertStringToDate(cursor3.getString(8)));
				lista_sedinte.add(d);
			}

		} else {
			Log.d("debug", "ninci-sedinte");
		}

		cursor3.close();
		
		sqlTable.close();
		sqlHelper.close();

		return lista_sedinte;
	}

	public ArrayList<SedintaCurenta> getListaSedinteByDate(Calendar c) {
		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();

		String[] COLUMNS = { "complet", "data", "ora", "solutie",
				"solutieSumar", "dataPronuntare", "documentSedinta",
				"numarDocument", "dataDocument", "id_dosar" };

		Cursor cursor3 = sqlTable.query(
				"dosarsedinta",
				COLUMNS,
				"data = '" + c.get(Calendar.YEAR) + "."
						+ (c.get(Calendar.MONTH) + 1) + "."
						+ c.get(Calendar.DAY_OF_MONTH) + "'", null, null, null,
				null);

		ArrayList<SedintaCurenta> lista_sedinte = new ArrayList<SedintaCurenta>();

		if (cursor3 != null) {

			while (cursor3.moveToNext()) {

				// Log.e("de", cursor3.getString(1) + " <-data");

				DosarSedinta d = new DosarSedinta();
				d.setComplet(cursor3.getString(0));
				d.setData(convertStringToDate(cursor3.getString(1)));
				d.setOra(cursor3.getString(2));
				d.setSolutie(cursor3.getString(3));
				d.setSolutieSumar(cursor3.getString(4));
				d.setDataPronuntare(convertStringToDate(cursor3.getString(5)));
				d.setDocumentSedinta(cursor3.getString(6));
				d.setNumarDocument(cursor3.getString(7));
				d.setDataDocument(convertStringToDate(cursor3.getString(8)));

				SedintaCurenta dd = new SedintaCurenta();
				dd.setDosarSedinta(d);

				String[] COLUMNSS = { "numar", "numarVechi", "data",
						"institutie", "departament", "categorieCaz",
						"stadiuProcesual", "obiect", "dataModificare", "nume" };

				Cursor cursor4 = sqlTable.query("dosar", COLUMNSS, "id = '"
						+ cursor3.getString(9) + "'", null, null, null, null);

				cursor4.moveToFirst();
				
				dd.setDosar(cursor4.getString(0));
				dd.setNume(cursor4.getString(9));

				dd.setInstitutie(cursor4.getString(3));
				dd.setId_dosar(Integer.valueOf(cursor3.getString(9)));

				// d.setComplet(cursor3.getString(0));
				// d.setData(convertStringToDate(cursor3.getString(1)));
				// d.setOra(cursor3.getString(2));
				// d.setSolutie(cursor3.getString(3));
				// d.setSolutieSumar(cursor3.getString(4));
				// d.setDataPronuntare(convertStringToDate(cursor3.getString(5)));
				// d.setDocumentSedinta(cursor3.getString(6));
				// d.setNumarDocument(cursor3.getString(7));
				// d.setDataDocument(convertStringToDate(cursor3.getString(8)));

				cursor4.close();
				
				lista_sedinte.add(dd);
			}

		} else {
			Log.d("debug", "ninci-sedinte");
		}

		cursor3.close();
		sqlTable.close();
		sqlHelper.close();

		return lista_sedinte;

	}

	public ArrayList<Dosar> getListaDosareNumar(String numar) {
		ArrayList<Dosar> lista = new ArrayList<Dosar>();

		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();

		String[] COLUMNS = { "id" };

		Cursor cursor = sqlTable.query("dosar", COLUMNS, "numar = '" + numar
				+ "'", null, null, null, null);
		// cursor.moveToFirst();

		if (cursor != null) {
			while (cursor.moveToNext()) {
				lista.add(getDosar(Integer.valueOf(cursor.getString(0))));
			}
		}

		cursor.close();
		
		sqlTable.close();
		sqlHelper.close();

		return lista;
	}
	
	
	public ArrayList<Dosar> getListaDosareNumarWithoutClosed(String numar) {
		ArrayList<Dosar> lista = new ArrayList<Dosar>();

		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();

		String[] COLUMNS = { "id" };

		Cursor cursor = sqlTable.query("dosar", COLUMNS, "numar = '" + numar
				+ "'", null, null, null, null);

		if (cursor != null) {
			while (cursor.moveToNext()) {
				lista.add(getDosar(Integer.valueOf(cursor.getString(0))));
			}
		}

		cursor.close();
		sqlTable.close();
		sqlHelper.close();

		return lista;
	}

	//
	// public ArrayList<Calendar> getSedinteByDate(Calendar c) {
	//
	// HelperSql sqlHelper = new HelperSql(context);
	// SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();
	//
	// String[] COLUMNS = { "complet", "data", "ora", "solutie",
	// "solutieSumar", "dataPronuntare", "documentSedinta",
	// "numarDocument", "dataDocument" };
	//
	// Cursor cursor3 = sqlTable.query(
	// "dosarsedinta",
	// COLUMNS,
	// "data >= '" + c.get(Calendar.YEAR) + "."
	// + (c.get(Calendar.MONTH) + 1) + ".1' AND "
	// + "data <= '" + c.get(Calendar.YEAR) + "."
	// + (c.get(Calendar.MONTH) + 1) + ".31" + "'", null,
	// null, null, null);
	//
	// ArrayList<Calendar> lista_cal = new ArrayList<Calendar>();
	//
	// if (cursor3 != null) {
	// while (cursor3.moveToNext()) {
	// Calendar cc = Calendar.getInstance();
	// cc = convertStringToDate(cursor3.getString(1));
	// lista_cal.add(cc);
	// }
	//
	// } else {
	// Log.d("debug", "ninci-sedinte");
	// }
	//
	// sqlTable.close();
	// sqlHelper.close();
	//
	// return lista_cal;
	//
	// }
	//
	// public ArrayList<Calendar> getSedinteByDate(Calendar c) {
	//
	// HelperSql sqlHelper = new HelperSql(context);
	// SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();
	//
	// String[] COLUMNS = { "complet", "data", "ora", "solutie",
	// "solutieSumar", "dataPronuntare", "documentSedinta",
	// "numarDocument", "dataDocument" };
	//
	// Cursor cursor3 = sqlTable.query(
	// "dosarsedinta",
	// COLUMNS,
	// "data >= '" + c.get(Calendar.YEAR) + "."
	// + (c.get(Calendar.MONTH) + 1) + ".1' AND "
	// + "data <= '" + c.get(Calendar.YEAR) + "."
	// + (c.get(Calendar.MONTH) + 1) + ".31" + "'", null,
	// null, null, null);
	//
	// // Cursor cursor3 = sqlTable.query("dosarsedinta", COLUMNS, null, null,
	// // null,
	// // null, null);
	//
	// ArrayList<Calendar> lista_cal = new ArrayList<Calendar>();
	// if (cursor3 != null) {
	// while (cursor3.moveToNext()) {
	// Calendar cc = Calendar.getInstance();
	// cc = convertStringToDate(cursor3.getString(1));
	// lista_cal.add(cc);
	// }
	//
	// } else {
	// Log.d("debug", "ninci-sedinte");
	// }
	//
	// sqlTable.close();
	// sqlHelper.close();
	//
	// return lista_cal;
	//
	// }

	public ArrayList<Calendar> getSedinteByDate(Calendar c) {

		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();

		String[] COLUMNS = { "complet", "data", "ora", "solutie",
				"solutieSumar", "dataPronuntare", "documentSedinta",
				"numarDocument", "dataDocument" };

		// Cursor cursor3 = sqlTable.query(
		// "dosarsedinta",
		// COLUMNS,
		// "data >= '" + c.get(Calendar.YEAR) + "."
		// + (c.get(Calendar.MONTH) + 1) + ".1' AND "
		// + "data <= '" + c.get(Calendar.YEAR) + "."
		// + (c.get(Calendar.MONTH) + 1) + ".31" + "'", null,
		// null, null, null);

		Cursor cursor3 = sqlTable.query("dosarsedinta", COLUMNS, null, null,
				null, null, null);

		// Cursor cursor3 = sqlTable.query("dosarsedinta", COLUMNS, null, null,
		// null,
		// null, null);

		ArrayList<Calendar> lista_cal = new ArrayList<Calendar>();
		if (cursor3 != null) {
			while (cursor3.moveToNext()) {
				Calendar cc = Calendar.getInstance();
				cc = convertStringToDate(cursor3.getString(1));
				lista_cal.add(cc);
			}

		} else {
			Log.d("debug", "ninci-sedinte");
		}

		cursor3.close();
		sqlTable.close();
		sqlHelper.close();

		return lista_cal;

	}
	

	public ArrayList<Calendar> getSedinteByDateWithoutAmanate(Calendar c) {

		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();

		String[] COLUMNS = { "complet", "data", "ora", "solutie",
				"solutieSumar", "dataPronuntare", "documentSedinta",
				"numarDocument", "dataDocument" };

		// Cursor cursor3 = sqlTable.query(
		// "dosarsedinta",
		// COLUMNS,
		// "data >= '" + c.get(Calendar.YEAR) + "."
		// + (c.get(Calendar.MONTH) + 1) + ".1' AND "
		// + "data <= '" + c.get(Calendar.YEAR) + "."
		// + (c.get(Calendar.MONTH) + 1) + ".31" + "'", null,
		// null, null, null);

		Cursor cursor3 = sqlTable.query("dosarsedinta", COLUMNS, "solutie!='Amână cauza'", null,
				null, null, null);

//		 Cursor cursor3 = sqlTable.query("dosarsedinta", COLUMNS, null, null,
		// null,
		// null, null);

		ArrayList<Calendar> lista_cal = new ArrayList<Calendar>();
		if (cursor3 != null) {
			while (cursor3.moveToNext()) {
				Calendar cc = Calendar.getInstance();
				cc = convertStringToDate(cursor3.getString(1));
				lista_cal.add(cc);
			}

		} else {
			Log.d("debug", "ninci-sedinte");
		}

		
		cursor3.close();		
		sqlTable.close();
		sqlHelper.close();
		
		Log.e("debug", "Marime d: "+lista_cal.size());
		
		return lista_cal;

	}

	public void putObservatii(String nume, String obervatii, int id) {

		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getWritableDatabase();

		ContentValues val = new ContentValues();
		val.put("nume", nume + "");
		val.put("observatii", obervatii + "");

		sqlTable.update("dosar", val, "id='" + id + "'", null);

		sqlTable.close();
		sqlHelper.close();

	}

	public DosarObservatii getObservatii(int id) {

		DosarObservatii d = new DosarObservatii();

		d.setId(id);

		HelperSql sqlHelper = new HelperSql(context);
		SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();

		String[] COLUMNS = { "nume", "observatii" };

		Cursor cursor3 = sqlTable.query("dosar", COLUMNS, "id = '" + id + "'",
				null, null, null, null);

		if (cursor3 != null) {
			cursor3.moveToFirst();
			d.setNume(cursor3.getString(0));
			d.setObservatii(cursor3.getString(1));
		}

		cursor3.close();

		sqlTable.close();
		sqlHelper.close();
		
		return d;
	}

}
