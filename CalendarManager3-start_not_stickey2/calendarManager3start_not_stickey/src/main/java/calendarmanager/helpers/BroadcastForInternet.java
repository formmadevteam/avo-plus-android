package calendarmanager.helpers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class BroadcastForInternet extends BroadcastReceiver {


	public boolean isOnline(Context context) {
		try {
			Process p1 = java.lang.Runtime.getRuntime().exec(
					"ping -c 1 www.google.com");
			int returnVal = p1.waitFor();
			boolean reachable = (returnVal == 0);
			return reachable;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	private PendingIntent pendingIntent;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		// Intent i = new Intent(context, BackupService.class);

		
		SharedPreferences prefs = context.getSharedPreferences("com.example.app",
				Context.MODE_PRIVATE);
		
		int hour = prefs.getInt("ora_update", 1);
		int minute = prefs.getInt("minut_update", 0);
		
		long delay = (hour*60+minute)*60*1000;
		
		Intent intentt = new Intent(context, BackupService.class);
		PendingIntent pendingIntent = PendingIntent.getService(
				context, 433, intentt,
				PendingIntent.FLAG_UPDATE_CURRENT);

		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(context.ALARM_SERVICE);

		if (isOnline(context)) {
			// context.startService(i);
			alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
					System.currentTimeMillis(), delay, pendingIntent);
			Log.d("service started", "service started");

		} else {
			Log.d("de", "stop service");
			context.stopService(intentt);
			alarmManager.cancel(pendingIntent);
		}
	}

}
