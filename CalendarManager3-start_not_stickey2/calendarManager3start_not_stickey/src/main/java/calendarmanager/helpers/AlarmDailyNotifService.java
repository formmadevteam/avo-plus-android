	package calendarmanager.helpers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import calendarmanager.activities.MainActivity;
import calendarmanager.activities.R;

public class AlarmDailyNotifService extends Service {

	private Timer timer = new Timer();

	private PendingIntent pendingIntent;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public String convertDateToString(Calendar cal) {
		if (cal != null)
			return String.valueOf(cal.get(Calendar.YEAR) + "."
					+ (cal.get(Calendar.MONTH) + 1) + "."
					+ cal.get(Calendar.DAY_OF_MONTH));
		else
			return "";
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		Log.e("debug", "Aci");
		
		SharedPreferences prefs = this.getSharedPreferences("com.example.app",
				Context.MODE_PRIVATE);

		int ok_prog = prefs.getInt("ok_prog", 0);

		if (ok_prog != 0) {
			if (intent != null)
				
				if (intent.getStringExtra("data") != null) {
					Intent resultIntent = new Intent(this, MainActivity.class);
					resultIntent
							.putExtra("data", intent.getStringExtra("data"));

					NotificationCompat.Builder mBuilder;

					int size = intent.getIntExtra("size", 0);
					if (size != 0) {
						mBuilder = new NotificationCompat.Builder(this)
								.setSmallIcon(R.drawable.ic_action_refresh)
								.setContentTitle("Programul de maine: ")
								.setContentText(
										"Data: "
												+ intent.getStringExtra("data"))
								.setAutoCancel(true);

					} else {
						mBuilder = new NotificationCompat.Builder(this)
								.setSmallIcon(R.drawable.ic_action_refresh)
								.setContentTitle("Nu exista sedinte pe maine")
								.setContentText(
										"Data: "
												+ intent.getStringExtra("data"))
								.setAutoCancel(true);
					}

					TaskStackBuilder stackBuilder = TaskStackBuilder
							.create(this);
					stackBuilder.addParentStack(MainActivity.class);
					stackBuilder.addNextIntent(resultIntent);
					PendingIntent resultPendingIntent = stackBuilder
							.getPendingIntent(
									intent.getIntExtra("id_notif", 5455),
									PendingIntent.FLAG_ONE_SHOT);
					mBuilder.setContentIntent(resultPendingIntent);
					NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

					mNotificationManager.notify(
							(int)System.currentTimeMillis(),
							mBuilder.build());
				}

			return START_STICKY;
		} else {
			stopSelf();
			return 0;
		}

	}

	private void startBackup() {

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
		}
		System.out.println("Destroyed");
	}

	protected Calendar convertDate(String data) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			Date d = format.parse(data);
			cal.setTime(d);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return cal;
	}

}