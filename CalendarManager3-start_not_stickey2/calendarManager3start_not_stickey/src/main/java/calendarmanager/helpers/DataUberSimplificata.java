package calendarmanager.helpers;

import java.util.Calendar;

import android.provider.ContactsContract.Contacts.Data;

public class DataUberSimplificata {
	public int year;
	public int month;
	public int day;
	public DataUberSimplificata() {
	
	}
	public static DataUberSimplificata getToday() {
		Calendar cal = Calendar.getInstance();
		return getDataByCalendar(cal);
	}
	public static DataUberSimplificata getDataByCalendar(Calendar cal) {
		DataUberSimplificata dummy = new DataUberSimplificata();
		dummy.year = cal.get(Calendar.YEAR);
		dummy.month = cal.get(Calendar.MONTH);
		dummy.day = cal.get(Calendar.DAY_OF_MONTH);
		return dummy;
	}
}
