package calendarmanager.helpers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import calendaradapter.structures.DosarSimplif;
import calendarmanager.activities.ConnectionDetector;
import calendarmanager.notifications.UberNotificationManager;
import ecris.data.types.Dosar;
import ecris.data.types.DosarCaleAtac;
import ecris.data.types.DosarParte;
import ecris.data.types.DosarSedinta;

public class BackupService extends Service {

	private Timer timer = new Timer();

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	// public boolean isOnline(Context context) {
	// try {
	//
	//
	// Process p1 = java.lang.Runtime.getRuntime().exec(
	// "ping -c 1 www.google.com");
	// int returnVal = p1.waitFor();
	// boolean reachable = (returnVal == 0);
	// return reachable;
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// return false;
	// }

	public boolean isOnline(Context context) {

		ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

		Boolean isInternetPresent = cd.isConnectingToInternet();

		return isInternetPresent;

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();

		Log.e("bubu", "Sbeng - am pornit");

		startBackup();
		return START_STICKY;
	}

	private void startBackup() {
		// timer.scheduleAtFixedRate(new BackupTimerTask(),
		// System.currentTimeMillis(), delayTime());
		// functie();

		if (isOnline(getApplicationContext())) {
			RequestTask t = new RequestTask();
			t.execute();
		} else {
			Toast.makeText(getApplicationContext(),
					"Conexiunea la internet nu este functionala!",
					Toast.LENGTH_LONG).show();
		}

	}

//	1648/83/2001
	
	// static SharedPreferences prefs;

	private long delayTime() {

		SharedPreferences prefs = this.getSharedPreferences("com.example.app",
				Context.MODE_PRIVATE);

		int hour = prefs.getInt("ora_update", 1);
		int minute = prefs.getInt("mi" + "nut_update", 0);

		// long delay = (hour*60+minute)*60*1000;
		long delay = 1000;

		System.out.println("delay time:" + delay);
		return delay;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
		}
		System.out.println("Destroyed");
	}

	// public static ArrayList<Dosar> dose = null;

	class RequestTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			functie();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}

	}

	public void functie() {
		Log.e("debugu", "De aici incepem fxurile man");

		System.out.println("Backup started");
		// starting backup here

		// UberNotificationManager uberNM = UberNotificationManager
		// .getInstance(getApplicationContext());
		// uberNM.notifySyncNotification(getApplicationContext());
		UberNotificationManager uberNM = UberNotificationManager
				.getInstance(getApplicationContext());
		// uberNM.notifySyncNotification(getApplicationContext());

		HelperSqlFunctions f = new HelperSqlFunctions(getApplicationContext());
		ArrayList<DosarSimplif> list = f.getListaDosareSimplif();

		EcrisUtils e = new EcrisUtils();

		for (int i = 0; i < list.size() - 1; i++) {
			for (int j = i + 1; j < list.size(); j++) {
				if (list.get(i).getNumar().compareTo(list.get(j).getNumar()) == 0) {
					list.remove(j);
				}
			}
		}

		System.out.println(list.size() + "");
		AlarmManager alarmManager = (AlarmManager) getApplicationContext()
				.getSystemService(getApplicationContext().ALARM_SERVICE);

		// Log.e("debu", "Incepem: " + list.size());

		// DosarSimplif dosarSimplif = list.get(0);
		// ArrayList<Dosar> dose = e.getFahrenheitList(
		// dosarSimplif.getNumar(), getApplicationContext());

		// Log.e("debu", "Incepem-m: " + dose.size());

		
		
		for(int dos = 0; dos<list.size(); dos++){
//		for (DosarSimplif dosarSimplif : list) {
			int idnr = 0;
			
			ArrayList<Dosar> dosee = f.getListaDosareNumar(list.get(dos)
					.getNumar());

			if (isOnline(getApplicationContext())) {
				ArrayList<Dosar> dose = e.getFahrenheitList(
						list.get(dos).getNumar(), getApplicationContext());
				

				int notif = 1000;
				if (dose != null)
					for (int i = 0; i < dosee.size(); i++) {
						for (int j = 0; j < dose.size(); j++) {

							// System.out.println(dosee.get(i).getInstitutie());
							// System.out.println(dose.get(j).getInstitutie());
							//
							if (dosee.get(i).getInstitutie()
									.compareTo(dose.get(j).getInstitutie()) == 0) {
								
//								if (compareDosare(dosee.get(i), dose.get(j))) {
								
								Log.e("debug", "Close enough");
								
								idnr++;
								if (!compareSimplifDosare(dosee.get(i), dose.get(j))) {
									
									Log.e("debug", dosee.get(i).getDataModificare().toString()+" - "+dose.get(j).getDataModificare().toString());
									
									int idu = (int) dosee.get(i).getId();

									Intent intentt = new Intent(
											getApplicationContext(),
											BackupNotifService.class);
									intentt.putExtra("numarDosar", dosee.get(i)
											.getNumar());
									intentt.putExtra("id_dos", dosee.get(i)
											.getId());
									intentt.putExtra("id_notif", idu);

									PendingIntent pendingIntent = PendingIntent
											.getService(
													getApplicationContext(),
													idu,
													intentt,
													PendingIntent.FLAG_UPDATE_CURRENT);
									alarmManager.set(AlarmManager.RTC_WAKEUP,
											System.currentTimeMillis(),
											pendingIntent);

									String nume = dosee.get(i)
											.getDosarObservatii().getNume();
									String observatii = dosee.get(i)
											.getDosarObservatii()
											.getObservatii();

									f.deleteDosar(idu);
									f.insertDosarOnId(dose.get(j), idu, nume,
											observatii);

								}
							}
							
							
						}
					}

				else {
//					Toast.makeText(this,
//							"Conexiunea la internet nu este functionala!",
//							Toast.LENGTH_LONG).show();
					return;
				}

			}
			
			Log.e("debug", "Operatii efectuate: "+idnr);
			
		}

	}

	protected Calendar convertDate(String data) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			Date d = format.parse(data);
			cal.setTime(d);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return cal;
	}
	
	
	public boolean compareSimplifDosare(Dosar d1, Dosar d2) {
//
		String datm1 = d1.getDataModificare().get(Calendar.DAY_OF_MONTH) + "."
				+ d1.getDataModificare().get(Calendar.MONTH) + "."
				+ d1.getDataModificare().get(Calendar.YEAR);

		String datm2 = d2.getDataModificare().get(Calendar.DAY_OF_MONTH) + "."
				+ d2.getDataModificare().get(Calendar.MONTH) + "."
				+ d2.getDataModificare().get(Calendar.YEAR);

		if (datm1.compareTo(datm2) != 0)
			return false;
		
		return true;
	}

	public boolean compareDosare(Dosar d1, Dosar d2) {


		String datm1 = d1.getDataModificare().get(Calendar.DAY_OF_MONTH) + "."
				+ d1.getDataModificare().get(Calendar.MONTH) + "."
				+ d1.getDataModificare().get(Calendar.YEAR);

		String datm2 = d2.getDataModificare().get(Calendar.DAY_OF_MONTH) + "."
				+ d2.getDataModificare().get(Calendar.MONTH) + "."
				+ d2.getDataModificare().get(Calendar.YEAR);

		if (datm1.compareTo(datm2) != 0)
			return false;
		
		if (d1.getNumar().compareTo(d2.getNumar()) != 0)
			return false;
		// Log.e("debu", "0");
		if (d1.getNumarVechi().compareTo(d2.getNumarVechi()) != 0)
			return false;
		// Log.e("debu", "1");

		String dat1 = d1.getData().get(Calendar.DAY_OF_MONTH) + "."
				+ d1.getData().get(Calendar.MONTH) + "."
				+ d1.getData().get(Calendar.YEAR);

		String dat2 = d2.getData().get(Calendar.DAY_OF_MONTH) + "."
				+ d2.getData().get(Calendar.MONTH) + "."
				+ d2.getData().get(Calendar.YEAR);

		if (dat1.compareTo(dat2) != 0)
			return false;
		if (d1.getInstitutie().compareTo(d2.getInstitutie()) != 0)
			return false;
		if (d1.getDepartament().compareTo(d2.getDepartament()) != 0)
			return false;
		if (d1.getCategorieCaz().compareTo(d2.getCategorieCaz()) != 0)
			return false;
		if (d1.getStadiuProcesual().compareTo(d2.getStadiuProcesual()) != 0)
			return false;
		if (d1.getObiect().compareTo(d2.getObiect()) != 0)
			return false;
	

		if (d1.getSedinte() != null && d2.getSedinte() != null) {
			if (d1.getSedinte().size() == d2.getSedinte().size()) {
				for (int i = 0; i < d1.getSedinte().size(); i++) {
					if (!compareSedinte(d1.getSedinte().get(i), d2.getSedinte()
							.get(i))) {
						return false;
					}
				}
			} else {
				return false;
			}
		} else {
			if (d2.getSedinte() != null) {
				return false;
			}
		}

		// if (d1.getSedinte().size() != d2.getSedinte().size())
		// return false;
		// Log.e("debu", "9");

		if (d1.getParti().size() == d2.getParti().size()) {
			for (int i = 0; i < d1.getParti().size(); i++) {
				if (!compareParti(d1.getParti().get(i), d2.getParti().get(i))) {
					return false;
				}
			}
		} else {
			return false;
		}

		// if (d1.getParti().size() != d2.getParti().size())
		// return false;
		// Log.e("debu", "8");
		//

		if (d1.getCaiAtac() != null && d2.getCaiAtac() != null) {
			if (d1.getCaiAtac().size() == d2.getCaiAtac().size()) {
				for (int i = 0; i < d1.getCaiAtac().size(); i++) {
					if (!compareCaiDeAtac(d1.getCaiAtac().get(i), d2
							.getCaiAtac().get(i))) {
						return false;
					}
				}
			} else {
				return false;
			}
		}

		// if (d1.getCaiAtac() != null && d2.getCaiAtac() != null)
		// if (d1.getCaiAtac().size() != d2.getCaiAtac().size())
		// return false;

		// Log.e("debu", "10");

		return true;
	}

	public boolean compareSedinte(DosarSedinta d1, DosarSedinta d2) {
		// Log.e("frbu", "Zbeng");
		if (d1.getComplet().compareTo(d2.getComplet()) != 0)
			return false;
		if (d1.getOra().compareTo(d2.getOra()) != 0)
			return false;
		if (d1.getSolutie().compareTo(d2.getSolutie()) != 0)
			return false;
		if (d1.getSolutieSumar().compareTo(d2.getSolutieSumar()) != 0)
			return false;
		if (d1.getDocumentSedinta() != null && d2.getDocumentSedinta() != null)
			if (d1.getDocumentSedinta().compareTo(d2.getDocumentSedinta()) != 0)
				return false;
		if (d1.getNumarDocument().compareTo(d2.getNumarDocument()) != 0)
			return false;

		String datm1 = d1.getData().get(Calendar.DAY_OF_MONTH) + "."
				+ d1.getData().get(Calendar.MONTH) + "."
				+ d1.getData().get(Calendar.YEAR);

		String datm2 = d2.getData().get(Calendar.DAY_OF_MONTH) + "."
				+ d2.getData().get(Calendar.MONTH) + "."
				+ d2.getData().get(Calendar.YEAR);

		if (datm1.compareTo(datm2) != 0)
			return false;

		if (d1.getDataDocument() != null && d2.getDataDocument() != null) {
			String dat1 = d1.getDataDocument().get(Calendar.DAY_OF_MONTH) + "."
					+ d1.getDataDocument().get(Calendar.MONTH) + "."
					+ d1.getDataDocument().get(Calendar.YEAR);

			String dat2 = d2.getDataDocument().get(Calendar.DAY_OF_MONTH) + "."
					+ d2.getDataDocument().get(Calendar.MONTH) + "."
					+ d2.getDataDocument().get(Calendar.YEAR);

			if (dat1.compareTo(dat2) != 0)
				return false;
		}

		return true;
	}

	public boolean compareParti(DosarParte d1, DosarParte d2) {
		if (d1.getNume().compareTo(d2.getNume()) != 0)
			return false;

		if (d1.getCalitateParte().compareTo(d2.getCalitateParte()) != 0)
			return false;

		return true;
	}

	public boolean compareCaiDeAtac(DosarCaleAtac d1, DosarCaleAtac d2) {
		if (d1.getParteDeclaratoare().compareTo(d2.getParteDeclaratoare()) != 0)
			return false;

		if (d1.getTipCaleAtac().compareTo(d2.getTipCaleAtac()) != 0)
			return false;

		if (d1.getDataDeclarare() != null && d2.getDataDeclarare() != null) {
			String dat1 = d1.getDataDeclarare().get(Calendar.DAY_OF_MONTH)
					+ "." + d1.getDataDeclarare().get(Calendar.MONTH) + "."
					+ d1.getDataDeclarare().get(Calendar.YEAR);

			String dat2 = d2.getDataDeclarare().get(Calendar.DAY_OF_MONTH)
					+ "." + d2.getDataDeclarare().get(Calendar.MONTH) + "."
					+ d2.getDataDeclarare().get(Calendar.YEAR);

			if (dat1.compareTo(dat2) != 0)
				return false;
		}
		return true;
	}

}