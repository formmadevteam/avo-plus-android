package calendarmanager.helpers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import calendaradapter.structures.SedintaCurenta;

public class AlarmDailyService extends Service {

	private Timer timer = new Timer();

	private PendingIntent pendingIntent;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public String convertDateToString(Calendar cal) {
		if (cal != null)
			return String.valueOf(cal.get(Calendar.YEAR) + "."
					+ (cal.get(Calendar.MONTH) + 1) + "."
					+ cal.get(Calendar.DAY_OF_MONTH));
		else
			return "";
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		Log.e("debug", "Service program a doua zi - started");
		
		ArrayList<String> listaa = new ArrayList<String>();
		Calendar currentDate = Calendar.getInstance();
		
		currentDate = Calendar.getInstance();
		currentDate.set(Calendar.HOUR_OF_DAY,
				currentDate.getActualMinimum(Calendar.HOUR_OF_DAY));
		currentDate.set(Calendar.MINUTE,
				currentDate.getActualMinimum(Calendar.MINUTE));
		currentDate.set(Calendar.SECOND,
				currentDate.getActualMinimum(Calendar.SECOND));
		currentDate.set(Calendar.MILLISECOND,
				currentDate.getActualMinimum(Calendar.MILLISECOND));

		currentDate.add(Calendar.DAY_OF_MONTH, 1);

		HelperSqlFunctions h = new HelperSqlFunctions(getApplicationContext());

		ArrayList<SedintaCurenta> lis = h.getListaSedinteByDate(currentDate);
		for (int i = 0; i < lis.size(); i++) {
			listaa.add(lis.get(i).getDosar());
		}
		
		
		Log.e("debug", "Aci");
		
		System.out.println(listaa.size()+" - marime");

		AlarmManager alarmManager = (AlarmManager) getApplicationContext()
				.getSystemService(getApplicationContext().ALARM_SERVICE);

		
		Intent intentt = new Intent(getApplicationContext(),
				AlarmDailyNotifService.class);
		intentt.putStringArrayListExtra("lista", listaa);
		intentt.putExtra("id_notif", 43);
		intentt.putExtra("size", listaa.size());
		
		intentt.putExtra("data", convertDateToString(currentDate));

		PendingIntent pendingIntent = PendingIntent.getService(
				getApplicationContext(), 43, intentt,
				PendingIntent.FLAG_UPDATE_CURRENT);
		alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
				pendingIntent);

		return START_STICKY;
	}

	private void startBackup() {

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
		}
	}

	protected Calendar convertDate(String data) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			Date d = format.parse(data);
			cal.setTime(d);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return cal;
	}

}