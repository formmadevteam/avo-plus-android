package calendarmanager.helpers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import calendarmanager.activities.DosarActivity;
import calendarmanager.activities.MainActivity;
import calendarmanager.activities.R;

public class BackupNotifService extends Service {

	private Timer timer = new Timer();

	private PendingIntent pendingIntent;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		// TODO retrive program a doua zi + notify it

		Intent resultIntent = new Intent(this, DosarActivity.class);
		if (intent != null) {
			final int idd = intent.getIntExtra("id_dos", -32);
			if (idd != -32) {
				resultIntent.putExtra("id_dos", idd);

				NotificationCompat.Builder mBuilder;

				mBuilder = new NotificationCompat.Builder(this)
						.setSmallIcon(R.drawable.ic_action_refresh)
						.setContentTitle("A aparut o modificare")
						.setContentText(
								"Dosar: " + intent.getStringExtra("numarDosar"))
						.setAutoCancel(true);

				TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
				// Adds the back stack for the Intent (but not the Intent
				// itself)
				stackBuilder.addParentStack(MainActivity.class);
				// Adds the Intent that starts the Activity to the top of the
				// stack
				stackBuilder.addNextIntent(resultIntent);
				PendingIntent resultPendingIntent = stackBuilder
						.getPendingIntent(
								intent.getIntExtra("id_notif", 42132),
								PendingIntent.FLAG_ONE_SHOT);
				mBuilder.setContentIntent(resultPendingIntent);
				NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
				// mId allows you to update the notification later on.
				mNotificationManager
						.notify(intent.getIntExtra("id_notif", 42132),
								mBuilder.build());

				return START_STICKY;
			} else {
				stopSelf();
				return 0;
			}
		}
		stopSelf();
		return 0;
	}

	private void startBackup() {

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
		}

		System.out.println("Destroyed");

		// Toast.makeText(this, "Service Destroyed", Toast.LENGTH_SHORT).show();
	}

	protected Calendar convertDate(String data) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			Date d = format.parse(data);
			cal.setTime(d);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return cal;
	}

}