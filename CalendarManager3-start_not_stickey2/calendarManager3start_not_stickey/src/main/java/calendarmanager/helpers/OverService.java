package calendarmanager.helpers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

import calendaradapter.structures.SedintaCurenta;
import calendarmanager.activities.MainActivity;
import calendarmanager.activities.R;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import ecris.data.types.Dosar;
import ecris.data.types.DosarSedinta;

public class OverService extends Service {

	private Timer timer = new Timer();

	private PendingIntent pendingIntent;

	private AlarmManager alarmMgr;
	private PendingIntent alarmIntent;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();
		startBackup();
		return START_STICKY;
	}

	public String convertDateToString(Calendar cal) {
		if (cal != null)
			return String.valueOf(cal.get(Calendar.YEAR) + "."
					+ (cal.get(Calendar.MONTH) + 1) + "."
					+ cal.get(Calendar.DAY_OF_MONTH));
		else
			return "";
	}

	public Calendar convertStringTimeToDate(String cal) {
		Calendar cale = Calendar.getInstance();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm");
			cale.setTime(sdf.parse(cal));
		} catch (Exception e) {
		}

		return cale;
	}

	private void startBackup() {

		Log.e("debug-erro", "Muie ba am trecut");

		AlarmManager alarmManager = (AlarmManager) getApplicationContext()
				.getSystemService(getApplicationContext().ALARM_SERVICE);

		HelperSqlFunctions h = new HelperSqlFunctions(getApplicationContext());
		ArrayList<Dosar> lis = h.getListaDosare();

		ArrayList<DosarSedinta> lista_sed = new ArrayList<DosarSedinta>();

		for (Dosar dosar : lis) {
			ArrayList<DosarSedinta> lista_sedinta_dosar = dosar.getSedinte();
			for (DosarSedinta dosarSedinta : lista_sedinta_dosar) {
				lista_sed.add(dosarSedinta);
			}
		}
		Calendar cuc = Calendar.getInstance();

		ArrayList<Calendar> lista_date = new ArrayList<Calendar>();
		for (DosarSedinta dosarSedinta : lista_sed) {
			if (dosarSedinta.getData().after(cuc))
				lista_date.add(dosarSedinta.getData());
		}

		for (int i = 0; i < lista_date.size(); i++) {
			for (int j = i + 1; j < lista_date.size(); j++) {
				if (lista_date.get(i).compareTo(lista_date.get(j)) == 0) {
					lista_date.remove(j);
				}
			}
		}
//
//		for (Calendar calendar : lista_date) {
//			System.out.println(calendar);
//		}

		int id_notif = 100;
		for (Calendar calendar : lista_date) {
			ArrayList<SedintaCurenta> liss = new ArrayList<SedintaCurenta>();
			liss = h.getListaSedinteByDate(calendar);

			SedintaCurenta s = new SedintaCurenta();

			int ok = 1;

			for (int i = 0; i < liss.size(); i++) {
				for (int j = i + 1; j < liss.size(); j++) {
					if (liss.get(i).getDosarSedinta().getOra()
							.compareTo(liss.get(j).getDosarSedinta().getOra()) == 0) {
						ok = 0;
					}
				}
			}

			for (int i = 0; i < liss.size(); i++) {
				for (int j = i + 1; j < liss.size(); j++) {
					if (liss.get(i).getInstitutie()
							.compareTo(liss.get(j).getInstitutie()) != 0) {
						ok = 0;
					}
				}
			}

			if (ok == 0) {

				Intent intentt = new Intent(getApplicationContext(),
						OverNotifService.class);
				intentt.putExtra("data", convertDateToString(calendar));
				intentt.putExtra("id_notif", id_notif);

				PendingIntent pendingIntent = PendingIntent.getService(
						getApplicationContext(), id_notif, intentt,
						PendingIntent.FLAG_UPDATE_CURRENT);
				alarmManager.set(AlarmManager.RTC_WAKEUP,
						System.currentTimeMillis(), pendingIntent);

				
				id_notif++;
				
				
//				Intent resultIntent = new Intent(this, MainActivity.class);
//				resultIntent.putExtra("data", convertDateToString(calendar));
//				NotificationCompat.Builder mBuilder;
//				mBuilder = new NotificationCompat.Builder(this)
//						.setSmallIcon(R.drawable.ic_action_refresh)
//						.setContentTitle("Exista o problema ")
//						.setContentText("Data: " + convertDateToString(calendar))
//						.setAutoCancel(true);
//				TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//				stackBuilder.addParentStack(MainActivity.class);
//				stackBuilder.addNextIntent(resultIntent);
//				PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
//						id_notif,
//						PendingIntent.FLAG_ONE_SHOT);
//				mBuilder.setContentIntent(resultPendingIntent);
//				NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//				mNotificationManager.notify(id_notif,
//						mBuilder.build());
//				
			}

		}

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
		}
	}

	protected Calendar convertDate(String data) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			Date d = format.parse(data);
			cal.setTime(d);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return cal;
	}

}