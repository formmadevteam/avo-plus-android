package calendarmanager.helpers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class BroadcastForStartPhone extends BroadcastReceiver {

	private AlarmManager alarmMgr;
	private PendingIntent alarmIntent;

	public boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnected()) {
			return true;
		}
		return false;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {

			Intent intent2 = new Intent(context,
					AlarmService.class);
			context.stopService(intent2);
			if (context.startService(intent2) == null) {
				context.startService(intent2);
			}
			
			SharedPreferences prefs = context.getSharedPreferences(
					"com.example.app", Context.MODE_PRIVATE);

			int hour = prefs.getInt("ora_update", 1);
			int minute = prefs.getInt("minut_update", 0);

			long delay = (hour * 60 + minute) * 60 * 1000;

			Intent intentt = new Intent(context, BackupService.class);
			PendingIntent pendingIntent = PendingIntent.getService(context,
					433, intentt, PendingIntent.FLAG_UPDATE_CURRENT);

			AlarmManager alarmManager = (AlarmManager) context
					.getSystemService(context.ALARM_SERVICE);

			context.stopService(intentt);
			alarmManager.cancel(pendingIntent);
			if (isOnline(context)) {
				alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
						System.currentTimeMillis(), delay, pendingIntent);
				Log.e("service started", "service started");
				System.out.println("update");
			}
		}
	}

}
