package calendarmanager.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class HelperSql extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "CalendarManagsereabutaoana.db";
	private static final int DATABASE_VERSION = 12;
	private String sql;

	public HelperSql(Context ctx) {
		super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		sql = "CREATE TABLE IF NOT EXISTS " + "OBSERVED" + "("
				+ "[id] [integer] PRIMARY KEY AUTOINCREMENT,"
				+ "[data_type] [integer](5) NOT NULL,"
				+ "[data_date] [date](11) NULL,"
				+ "[data_code] [char](100) NULL DEFAULT ('')" + ")";

		db.execSQL(sql);

		sql = "CREATE TABLE IF NOT EXISTS " + "ANUNTURI" + "("
				+ "[id] [integer] PRIMARY KEY AUTOINCREMENT,"
				+ "[data_denum] [char](30) NOT NULL,"
				+ "[data_obs] [char](255) NULL" + ")";

		db.execSQL(sql);

		sql = "CREATE TABLE IF NOT EXISTS `dosar` ("
				+ "[id] [integer] PRIMARY KEY AUTOINCREMENT,"
				+ "`nume` varchar(255) NOT NULL,"
				+ "`observatii` varchar(255) NOT NULL,"
				+ "`numar` varchar(255) NOT NULL,"
				+ "`numarVechi` varchar(255) NOT NULL,"
				+ "`data` date NOT NULL,"
				+ "`institutie` varchar(255) NOT NULL,"
				+ "`departament` varchar(255) NOT NULL,"
				+ "`categorieCaz` varchar(255) NOT NULL,"
				+ "`stadiuProcesual` varchar(255) NOT NULL,"
				+ "`obiect` varchar(255) NOT NULL,"
				+ "`dataModificare` date NOT NULL" + ")";

		db.execSQL(sql);

		sql = "CREATE TABLE IF NOT EXISTS `dosarcaleatac` ("
				+ "[id] [integer] PRIMARY KEY AUTOINCREMENT,"
				+ "`id_dosar` int(11) NOT NULL," + "`dataDeclarare` date,"
				+ "`parteDeclaratoare` varchar(255),"
				+ "`tipCaleAtac` varchar(255)" + ")";

		db.execSQL(sql);

		sql = "CREATE TABLE IF NOT EXISTS `dosarparte` ("
				+ "[id] [integer] PRIMARY KEY AUTOINCREMENT,"
				+ "`id_dosar` int(11) NOT NULL," + "`nume` varchar(255),"
				+ "`calitateParte` varchar(255)" + ")";

		db.execSQL(sql);

		sql = "CREATE TABLE IF NOT EXISTS `dosarsedinta` ("
				+ "[id] [integer] PRIMARY KEY AUTOINCREMENT,"
				+ "`id_dosar` int(11) NOT NULL," + "`complet` varchar(255),"
				+ "`data` date," + "`ora` varchar(50),"
				+ "`solutie` varchar(255)," + "`solutieSumar` varchar(255),"
				+ "`dataPronuntare` date," + "`documentSedinta` varchar(255),"
				+ "`numarDocument` varchar(255)," + "`dataDocument` date" + ")";

		db.execSQL(sql);

		sql = "CREATE TABLE IF NOT EXISTS `sedinta` ("
				+ "[id] [integer] PRIMARY KEY AUTOINCREMENT,"
				+ "`departament` varchar(255) NOT NULL,"
				+ "`complet` varchar(255) NOT NULL," + "`data` date NOT NULL,"
				+ "`ora` varchar(50) NOT NULL" + ")";

		db.execSQL(sql);

		sql = "CREATE TABLE IF NOT EXISTS `sedintadosar` ("
				+ "[id] [integer] PRIMARY KEY AUTOINCREMENT,"
				+ "`id_sedinta` int(11) NOT NULL,"
				+ "`numar` varchar(255) NOT NULL,"
				+ "`numarVechi` varchar(255) NOT NULL,"
				+ "`data` date NOT NULL," + "`ora` varchar(50) NOT NULL,"
				+ "`categorieCaz` varchar(255) NOT NULL,"
				+ "`stadiuProcesual` varchar(255) NOT NULL" + ")";

		db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onCreate(db);
	}

}