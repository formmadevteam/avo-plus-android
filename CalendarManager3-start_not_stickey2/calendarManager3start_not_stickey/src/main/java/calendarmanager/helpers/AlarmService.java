package calendarmanager.helpers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;
import ecris.data.types.Dosar;
import ecris.data.types.DosarSedinta;

public class AlarmService extends Service {

	private Timer timer = new Timer();

	private PendingIntent pendingIntent;

	private AlarmManager alarmMgr;
	private PendingIntent alarmIntent;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();
		startBackup();
		return START_STICKY;
	}

	public String convertDateToString(Calendar cal) {
		if (cal != null)
			return String.valueOf(cal.get(Calendar.YEAR) + "."
					+ (cal.get(Calendar.MONTH) + 1) + "."
					+ cal.get(Calendar.DAY_OF_MONTH));
		else
			return "";
	}

	public Calendar convertStringTimeToDate(String cal) {
		Calendar cale = Calendar.getInstance();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm");
			cale.setTime(sdf.parse(cal));
		} catch (Exception e) {
		}

		return cale;
	}

	private void startBackup() {

		Log.e("debug-erro", "Muie ba am trecut");

		Context context = getApplicationContext();

		SharedPreferences prefs = this.getSharedPreferences("com.example.app",
				Context.MODE_PRIVATE);

		int ora_prog = prefs.getInt("ora_prog", 18);
		int minut_prog = prefs.getInt("minut_prog", 0);

		int ora_updatepre1 = prefs.getInt("ora_prenot1", 1);
		int minut_updatepre1 = prefs.getInt("minut_prenot1", 0);

		int ora_updatepre2 = prefs.getInt("ora_prenot2", 0);
		int minut_updatepre2 = prefs.getInt("minut_prenot2", 15);

		Log.e("debug", ora_prog + " progo");
		Log.e("debug", minut_prog + " progm");

		Intent myIntent = new Intent(context, AlarmDailyService.class);

		alarmIntent = PendingIntent.getService(context,
				(int) System.currentTimeMillis(), myIntent, 0);

		Calendar cucur = Calendar.getInstance();
		cucur.setTimeInMillis(System.currentTimeMillis());

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.set(Calendar.HOUR_OF_DAY, ora_prog);
		calendar.set(Calendar.MINUTE, minut_prog);
		calendar.set(Calendar.SECOND,
				calendar.getActualMinimum(Calendar.SECOND));

		Log.e("debug", "Cale : " + calendar.toString());

		if (calendar.after(cucur)) {
			Log.e("debug", "Ici sa");

			AlarmManager alarmManager = (AlarmManager) context
					.getSystemService(context.ALARM_SERVICE);
			alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
					calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY,
					alarmIntent);
		}

		else {
			Log.e("debug", "Ici sa 2");

			AlarmManager alarmManager = (AlarmManager) context
					.getSystemService(context.ALARM_SERVICE);
			alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
					calendar.getTimeInMillis() + AlarmManager.INTERVAL_DAY,
					AlarmManager.INTERVAL_DAY, alarmIntent);
		}

		HelperSqlFunctions h = new HelperSqlFunctions(getApplicationContext());

		Calendar current = Calendar.getInstance();

		ArrayList<Dosar> lis = h.getListaDosare();

		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(context.ALARM_SERVICE);
		
		int id = 1;
		for (Dosar dosar : lis) {
			ArrayList<DosarSedinta> lista_sed = dosar.getSedinte();
			for (DosarSedinta dosarSedinta : lista_sed) {
				String datel = convertDateToString(dosarSedinta.getData())
						+ " " + dosarSedinta.getOra();

				Calendar curu = convertStringTimeToDate(datel);
				Calendar timeOff9 = Calendar.getInstance();
				timeOff9.setTimeInMillis(curu.getTimeInMillis());

				Calendar curu1 = Calendar.getInstance();
				curu1.setTimeInMillis(curu.getTimeInMillis());
				curu1.add(Calendar.HOUR_OF_DAY, -ora_updatepre1);
				curu1.add(Calendar.MINUTE, -minut_updatepre1);

				if (curu1.after(current)) {
					Intent i = new Intent(context, AlarmSedintaService.class);
					i.putExtra("numarDosar", dosar.getNumar());
					i.putExtra("id_notif", id);
					i.putExtra("before", 1);
					i.putExtra("before-time-ora", ora_updatepre1 + "");
					i.putExtra("before-time-min", minut_updatepre1 + "");

					timeOff9.add(Calendar.HOUR_OF_DAY, -ora_updatepre1);
					timeOff9.add(Calendar.MINUTE, -minut_updatepre1);

					PendingIntent pendingIntent = PendingIntent.getService(
							getApplicationContext(), id, i,
							PendingIntent.FLAG_UPDATE_CURRENT);

					alarmManager.set(AlarmManager.RTC_WAKEUP,
							timeOff9.getTimeInMillis(), pendingIntent);

					id++;
				}

				else {
					timeOff9.add(Calendar.HOUR_OF_DAY, -ora_updatepre1);
					timeOff9.add(Calendar.MINUTE, -minut_updatepre1);
				}

				Calendar curu2 = Calendar.getInstance();
				curu2.setTimeInMillis(curu.getTimeInMillis());
				curu2.add(Calendar.HOUR_OF_DAY, -ora_updatepre2);
				curu2.add(Calendar.MINUTE, -minut_updatepre2);

				if (curu2.after(current)) {

					Intent i2 = new Intent(context, AlarmSedintaService.class);
					i2.putExtra("numarDosar", dosar.getNumar());
					i2.putExtra("id_notif", id);
					i2.putExtra("before", 10);
					i2.putExtra("before-time-ora", ora_updatepre2 + "");
					i2.putExtra("before-time-min", minut_updatepre2 + "");

					System.out.println("probla2: " + ora_updatepre2 + " : "
							+ minut_updatepre2);

					timeOff9.add(Calendar.HOUR_OF_DAY, ora_updatepre1
							- ora_updatepre2);
					timeOff9.add(Calendar.MINUTE, minut_updatepre1
							- minut_updatepre2);

					pendingIntent = PendingIntent.getService(
							getApplicationContext(), id, i2,
							PendingIntent.FLAG_UPDATE_CURRENT);

					alarmManager.set(AlarmManager.RTC_WAKEUP,
							timeOff9.getTimeInMillis(), pendingIntent);

					id++;

				}
			}
		}

		Intent intentut = new Intent(getApplicationContext(), OverService.class);
		startService(intentut);

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
		}

		System.out.println("Destroyed");
		// Toast.makeText(this, "Service Destroyed", Toast.LENGTH_SHORT).show();
	}

	protected Calendar convertDate(String data) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			Date d = format.parse(data);
			cal.setTime(d);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return cal;
	}

}