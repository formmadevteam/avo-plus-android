package calendarmanager.helpers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import calendarmanager.activities.MainActivity;
import calendarmanager.activities.R;

public class OverNotifService extends Service {

	private Timer timer = new Timer();

	private PendingIntent pendingIntent;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		// TODO retrive program a doua zi + notify it
		SharedPreferences prefs = this.getSharedPreferences(
				"com.example.app", Context.MODE_PRIVATE);
		
		int ok_prog = prefs.getInt("ok_prob", 0);

		if (ok_prog != 0) {
			Intent resultIntent = new Intent(this, MainActivity.class);
			resultIntent.putExtra("data", intent.getStringExtra("data"));
			NotificationCompat.Builder mBuilder;
			mBuilder = new NotificationCompat.Builder(this)
					.setSmallIcon(R.drawable.ic_action_refresh)
					.setContentTitle("Exista o problema ")
					.setContentText("Data: " + intent.getStringExtra("data"))
					.setAutoCancel(true);
			TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
			stackBuilder.addParentStack(MainActivity.class);
			stackBuilder.addNextIntent(resultIntent);
			PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
					intent.getIntExtra("id_notif", 42132),
					PendingIntent.FLAG_ONE_SHOT);
			mBuilder.setContentIntent(resultPendingIntent);
			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.notify(intent.getIntExtra("id_notif", 42132),
					mBuilder.build());
			return START_STICKY;
		}
		else{
			stopSelf();
			return 0;
		}

	}

	private void startBackup() {

		// AlarmManager alarmManager = (AlarmManager)
		// getSystemService(ALARM_SERVICE);
		//
		// Calendar calendar2 = Calendar.getInstance();
		// calendar2.setTimeInMillis(System.currentTimeMillis());
		//
		// calendar.set(Calendar.HOUR_OF_DAY, 00);
		// calendar2.set(Calendar.MINUTE, 32);
		// calendar2.set(Calendar.SECOND, 0);
		//
		// Log.e("fdebug", calendar.toString());
		//
		// calendar.add(Calendar.SECOND, 10);
		//
		// alarmManager.set(AlarmManager.RTC_WAKEUP,
		// calendar.getTimeInMillis(), pendingIntent);

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
		}

		System.out.println("Destroyed");

		// Toast.makeText(this, "Service Destroyed", Toast.LENGTH_SHORT).show();
	}

	protected Calendar convertDate(String data) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			Date d = format.parse(data);
			cal.setTime(d);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return cal;
	}

}