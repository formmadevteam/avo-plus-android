package calendarmanager.helpers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import calendaradapter.structures.DosarSimplif;
import calendarmanager.activities.ListaDosareActivity;
import calendarmanager.activities.MainActivity;
import calendarmanager.notifications.UberNotificationManager;
import ecris.data.types.Dosar;
import ecris.data.types.DosarCaleAtac;
import ecris.data.types.DosarParte;
import ecris.data.types.DosarSedinta;

public class UpdateService extends Service {
	private Looper mServiceLooper;

	private ServiceHandler mServiceHandler;

	// Handler that receives messages from the thread
	private final class ServiceHandler extends Handler {
		public ServiceHandler(Looper looper) {
			super(looper);
		}

		protected Calendar convertDate(String data) {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat format = new SimpleDateFormat(
					"yyyy-MM-dd'T'HH:mm:ss");
			try {
				Date d = format.parse(data);
				cal.setTime(d);
			} catch (Throwable e) {
				e.printStackTrace();
			}
			return cal;
		}

		public boolean compareDosare(Dosar d1, Dosar d2) {
			// boolean ok = true;

			// ok = d1.equals(d2);

			if (d1.getNumar().compareTo(d2.getNumar()) != 0)
				return false;
			Log.e("debu", "0");
			if (d1.getNumarVechi().compareTo(d2.getNumarVechi()) != 0)
				return false;
			Log.e("debu", "1");

			String dat1 = d1.getData().get(Calendar.DAY_OF_MONTH) + "."
					+ d1.getData().get(Calendar.MONTH) + "."
					+ d1.getData().get(Calendar.YEAR);

			String dat2 = d2.getData().get(Calendar.DAY_OF_MONTH) + "."
					+ d2.getData().get(Calendar.MONTH) + "."
					+ d2.getData().get(Calendar.YEAR);

			if (dat1.compareTo(dat2) != 0)
				return false;
			if (d1.getInstitutie().compareTo(d2.getInstitutie()) != 0)
				return false;
			Log.e("debu", "3");
			if (d1.getDepartament().compareTo(d2.getDepartament()) != 0)
				return false;
			Log.e("debu", "4");
			if (d1.getCategorieCaz().compareTo(d2.getCategorieCaz()) != 0)
				return false;
			Log.e("debu", "5");
			if (d1.getStadiuProcesual().compareTo(d2.getStadiuProcesual()) != 0)
				return false;
			Log.e("debu", "6");
			if (d1.getObiect().compareTo(d2.getObiect()) != 0)
				return false;
			Log.e("debu", "7");

			String datm1 = d1.getDataModificare().get(Calendar.DAY_OF_MONTH)
					+ "." + d1.getDataModificare().get(Calendar.MONTH) + "."
					+ d1.getDataModificare().get(Calendar.YEAR);

			String datm2 = d2.getDataModificare().get(Calendar.DAY_OF_MONTH)
					+ "." + d2.getDataModificare().get(Calendar.MONTH) + "."
					+ d2.getDataModificare().get(Calendar.YEAR);

			if (datm1.compareTo(datm2) != 0)
				return false;

			if (d1.getSedinte().size() == d2.getSedinte().size()) {
				for (int i = 0; i < d1.getSedinte().size(); i++) {
					if (!compareSedinte(d1.getSedinte().get(i), d2.getSedinte()
							.get(i))) {
						return false;
					}
				}
			} else {
				return false;
			}

			// if (d1.getSedinte().size() != d2.getSedinte().size())
			// return false;
			Log.e("debu", "9");

			if (d1.getParti().size() == d2.getParti().size()) {
				for (int i = 0; i < d1.getParti().size(); i++) {
					if (!compareParti(d1.getParti().get(i), d2.getParti()
							.get(i))) {
						return false;
					}
				}
			} else {
				return false;
			}

			// if (d1.getParti().size() != d2.getParti().size())
			// return false;
			Log.e("debu", "8");
			//

			if (d1.getCaiAtac() != null && d2.getCaiAtac() != null) {
				if (d1.getCaiAtac().size() == d2.getCaiAtac().size()) {
					for (int i = 0; i < d1.getCaiAtac().size(); i++) {
						if (!compareCaiDeAtac(d1.getCaiAtac().get(i), d2
								.getCaiAtac().get(i))) {
							return false;
						}
					}
				} else {
					return false;
				}
			}

			// if (d1.getCaiAtac() != null && d2.getCaiAtac() != null)
			// if (d1.getCaiAtac().size() != d2.getCaiAtac().size())
			// return false;

			Log.e("debu", "10");

			return true;
		}

		public boolean compareSedinte(DosarSedinta d1, DosarSedinta d2) {
			Log.e("frbu", "Zbeng");
			if (d1.getComplet().compareTo(d2.getComplet()) != 0)
				return false;
			if (d1.getOra().compareTo(d2.getOra()) != 0)
				return false;
			if (d1.getSolutie().compareTo(d2.getSolutie()) != 0)
				return false;
			if (d1.getSolutieSumar().compareTo(d2.getSolutieSumar()) != 0)
				return false;
			if (d1.getDocumentSedinta() != null
					&& d2.getDocumentSedinta() != null)
				if (d1.getDocumentSedinta().compareTo(d2.getDocumentSedinta()) != 0)
					return false;
			if (d1.getNumarDocument().compareTo(d2.getNumarDocument()) != 0)
				return false;

			String datm1 = d1.getData().get(Calendar.DAY_OF_MONTH) + "."
					+ d1.getData().get(Calendar.MONTH) + "."
					+ d1.getData().get(Calendar.YEAR);

			String datm2 = d2.getData().get(Calendar.DAY_OF_MONTH) + "."
					+ d2.getData().get(Calendar.MONTH) + "."
					+ d2.getData().get(Calendar.YEAR);

			if (datm1.compareTo(datm2) != 0)
				return false;

			if (d1.getDataDocument() != null && d2.getDataDocument() != null) {
				String dat1 = d1.getDataDocument().get(Calendar.DAY_OF_MONTH)
						+ "." + d1.getDataDocument().get(Calendar.MONTH) + "."
						+ d1.getDataDocument().get(Calendar.YEAR);

				String dat2 = d2.getDataDocument().get(Calendar.DAY_OF_MONTH)
						+ "." + d2.getDataDocument().get(Calendar.MONTH) + "."
						+ d2.getDataDocument().get(Calendar.YEAR);

				if (dat1.compareTo(dat2) != 0)
					return false;
			}

			return true;
		}

		public boolean compareParti(DosarParte d1, DosarParte d2) {
			if (d1.getNume().compareTo(d2.getNume()) != 0)
				return false;

			if (d1.getCalitateParte().compareTo(d2.getCalitateParte()) != 0)
				return false;

			return true;
		}

		public boolean compareCaiDeAtac(DosarCaleAtac d1, DosarCaleAtac d2) {
			if (d1.getParteDeclaratoare().compareTo(d2.getParteDeclaratoare()) != 0)
				return false;

			if (d1.getTipCaleAtac().compareTo(d2.getTipCaleAtac()) != 0)
				return false;

			if (d1.getDataDeclarare() != null && d2.getDataDeclarare() != null) {
				String dat1 = d1.getDataDeclarare().get(Calendar.DAY_OF_MONTH)
						+ "." + d1.getDataDeclarare().get(Calendar.MONTH) + "."
						+ d1.getDataDeclarare().get(Calendar.YEAR);

				String dat2 = d2.getDataDeclarare().get(Calendar.DAY_OF_MONTH)
						+ "." + d2.getDataDeclarare().get(Calendar.MONTH) + "."
						+ d2.getDataDeclarare().get(Calendar.YEAR);

				if (dat1.compareTo(dat2) != 0)
					return false;
			}
			return true;
		}

		@Override
		public void handleMessage(Message msg) {
					HelperSqlFunctions f = new HelperSqlFunctions(
					getApplicationContext());
			ArrayList<DosarSimplif> list = f.getListaDosareSimplif();

			EcrisUtils e = new EcrisUtils();

			Log.e("debu", "Incepem: " + list.size());
			for (DosarSimplif dosarSimplif : list) {

				ArrayList<Dosar> dosee = f.getListaDosareNumar(dosarSimplif
						.getNumar());

				dosee.get(0).setObiect("Puii mei");

				ArrayList<Dosar> dose = e.getFahrenheitList(
						dosarSimplif.getNumar(), getApplicationContext());

				if (dose==null || dosee==null){
					stopSelf(msg.arg1);
					return;
					
				}

				for (int i = 0; i < dosee.size(); i++) {
					for (int j = 0; j < dose.size(); j++) {
						if (dosee.get(i).getInstitutie()
								.compareTo(dose.get(j).getInstitutie()) == 0) {
							Log.e("debug", "Aproape bun tati");
							if (compareDosare(dosee.get(i), dose.get(j))) {
								Log.e("debug", "Esti bun tati");
							} else {
								// TODO notificare update + update in DB
								Log.e("debug", "Au aparut modificari");
							}
						}
					}
				}
			}
			stopSelf(msg.arg1);
		}
	}

	@Override
	public void onCreate() {
		HandlerThread thread = new HandlerThread("ServiceStartArguments");
		thread.start();
		mServiceLooper = thread.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper);

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		Message msg = mServiceHandler.obtainMessage();
		msg.arg1 = startId;
		mServiceHandler.sendMessage(msg);
		
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {
	}
}
