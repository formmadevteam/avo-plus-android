package calendarmanager.helpers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import ecris.data.types.Dosar;
import ecris.data.types.DosarCaleAtac;
import ecris.data.types.DosarParte;
import ecris.data.types.DosarSedinta;

public class EcrisUtils {

	/**
	 * 8441/296/2014
	 * 
	 */

	private final String NAMESPACE = "portalquery.just.ro";

	private final String URL = "http://portalquery.just.ro/query.asmx";

	private final String SOAP_ACTION = "portalquery.just.ro/CautareDosare";

	private final String METHOD_NAME = "CautareDosare";
	
	private final String METHOD_NAME2 = "CautareDosare2";

	
	public static String getMonthByCalendar(Calendar c) {
		// String str;
		switch (c.get(Calendar.MONTH)) {
		case 0:
			return "Ianuarie";
		case 1:
			return "Februarie";
		case 2:
			return "Martie";
		case 3:
			return "Aprilie";
		case 4:
			return "Mai";
		case 5:
			return "Iunie";
		case 6:
			return "Iulie";
		case 7:
			return "August";
		case 8:
			return "Septembrie";
		case 9:
			return "Octombrie";
		case 10:
			return "Noiembrie";
		case 11:
			return "Decembrie";

		default:
			return "";
		}

	}

	public static String getDayByCalendar(Calendar c) {
		switch (c.get(Calendar.DAY_OF_WEEK)) {
		case Calendar.MONDAY:
			return "Luni";
		case Calendar.TUESDAY:
			return "Marti";
		case Calendar.WEDNESDAY:
			return "Miercuri";
		case Calendar.THURSDAY:
			return "Joi";
		case Calendar.FRIDAY:
			return "Vineri";
		case Calendar.SATURDAY:
			return "Sambata";
		case Calendar.SUNDAY:
			return "Duminica";

		default:
			return "";
		}

	}

	public void afisareDosar(Dosar dosar) {
		// TODO Auto-generated method stub

		Log.e("dosar", "Numar: " + dosar.getNumar());
		Log.e("dosar", "NumarVechi: " + dosar.getNumarVechi());
		Log.e("dosar", "Data: " + dosar.getData());
		Log.e("dosar", "Institutie: " + dosar.getInstitutie());
		Log.e("dosar", "Departament: " + dosar.getDepartament());
		Log.e("dosar", "CategorieCaz: " + dosar.getCategorieCaz());
		Log.e("dosar", "StadiuProcesual: " + dosar.getStadiuProcesual());
		Log.e("dosar", "Obiect: " + dosar.getObiect());
		Log.e("dosar", "DataModificare: " + dosar.getDataModificare().getTime());
		Log.e("dosar", "Nr parti: " + dosar.getParti().size());

	}

	protected Calendar convertDate(String data) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			Date d = format.parse(data);
			cal.setTime(d);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return cal;
	}

	protected void parsareSimplificatRaspunsDosare(Dosar dosar, SoapObject response) {
		
		
	}
	
	protected void parsareRaspunsDosare(Dosar dosar, SoapObject response) {

		SoapObject prop = response;

		String numar = prop.getProperty("numar").toString();
		dosar.setNumar(numar);
		String numarVechi = prop.getProperty("numarVechi").toString();
		dosar.setNumarVechi(numarVechi);
		String data = prop.getProperty("data").toString();
		dosar.setData(convertDate(data));
		String institutie = prop.getProperty("institutie").toString();
		dosar.setInstitutie(institutie);
		String departament = prop.getProperty("departament").toString();
		dosar.setDepartament(departament);
		String categorieCaz = prop.getProperty("categorieCaz").toString();
		dosar.setCategorieCaz(categorieCaz);
		String stadiuProcesual = prop.getProperty("stadiuProcesual").toString();
		dosar.setStadiuProcesual(stadiuProcesual);
		String obiect = prop.getProperty("obiect").toString();
		dosar.setObiect(obiect);
		String dataModificare = prop.getProperty("dataModificare").toString();
		dosar.setDataModificare(convertDate(dataModificare));

		SoapObject parti = (SoapObject) prop.getPropertySafely("parti", null);

		if (parti != null) {

			ArrayList<DosarParte> listaParti = new ArrayList<DosarParte>();

			for (int i = 0; i < parti.getPropertyCount(); i++) {
				SoapObject caractParti = (SoapObject) parti.getProperty(i);
				String nume = caractParti.getProperty("nume").toString();
				String calitateParte = caractParti.getProperty("calitateParte")
						.toString();
				DosarParte dosarParte = new DosarParte();
				dosarParte.setNume(nume);
				dosarParte.setCalitateParte(calitateParte);
				listaParti.add(dosarParte);
			}
			dosar.setParti(listaParti);

		}
		SoapObject sedinte = (SoapObject) prop.getPropertySafely("sedinte",
				null);
		// SoapObject dosareCaiAtac = (SoapObject)
		// caiAtac.getProperty("DosarCaleAtac");
		if (sedinte != null) {

			// SoapObject sedinte = (SoapObject) prop.getProperty("sedinte");

			ArrayList<DosarSedinta> listaSedinte = new ArrayList<DosarSedinta>();

			for (int i = 0; i < sedinte.getPropertyCount(); i++) {
				SoapObject caractSedinte = (SoapObject) sedinte.getProperty(i);

				String complet = caractSedinte.getProperty("complet")
						.toString();
				Calendar dataS = convertDate(caractSedinte.getProperty("data")
						.toString());
				String ora = caractSedinte.getProperty("ora").toString();
				String solutie = caractSedinte.getProperty("solutie")
						.toString();
				String solutieSumar = caractSedinte.getProperty("solutieSumar")
						.toString();

				Calendar dataPronuntare = null;
				if (caractSedinte.getProperty("dataPronuntare") != null)
					dataPronuntare = convertDate(caractSedinte.getProperty(
							"dataPronuntare").toString());

				String documentSedinta = null;
				if (caractSedinte.getProperty("documentSedinta") != null)
					documentSedinta = caractSedinte.getProperty(
							"documentSedinta").toString();

				String numarDocument = caractSedinte.getProperty(
						"numarDocument").toString();

				Calendar dataDocument = null;
				if (caractSedinte.getProperty("dataDocument") != null)
					dataDocument = convertDate(caractSedinte.getProperty(
							"dataDocument").toString());

				DosarSedinta dosarSedinta = new DosarSedinta();
				dosarSedinta.setComplet(complet);
				dosarSedinta.setData(dataS);
				dosarSedinta.setOra(ora);
				dosarSedinta.setSolutie(solutie);
				dosarSedinta.setSolutieSumar(solutieSumar);
				dosarSedinta.setDataPronuntare(dataPronuntare);
				dosarSedinta.setDocumentSedinta(documentSedinta);
				dosarSedinta.setNumarDocument(numarDocument);
				dosarSedinta.setDataDocument(dataDocument);

				listaSedinte.add(dosarSedinta);
			}
			dosar.setSedinte(listaSedinte);

		}

		SoapObject caiAtac = (SoapObject) prop.getPropertySafely("caiAtac",
				null);
		// SoapObject dosareCaiAtac = (SoapObject)
		// caiAtac.getProperty("DosarCaleAtac");
		if (caiAtac != null) {
			ArrayList<DosarCaleAtac> listaCaiAtac = new ArrayList<DosarCaleAtac>();

			for (int i = 0; i < caiAtac.getPropertyCount(); i++) {
				SoapObject caractCaleAtac = (SoapObject) caiAtac.getProperty(i);

				// private Calendar dataDeclarare;
				// private String parteDeclaratoare;
				// private String tipCaleAtac;

				Calendar dataDeclarare = convertDate(caractCaleAtac
						.getProperty("dataDeclarare").toString());

				String parteDeclaratoare = caractCaleAtac.getProperty(
						"parteDeclaratoare").toString();

				String tipCaleAtac = caractCaleAtac.getProperty("tipCaleAtac")
						.toString();

				DosarCaleAtac dosarCaleAtac = new DosarCaleAtac();
				dosarCaleAtac.setDataDeclarare(dataDeclarare);
				dosarCaleAtac.setParteDeclaratoare(parteDeclaratoare);
				dosarCaleAtac.setTipCaleAtac(tipCaleAtac);

				listaCaiAtac.add(dosarCaleAtac);
			}

			dosar.setCaiAtac(listaCaiAtac);
		}

	}

	String xml_site = "http://portalquery.just.ro/query.asmx?WSDL";
	String xml_request;
	ArrayList<Dosar> listaDosareCurente;

	public ArrayList<Dosar> getFahrenheitByName(String celsius, Context context) {

		// Log.e("eroare", "Android: in function");

		// Create request
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

		// Property which holds input parameters
		PropertyInfo celsiusPI = new PropertyInfo();
		// Set Name
		// celsiusPI.setName("numarDosar");
		celsiusPI.setName("numeParte");
		// Set Value
		celsiusPI.setValue(celsius);
		// Set dataType
		celsiusPI.setType(String.class);
		// Add the property to request object
		request.addProperty(celsiusPI);
		// Create envelope
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;

		// Set output SOAP object
		envelope.setOutputSoapObject(request);
		// Create HTTP call object
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			// Invole web service
			androidHttpTransport.call(SOAP_ACTION, envelope);

			SoapObject response = (SoapObject) envelope.getResponse();

			EcrisUtils e = new EcrisUtils();

			listaDosareCurente = new ArrayList<Dosar>();

			int i = 0;

			while (i < response.getPropertyCount()) {
				Dosar dosar = new Dosar();
				parsareRaspunsDosare(dosar,
						(SoapObject) response.getProperty(i));
				listaDosareCurente.add(dosar);
				i++;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return listaDosareCurente;
	}

	public int getFahrenheit(String celsius, Context context) {

		int ok = 0;

		// Log.e("eroare", "Android: in function");

		// Create request
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

		// Property which holds input parameters
		PropertyInfo celsiusPI = new PropertyInfo();
		// Set Name
		celsiusPI.setName("numarDosar");
		// celsiusPI.setName("numeParte");
		// Set Value
		celsiusPI.setValue(celsius);
		// Set dataType
		celsiusPI.setType(String.class);
		// Add the property to request object
		request.addProperty(celsiusPI);
		// Create envelope
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;

		// Set output SOAP object
		envelope.setOutputSoapObject(request);
		// Create HTTP call object
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			// Invole web service
			androidHttpTransport.call(SOAP_ACTION, envelope);

			// Get the response
			// SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			// if(response != null)
			// Assign it to fahren static variable
			// Log.e("buze", envelope.getResponse()+"");
			SoapObject response = (SoapObject) envelope.getResponse();

			// Log.e("debug", String.valueOf(envelope.getResponse())+"");

			EcrisUtils e = new EcrisUtils();
			Dosar dosar = new Dosar();
			listaDosareCurente = new ArrayList<Dosar>();
			int i = 0;
			if (response.getPropertyCount() != 0) {
				ok = 1;
			} else {
				return 0;
			}
			while (i < response.getPropertyCount()) {
				parsareRaspunsDosare(dosar,
						(SoapObject) response.getProperty(i));

				HelperSqlFunctions h = new HelperSqlFunctions(context);
				h.insertDosar(dosar);

				// listaDosareCurente.add(dosar);
				// afisareDosar(listaDosareCurente.get(i));
				i++;
			}

			// afisareDosar(listaDosareCurente.get(0));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return ok;
	}

	public boolean isOnline(Context context) {
		try {
			Process p1 = java.lang.Runtime.getRuntime().exec(
					"ping -c 1 www.google.com");
			int returnVal = p1.waitFor();
			boolean reachable = (returnVal == 0);
			return reachable;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public ArrayList<Dosar> getFahrenheitList(String celsius, Context context) {

		// Log.e("eroare", "Android: in function");

		// Create request
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

		// Property which holds input parameters
		PropertyInfo celsiusPI = new PropertyInfo();
		// Set Name
		celsiusPI.setName("numarDosar");
		// celsiusPI.setName("numeParte");
		// Set Value
		celsiusPI.setValue(celsius);
		// Set dataType
		celsiusPI.setType(String.class);
		// Add the property to request object
		request.addProperty(celsiusPI);
		// Create envelope
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;

		// Set output SOAP object
		envelope.setOutputSoapObject(request);
		// Create HTTP call object
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {

			SoapObject response = null;
			if (isOnline(context)) {
				// Invole web service
				androidHttpTransport.call(SOAP_ACTION, envelope);

				// Get the response
				// SoapPrimitive response = (SoapPrimitive)
				// envelope.getResponse();
				// if(response != null)
				// Assign it to fahren static variable
				// Log.e("buze", envelope.getResponse()+"");
				response = (SoapObject) envelope.getResponse();
			}
			// Log.e("debug", String.valueOf(envelope.getResponse())+"");

			EcrisUtils e = new EcrisUtils();
			listaDosareCurente = new ArrayList<Dosar>();
			int i = 0;
			if (response != null) {
				while (i < response.getPropertyCount()) {

					Dosar dosar = new Dosar();

					parsareRaspunsDosare(dosar,
							(SoapObject) response.getProperty(i));

					// HelperSqlFunctions h = new HelperSqlFunctions(context);
					// h.insertDosar(dosar);

					listaDosareCurente.add(dosar);
					// afisareDosar(listaDosareCurente.get(i));
					i++;
				}
			}
			// afisareDosar(listaDosareCurente.get(0));

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return listaDosareCurente;
	}
	

	public ArrayList<Dosar> getFahrenheitListForList(ArrayList<String> celsius, Context context) {

		// Log.e("eroare", "Android: in function");

		// Create request
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
		
		for (String string : celsius) {
			PropertyInfo celsiusPI = new PropertyInfo();
			celsiusPI.setName("numarDosar");
			celsiusPI.setValue(string);
			celsiusPI.setType(String.class);
			request.addProperty(celsiusPI);
			Log.e("deni", "bagg");
		}
		
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;

		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {

			SoapObject response = null;
			if (isOnline(context)) {
			androidHttpTransport.call(SOAP_ACTION, envelope);
			
				response = (SoapObject) envelope.getResponse();
				Log.e("desf", response.toString());
			}
			EcrisUtils e = new EcrisUtils();
			listaDosareCurente = new ArrayList<Dosar>();
			int i = 0;
			if (response != null) {
				while (i < response.getPropertyCount()) {

					Dosar dosar = new Dosar();

					parsareRaspunsDosare(dosar,
							(SoapObject) response.getProperty(i));
					listaDosareCurente.add(dosar);
					i++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return listaDosareCurente;
	}
	
	

}
