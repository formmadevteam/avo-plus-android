package calendarmanager.activities;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TimePicker;
import calendarmanager.helpers.AlarmService;
import calendarmanager.helpers.BackupService;

public class SetariNotificariActivity extends FragmentActivity {

	/*
	 * TODO
	 * 
	 * Setare ora update dimineata : ora 7 Setare ora notificare inainte procese
	 * in curs : 1 ora + 15 min Setare ora prenotificare program cu o zi in urma
	 * : 18:00 Setare ora minima intre sedinte ; 1 ora
	 */

	public boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		// should check null because in air plan mode it will be null
		if (netInfo != null && netInfo.isConnected()) {
			return true;
		}
		return false;
	}

	private ProgressDialog dialog2;

	public boolean verificaCont(int numar) {
		if (numar % 2 == 0)
			return true;
		return false;
	}

	EditText edt1;
	static SharedPreferences prefs;
	static SharedPreferences.Editor editor;
	static Button btn;
	static Button btn2;
	static Button btn3;
	static Button btn4;
	static Button btn5;
	static Button btn6;

	public void onToggleClicked1(View view) {
		// Is the toggle on?
		boolean on = ((Switch) view).isChecked();

		if (on) {
			editor.putInt("ok_prob", 1);

		} else {
			editor.putInt("ok_prob", 0);
		}

	}

	public void onToggleClicked2(View view) {
		// Is the toggle on?
		boolean on = ((Switch) view).isChecked();
		if (on) {
			editor.putInt("ok_pros", 1);
		} else {
			editor.putInt("ok_pros", 0);
		}
	}

	public void onToggleClicked3(View view) {
		// Is the toggle on?
		boolean on = ((Switch) view).isChecked();

		if (on) {
			editor.putInt("ok_prog", 1);
		} else {
			editor.putInt("ok_prog", 0);
		}
	}

	public Switch sw1;
	public Switch sw2;
	public Switch sw3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_setari_notificari);

		dialog2 = new ProgressDialog(SetariNotificariActivity.this);

		prefs = this.getSharedPreferences("com.example.app",
				Context.MODE_PRIVATE);
		editor = prefs.edit();

		sw1 = (Switch) findViewById(R.id.togglebutton1);
		sw2 = (Switch) findViewById(R.id.togglebutton2);
		sw3 = (Switch) findViewById(R.id.togglebutton3);

		int ok_prob = prefs.getInt("ok_prob", 0);
		int ok_prog = prefs.getInt("ok_prog", 0);
		int ok_pros = prefs.getInt("ok_pros", 0);

		if (ok_prob != 0)
			sw1.setChecked(true);
		else
			sw1.setChecked(false);

		if (ok_pros != 0)
			sw2.setChecked(true);
		else
			sw2.setChecked(false);

		if (ok_prog != 0)
			sw3.setChecked(true);
		else
			sw3.setChecked(false);

		int ora_update_n = prefs.getInt("ora_update", 0);
		int minut_update_n = prefs.getInt("minut_update", 15);

		int ora_updatepre1_n = prefs.getInt("ora_prenot1", 1);
		int minut_updatepre1_n = prefs.getInt("minut_prenot1", 0);

		int ora_updatepre2_n = prefs.getInt("ora_prenot2", 0);
		int minut_updatepre2_n = prefs.getInt("minut_prenot2", 15);

		int ora_prog_n = prefs.getInt("ora_prog", 18);
		int minut_prog_n = prefs.getInt("minut_prog", 0);

		String ora_update;
		String minut_update;
		String ora_updatepre1;
		String minut_updatepre1;
		String ora_updatepre2;
		String minut_updatepre2;
		String ora_prog;
		String minut_prog;

		if (ora_update_n < 10)
			ora_update = "0" + ora_update_n;
		else
			ora_update = ora_update_n + "";

		if (minut_update_n < 10)
			minut_update = "0" + minut_update_n;
		else
			minut_update = minut_update_n + "";
		//
		if (ora_updatepre1_n < 10)
			ora_updatepre1 = "0" + ora_updatepre1_n;
		else
			ora_updatepre1 = ora_updatepre1_n + "";

		if (minut_updatepre1_n < 10)
			minut_updatepre1 = "0" + minut_updatepre1_n;
		else
			minut_updatepre1 = minut_updatepre1_n + "";
		//

		if (ora_updatepre2_n < 10)
			ora_updatepre2 = "0" + ora_updatepre2_n;
		else
			ora_updatepre2 = ora_updatepre2_n + "";

		if (minut_updatepre2_n < 10)
			minut_updatepre2 = "0" + minut_updatepre2_n;
		else
			minut_updatepre2 = minut_updatepre2_n + "";

		//
		if (ora_prog_n < 10)
			ora_prog = "0" + ora_prog_n;
		else
			ora_prog = ora_prog_n + "";

		if (minut_prog_n < 10)
			minut_prog = "0" + minut_prog_n;
		else
			minut_prog = minut_prog_n + "";

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");

		Switch sw1 = (Switch) findViewById(R.id.togglebutton1);
		sw1.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub

				if (isChecked) {
					editor.putInt("ok_prob", 1);

				} else {
					editor.putInt("ok_prob", 0);
				}

			}
		});

		Switch sw2 = (Switch) findViewById(R.id.togglebutton2);
		sw2.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub

				if (isChecked) {
					editor.putInt("ok_pros", 1);

				} else {
					editor.putInt("ok_pros", 0);
				}

			}
		});

		Switch sw3 = (Switch) findViewById(R.id.togglebutton3);
		sw3.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub

				if (isChecked) {
					editor.putInt("ok_prog", 1);

				} else {
					editor.putInt("ok_prog", 0);
				}

			}
		});

		btn = (Button) findViewById(R.id.time_pick_update);
		btn.setText(ora_update + ":" + minut_update);
		btn.setTypeface(tf);
		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showTimePickerDialog(v, 0);
			}
		});

		btn2 = (Button) findViewById(R.id.time_pick_1);
		btn2.setText(ora_updatepre1 + ":" + minut_updatepre1);
		btn2.setTypeface(tf);
		btn2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showTimePickerDialog(v, 1);
			}
		});

		btn3 = (Button) findViewById(R.id.time_pick_2);
		btn3.setText(ora_updatepre2 + ":" + minut_updatepre2);
		btn3.setTypeface(tf);
		btn3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showTimePickerDialog(v, 2);
			}
		});

		btn4 = (Button) findViewById(R.id.time_pick_prenot);
		btn4.setText(ora_prog + ":" + minut_prog);
		btn4.setTypeface(tf);
		btn4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showTimePickerDialog(v, 3);
			}
		});

		// 8 ore

		btn5 = (Button) findViewById(R.id.time_pick_save);
		btn5.setTypeface(tf);
		btn5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editor.apply();
				// TODO restart service: Alarm + Backup
				// Intent intent2 = new Intent(getApplicationContext(),
				// AlarmService.class);
				// stopService(intent2);
				// if (startService(intent2) == null) {
				// getApplicationContext().startService(intent2);
				// System.out.println("eci");
				// // Log.d("service started", "service startedMain");
				// }
				//
				// Context context = getApplicationContext();
				//
				// SharedPreferences prefs = context.getSharedPreferences(
				// "com.example.app", Context.MODE_PRIVATE);
				//
				// int hour = prefs.getInt("ora_update", 1);
				// int minute = prefs.getInt("minut_update", 0);
				//
				// long delay = (hour * 60 + minute) * 60 * 1000;
				//
				// Intent intentt = new Intent(context, BackupService.class);
				// PendingIntent pendingIntent =
				// PendingIntent.getService(context,
				// 433, intentt, PendingIntent.FLAG_UPDATE_CURRENT);
				//
				// AlarmManager alarmManager = (AlarmManager) context
				// .getSystemService(context.ALARM_SERVICE);
				//
				// context.stopService(intentt);
				// alarmManager.cancel(pendingIntent);
				// if (isOnline(context)) {
				// alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				// System.currentTimeMillis(), delay, pendingIntent);
				//
				// }

				TaskSetSettings t = new TaskSetSettings();
				t.execute();

				finish();
			}
		});

		ImageView imm = (ImageView) findViewById(R.id.back_dosar_pic);
		imm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	public static class TimePickerFragment extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current time as the default values for the picker
			final Calendar c = Calendar.getInstance();
			// int hour = c.get(Calendar.HOUR_OF_DAY);
			// int minute = c.get(Calendar.MINUTE);

			int hour = prefs.getInt("ora_update", 15);
			int minute = prefs.getInt("minut_update", 0);

			// Create a new instance of TimePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute, true);
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// Do something with the time chosen by the user

			editor.putInt("ora_update", hourOfDay);
			editor.putInt("minut_update", minute);

			// int ora_update = prefs.getInt("ora_update", 8);
			// int minut_update = prefs.getInt("minut_update", 0);
			String h;
			String m;
			if (hourOfDay < 10)
				h = "0" + hourOfDay;
			else
				h = hourOfDay + "";
			if (minute < 10)
				m = "0" + minute;
			else
				m = minute + "";
			btn.setText(h + ":" + m);

		}
	}

	private class TaskSetSettings extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			Intent intent2 = new Intent(getApplicationContext(),
					AlarmService.class);
			stopService(intent2);
			if (startService(intent2) == null) {
				getApplicationContext().startService(intent2);
				System.out.println("eci");
				// Log.d("service started", "service startedMain");
			}

			Context context = getApplicationContext();

			SharedPreferences prefs = context.getSharedPreferences(
					"com.example.app", Context.MODE_PRIVATE);

			int hour = prefs.getInt("ora_update", 1);
			int minute = prefs.getInt("minut_update", 0);

			long delay = (hour * 60 + minute) * 60 * 1000;

			Intent intentt = new Intent(context, BackupService.class);
			PendingIntent pendingIntent = PendingIntent.getService(context,
					433, intentt, PendingIntent.FLAG_UPDATE_CURRENT);

			AlarmManager alarmManager = (AlarmManager) context
					.getSystemService(context.ALARM_SERVICE);

			context.stopService(intentt);
			alarmManager.cancel(pendingIntent);
			if (isOnline(context)) {
				alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
						System.currentTimeMillis(), delay, pendingIntent);

			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					dialog2.dismiss();
				}
			});
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					dialog2.setMessage("Se salveaza datele...");
					dialog2.setCancelable(false);
					dialog2.show();
				}
			});
		}
	}

	public static class TimePickerFragment2 extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current time as the default values for the picker
			final Calendar c = Calendar.getInstance();
			// int hour = c.get(Calendar.HOUR_OF_DAY);
			// int minute = c.get(Calendar.MINUTE);

			int hour = prefs.getInt("ora_prenot1", 1);
			int minute = prefs.getInt("minut_prenot1", 0);

			// Create a new instance of TimePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute, true);
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// Do something with the time chosen by the user

			editor.putInt("ora_prenot1", hourOfDay);
			editor.putInt("minut_prenot1", minute);

			// int ora_update = prefs.getInt("ora_update", 8);
			// int minut_update = prefs.getInt("minut_update", 0);

			String h;
			String m;
			if (hourOfDay < 10)
				h = "0" + hourOfDay;
			else
				h = hourOfDay + "";
			if (minute < 10)
				m = "0" + minute;
			else
				m = minute + "";
			btn2.setText(h + ":" + m);

		}
	}

	public static class TimePickerFragment3 extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current time as the default values for the picker
			final Calendar c = Calendar.getInstance();
			// int hour = c.get(Calendar.HOUR_OF_DAY);
			// int minute = c.get(Calendar.MINUTE);

			int hour = prefs.getInt("ora_prenot2", 0);
			int minute = prefs.getInt("minut_prenot2", 15);

			// Create a new instance of TimePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute, true);
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// Do something with the time chosen by the user

			editor.putInt("ora_prenot2", hourOfDay);
			editor.putInt("minut_prenot2", minute);

			// int ora_update = prefs.getInt("ora_prenot2", 8);
			// int minut_update = prefs.getInt("minut_prenot2", 0);

			String h;
			String m;
			if (hourOfDay < 10)
				h = "0" + hourOfDay;
			else
				h = hourOfDay + "";
			if (minute < 10)
				m = "0" + minute;
			else
				m = minute + "";
			btn3.setText(h + ":" + m);

		}
	}

	public static class TimePickerFragment4 extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current time as the default values for the picker
			final Calendar c = Calendar.getInstance();
			// int hour = c.get(Calendar.HOUR_OF_DAY);
			// int minute = c.get(Calendar.MINUTE);

			int hour = prefs.getInt("ora_prog", 18);
			int minute = prefs.getInt("minut_prog", 0);

			// Create a new instance of TimePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute, true);
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// Do something with the time chosen by the user

			editor.putInt("ora_prog", hourOfDay);
			editor.putInt("minut_prog", minute);

			System.out.println(hourOfDay + " : " + minute + "    <-----");

			// int ora_update = prefs.getInt("ora_prenot2", 8);
			// int minut_update = prefs.getInt("minut_prenot2", 0);

			String h;
			String m;
			if (hourOfDay < 10)
				h = "0" + hourOfDay;
			else
				h = hourOfDay + "";
			if (minute < 10)
				m = "0" + minute;
			else
				m = minute + "";
			btn4.setText(h + ":" + m);

		}
	}

	public void showTimePickerDialog(View v, int i) {
		if (i == 0) {
			DialogFragment newFragment = new TimePickerFragment();
			newFragment.show(getSupportFragmentManager(), "timePicker");
		}

		if (i == 1) {
			DialogFragment newFragment = new TimePickerFragment2();
			newFragment.show(getSupportFragmentManager(), "timePicker");
		}

		if (i == 2) {
			DialogFragment newFragment = new TimePickerFragment3();
			newFragment.show(getSupportFragmentManager(), "timePicker");
		}

		if (i == 3) {
			DialogFragment newFragment = new TimePickerFragment4();
			newFragment.show(getSupportFragmentManager(), "timePicker");
		}

	}

}
