package calendarmanager.activities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class LicentaActivity extends Activity {

	class RequestTask extends AsyncTask<Void, Void, Void> {

		String uri;

		public RequestTask(String uri) {
			// TODO Auto-generated constructor stub
			this.uri = uri;
		}

		@Override
		protected Void doInBackground(Void... params) {
			HttpClient httpclient = new DefaultHttpClient();
			String responseString = null;

			try {
				// response = httpclient.execute(new HttpHost(uri));
				HttpPost httppost = new HttpPost(uri);
				HttpResponse response = httpclient.execute(httppost);
				StatusLine statusLine = response.getStatusLine();
				if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					response.getEntity().writeTo(out);
					out.close();
					responseString = out.toString();
				} else {
					// Closes the connection.
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}

			} catch (ClientProtocolException e) {
				// TODO Handle problems..
			} catch (IOException e) {
				// TODO Handle problems..
			}

			if (responseString.compareTo("1") == 0) {

				editor.putInt("cont",
						Integer.valueOf(edt1.getText().toString()));
				editor.apply();

				Intent start = new Intent(getApplicationContext(),
						MainActivity.class);
				start.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				getApplicationContext().startActivity(start);
				finish();

			}

			else {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						Toast.makeText(getApplicationContext(),
								"Licenta nu este compatibila",
								Toast.LENGTH_LONG).show();
					}
				});
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}
	}

	EditText edt1;
	SharedPreferences prefs;
	SharedPreferences.Editor editor;

	public boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		// should check null because in air plan mode it will be null
		if (netInfo != null && netInfo.isConnected()) {
			return true;
		}
		return false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_licenta);

		ImageView imm = (ImageView) findViewById(R.id.back_dosar_pic);
		imm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");
 
		TextView tv = (TextView) findViewById(R.id.licent);
		TextView tv2 = (TextView) findViewById(R.id.info_gen_licenta);

		edt1 = (EditText) findViewById(R.id.editTextLicenta);

		Button btn = (Button) findViewById(R.id.btn_licenta);

		tv.setTypeface(tf);
		tv2.setTypeface(tf);
		edt1.setTypeface(tf);
		btn.setTypeface(tf);

		prefs = this.getSharedPreferences("com.example.app",
				Context.MODE_PRIVATE);
		editor = prefs.edit();

		Button btn3 = (Button) findViewById(R.id.btn_contact);
		btn3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new  Intent(LicentaActivity.this, ActivityContact.class);
				startActivity(i);
				
			}
		});
		
		btn3.setTypeface(tf);
		
		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (edt1.getText().length() != 0) {

					if (isOnline(getApplicationContext())) {
						RequestTask r = new RequestTask(
								"http://boghislastuf.ro/sistemlicentiere/index.php?id="
										+ edt1.getText().toString());
						r.execute();
					}
					else{
						Toast.makeText(getApplicationContext(),
								"Conexiune la internet inexistenta",
								Toast.LENGTH_LONG).show();
					}
				}
			}
		});

		if (prefs.getInt("cont", 0) != 0) {
			btn.setEnabled(false);
			edt1.setText(prefs.getInt("cont", 0) + "  --- Licenta activata");
			edt1.setEnabled(false);
		}

	}

}
