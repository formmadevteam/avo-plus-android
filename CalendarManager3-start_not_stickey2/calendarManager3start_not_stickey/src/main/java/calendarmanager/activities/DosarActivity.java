package calendarmanager.activities;

import java.util.Calendar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import calendarmanager.adapters.AdapterForCaiAtac;
import calendarmanager.adapters.AdapterForParti;
import calendarmanager.adapters.AdapterForSedinte;
import calendarmanager.helpers.EcrisUtils;
import calendarmanager.helpers.HelperSqlFunctions;
import ecris.data.types.Dosar;

public class DosarActivity extends Activity {

	public TextView tv;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.e("er", "Bum");
		return super.onOptionsItemSelected(item);
	}

	public String parseCal(Calendar c) {
		if (c != null) {

			String s = c.get(Calendar.DAY_OF_MONTH) + "."
					+ (c.get(Calendar.MONTH) + 1) + "." + c.get(Calendar.YEAR);
			return s;
		}
		return "";
	}

	private class Task extends AsyncTask<Void, Void, Void> {

		int idd;

		Dosar dd;

		public Task(int id, Dosar ddd) {
			idd = id;
			dd = ddd;
		}

		@Override
		protected Void doInBackground(Void... params) {
			EcrisUtils e = new EcrisUtils();
			HelperSqlFunctions h = new HelperSqlFunctions(DosarActivity.this);

			System.out.println("zbeng: " + id);

			dd = h.getDosar(id);

			if (dd == null) {
				finish();
				ListaDosareActivity.ok = 0;
			}

			// Log.e("d", "dos ar: " + dd.getDosarObservatii().getNume());

			// e.afisareDosar(dd);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			TextView tv = (TextView) findViewById(R.id.dataDosarId);
			tv.setText(dd.getNumar() + "");

			TextView data_in = (TextView) findViewById(R.id.d_data_in);
			TextView data_ul = (TextView) findViewById(R.id.d_data_ul);
			TextView sectie = (TextView) findViewById(R.id.d_sectie);
			TextView materie = (TextView) findViewById(R.id.d_materie);
			TextView obiect = (TextView) findViewById(R.id.d_obiect);
			TextView stadiu = (TextView) findViewById(R.id.d_stadiu);

			TextView nume = (TextView) findViewById(R.id.d_nume);
			TextView observatie = (TextView) findViewById(R.id.d_obs);

			// data_in.setTypeface(tf);
			// data_ul.setTypeface(tf);
			// sectie.setTypeface(tf);
			// materie.setTypeface(tf);
			// obiect.setTypeface(tf);
			// stadiu.setTypeface(tf);

			data_in.setText(parseCal(dd.getData()));
			data_ul.setText(parseCal(dd.getDataModificare()));
			sectie.setText(dd.getDepartament() + "");
			materie.setText(dd.getCategorieCaz() + "");
			obiect.setText(dd.getObiect() + "");
			stadiu.setText(dd.getStadiuProcesual() + "");

			if (dd.getDosarObservatii() != null) {
				nume.setText(dd.getDosarObservatii().getNume() + "");
				observatie
						.setText(dd.getDosarObservatii().getObservatii() + "");
			}

			if (dd.getParti().size() != 0) {

				ListView listView = (ListView) findViewById(R.id.listviewparti);
				AdapterForParti listAdapterr = new AdapterForParti(
						DosarActivity.this, dd.getParti());

				listView.setAdapter(listAdapterr);
				listView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub

						if (dd.getParti().size() > 5 && arg2 == 5) {
							Intent i = new Intent(getApplicationContext(),
									ListaPartiActivity.class);
							i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							i.putExtra("id_dosar", id);
							getApplicationContext().startActivity(i);
						}

					}

				});
			}

			if (dd.getSedinte().size() != 0) {

				ListView listView2 = (ListView) findViewById(R.id.listviewsedinte);
				AdapterForSedinte listAdapterr2 = new AdapterForSedinte(
						DosarActivity.this, dd.getSedinte());

				listView2.setAdapter(listAdapterr2);

				listView2.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub

						Log.e("ed", arg2 + "");

						if (dd.getSedinte().size() < 5) {

							Intent i = new Intent(getApplicationContext(),
									SedintaActivity.class);
							i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

							if (dd.getSedinte().get(arg2).getDocumentSedinta() == null
									|| dd.getSedinte().get(arg2)
											.getDocumentSedinta() == "anyType{}") {
								i.putExtra("doc",
										parseCal(dd.getSedinte().get(arg2)
												.getDataDocument()));
							} else {
								i.putExtra(
										"doc",
										dd.getSedinte().get(arg2)
												.getDocumentSedinta()
												+ " "
												+ parseCal(dd.getSedinte()
														.get(arg2)
														.getDataDocument()));
							}

							if (dd.getSedinte().get(arg2).getSolutie() == null
									|| dd.getSedinte().get(arg2).getSolutie()
											.compareTo("anyType{}") == 0) {
								i.putExtra("tip", "-");
							} else {
								i.putExtra("tip", dd.getSedinte().get(arg2)
										.getSolutie());
							}

							if (dd.getSedinte().get(arg2).getSolutieSumar() == "null"
									|| dd.getSedinte().get(arg2)
											.getSolutieSumar()
											.compareTo("anyType{}") == 0) {

								i.putExtra("sol", "-");

							} else {
								i.putExtra("sol", dd.getSedinte().get(arg2)
										.getSolutieSumar());
							}

							i.putExtra("data",
									parseCal(dd.getSedinte().get(arg2)
											.getData()));
							i.putExtra("ora", dd.getSedinte().get(arg2)
									.getOra());
							i.putExtra("complet", dd.getSedinte().get(arg2)
									.getComplet());

							getApplicationContext().startActivity(i);

						} else {
							if (arg2 < 5) {
								Intent i = new Intent(getApplicationContext(),
										SedintaActivity.class);
								i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

								if (dd.getSedinte().get(arg2)
										.getDocumentSedinta() == null
										|| dd.getSedinte().get(arg2)
												.getDocumentSedinta() == "anyType{}") {
									i.putExtra("doc", parseCal(dd.getSedinte()
											.get(arg2).getDataDocument()));
								} else {
									i.putExtra("doc", dd.getSedinte().get(arg2)
											.getDocumentSedinta()
											+ " "
											+ parseCal(dd.getSedinte()
													.get(arg2)
													.getDataDocument()));
								}

								if (dd.getSedinte().get(arg2).getSolutie() == null
										|| dd.getSedinte().get(arg2)
												.getSolutie()
												.compareTo("anyType{}") == 0) {
									i.putExtra("tip", "-");
								} else {
									i.putExtra("tip", dd.getSedinte().get(arg2)
											.getSolutie());
								}

								if (dd.getSedinte().get(arg2).getSolutieSumar() == "null"
										|| dd.getSedinte().get(arg2)
												.getSolutieSumar()
												.compareTo("anyType{}") == 0) {

									i.putExtra("sol", "-");

								} else {
									i.putExtra("sol", dd.getSedinte().get(arg2)
											.getSolutieSumar());
								}

								i.putExtra("data", parseCal(dd.getSedinte()
										.get(arg2).getData()));
								i.putExtra("ora", dd.getSedinte().get(arg2)
										.getOra());
								i.putExtra("complet", dd.getSedinte().get(arg2)
										.getComplet());

								getApplicationContext().startActivity(i);

							} else {
								Intent i = new Intent(getApplicationContext(),
										ListaSedinteActivity.class);
								i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								i.putExtra("id_dosar", id);
								getApplicationContext().startActivity(i);
							}
						}

					}
				});
			}

			if (dd.getCaiAtac().size() != 0) {

				ListView listView3 = (ListView) findViewById(R.id.listviewcai);
				AdapterForCaiAtac listAdapterr3 = new AdapterForCaiAtac(
						DosarActivity.this, dd.getCaiAtac());

				listView3.setAdapter(listAdapterr3);

				listView3.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub

						if (dd.getParti().size() > 5 && arg2 == 5) {
							Intent i = new Intent(getApplicationContext(),
									ListaCaiAtacActivity.class);
							i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							i.putExtra("id_dosar", id);
							getApplicationContext().startActivity(i);
						}

					}

				});

			}
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					// mProgressDialog.dismiss();
				}
			});

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// mProgressDialog.show();
		}

	}

	static ProgressDialog mProgressDialog;
	//
	// private class TaskDelete extends AsyncTask<Void, Void, Void> {
	//
	// int idd;
	//
	// Dosar dd;
	//
	// public TaskDelete(int id) {
	// idd = id;
	// }
	//
	// @Override
	// protected Void doInBackground(Void... params) {
	// EcrisUtils e = new EcrisUtils();
	// HelperSqlFunctions h = new HelperSqlFunctions(DosarActivity.this);
	// h.deleteDosar(idd);
	//
	// return null;
	// }
	//
	// @Override
	// protected void onPostExecute(Void result) {
	// // TODO Auto-generated method stub
	// super.onPostExecute(result);
	// runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // TODO Auto-generated method stub
	// Toast.makeText(getApplicationContext(),
	// "Dosarul a fost sters!", Toast.LENGTH_LONG).show();
	// finish();
	// }
	// });
	//
	// }
	//
	// @Override
	// protected void onPreExecute() {
	// // TODO Auto-generated method stub
	// super.onPreExecute();
	// }
	//
	// }

	public Dosar dd;

	public static int id;

	public static int del = 0;

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Intent intent = getIntent();

		System.out.println("Intra in resume: "
				+ intent.getIntExtra("id_dos", -5));

		if (intent.getIntExtra("id_dos", -5) != -5) {
			int ide = intent.getIntExtra("id_dos", -5);
			Task t = new Task(ide, dd);
			t.execute();
		}
		if (del == 0) {
			Task t = new Task(id, dd);
			t.execute();
		} else {
			finish();
			ListaDosareActivity.ok = 1;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_dosar);
		TextView tv = (TextView) findViewById(R.id.dataDosarId);

		Intent intent = getIntent();
		if (intent.getIntExtra("id_dos", -5) != -5) {

			id = intent.getIntExtra("id_dos", -5);

			// System.out.println("buttttttttttttttttttttttt: " + id);

			Task t = new Task(id, dd);
			t.execute();
		}

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");
		tv.setTypeface(tf);

		dd = new Dosar();
		setTitle("");

		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage("Initializare..");
		mProgressDialog.setCancelable(true);

		// ActionBar actionBar = getActionBar();
		// actionBar.setIcon(R.drawable.stanga_data);
		// actionBar.setDisplayHomeAsUpEnabled(true);

		// Intent e = getIntent();
		// String dosar = e.getExtras().getString("id_dosar");
		// id = Integer.valueOf(dosar);

		Task t = new Task(id, dd);
		t.execute();

		TextView info_gen = (TextView) findViewById(R.id.info_gen);
		info_gen.setTypeface(tf);

		TextView info_parti = (TextView) findViewById(R.id.info_parti);
		info_parti.setTypeface(tf);

		TextView data_in = (TextView) findViewById(R.id.d_data_in);
		TextView data_ul = (TextView) findViewById(R.id.d_data_ul);
		TextView sectie = (TextView) findViewById(R.id.d_sectie);
		TextView materie = (TextView) findViewById(R.id.d_materie);
		TextView obiect = (TextView) findViewById(R.id.d_obiect);
		TextView stadiu = (TextView) findViewById(R.id.d_stadiu);

		TextView nume = (TextView) findViewById(R.id.d_nume);
		TextView observatie = (TextView) findViewById(R.id.d_obs);

		data_in.setTypeface(tf);
		data_ul.setTypeface(tf);
		sectie.setTypeface(tf);
		materie.setTypeface(tf);
		obiect.setTypeface(tf);
		stadiu.setTypeface(tf);
		nume.setTypeface(tf);
		observatie.setTypeface(tf);

		data_in.setText(dd.getData() + "");
		data_ul.setText(dd.getDataModificare() + "");
		sectie.setText(dd.getDepartament() + "");
		materie.setText(dd.getCategorieCaz() + "");
		obiect.setText(dd.getObiect() + "");
		stadiu.setText(dd.getStadiuProcesual() + "");
		//
		// nume.setText(dd.getDosarObservatii().getNume()+"");
		// observatie.setText(dd.getDosarObservatii().getObservatii()+"");
		//
		ImageView im = (ImageView) findViewById(R.id.delDosar);
		im.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// DialogInterface.OnClickListener dialogClickListener = new
				// DialogInterface.OnClickListener() {
				// @Override
				// public void onClick(DialogInterface dialog, int which) {
				// switch (which) {
				// case DialogInterface.BUTTON_POSITIVE:
				// // Yes button clicked
				// TaskDelete t = new TaskDelete(id);
				// t.execute();
				// break;
				//
				// case DialogInterface.BUTTON_NEGATIVE:
				// // No button clicked
				// break;
				// }
				// }
				// };
				//
				// AlertDialog.Builder builder = new AlertDialog.Builder(
				// DosarActivity.this);
				// builder.setMessage("Doriti sa stergeti acest dosar?")
				// .setPositiveButton("Da", dialogClickListener)
				// .setNegativeButton("Nu", dialogClickListener).show();

				Intent i = new Intent(getApplicationContext(),
						EditareDosarActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				i.putExtra("id_dosar", id);
				DosarActivity.this.startActivity(i);

			}
		});

		ImageView imm = (ImageView) findViewById(R.id.back_dosar_pic);
		imm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		Log.e("id_dosar", "Id dosar: " + id);

	}

}
