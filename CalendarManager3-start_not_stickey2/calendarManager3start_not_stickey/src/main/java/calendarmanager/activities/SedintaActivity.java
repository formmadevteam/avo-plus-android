package calendarmanager.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class SedintaActivity extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_sedinta);
		
		setTitle("");
		Intent i = getIntent();
		
		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");
		
		String data = i.getStringExtra("data");
		String ora = i.getStringExtra("ora");
		String complet = i.getStringExtra("complet");
		String tip = i.getStringExtra("tip");
		String sol = i.getStringExtra("sol");
		String doc = i.getStringExtra("doc");
		
//		Amână cauza
		
		Log.d("debug", tip+" <-- mumu");
		
		
		TextView oraa_txt = (TextView)findViewById(R.id.ora_txt);
		TextView tipp_txt = (TextView)findViewById(R.id.tip_txt);
		TextView comp_txt = (TextView)findViewById(R.id.com_txt);
		TextView soll_txt = (TextView)findViewById(R.id.sol_txt);
		TextView docc_txt = (TextView)findViewById(R.id.doc_txt);
		
		TextView dataa = (TextView)findViewById(R.id.data_dos);
		dataa.setText(data);
		TextView oraa = (TextView)findViewById(R.id.ora_res);
		oraa.setText(ora);
		TextView tipp = (TextView)findViewById(R.id.tip_res);
		tipp.setText(tip);
		TextView comp = (TextView)findViewById(R.id.com_res);
		comp.setText(complet);
		TextView soll = (TextView)findViewById(R.id.sol_res);
		soll.setText(sol);
		TextView docc = (TextView)findViewById(R.id.doc_res);
		docc.setText(doc);
		
		dataa.setTypeface(tf);
		oraa.setTypeface(tf);
		tipp.setTypeface(tf);
		comp.setTypeface(tf);
		soll.setTypeface(tf);
		docc.setTypeface(tf);
		
		oraa_txt.setTypeface(tf);
		tipp_txt.setTypeface(tf);
		comp_txt.setTypeface(tf);
		soll_txt.setTypeface(tf);
		docc_txt.setTypeface(tf);
		
		
		
//		TextView dataa = (TextView)findViewById(R.id.data_dos);
//		dataa.setText(data);
//		TextView oraa = (TextView)findViewById(R.id.ora);
//		oraa.setText(ora);
//		TextView tipp = (TextView)findViewById(R.id.tip_sol);
//		tipp.setText(tip);
//		TextView comp = (TextView)findViewById(R.id.complet);
//		comp.setText(complet);
//		TextView soll = (TextView)findViewById(R.id.sol);
//		soll.setText(sol);
//		TextView docc = (TextView)findViewById(R.id.doc);
//		docc.setText(doc);
//		
		ImageView img = (ImageView)findViewById(R.id.back_dosar_pic);
		img.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
			
				finish();
				return false;
			}
		});
		
	}
	
}
