package calendarmanager.activities;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.ListActivity;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import calendarmanager.adapters.AdapterForPartiSimple;
import calendarmanager.helpers.HelperSqlFunctions;
import ecris.data.types.Dosar;
import ecris.data.types.DosarParte;

public class ListaPartiActivity extends ListActivity {

	AdapterForPartiSimple lad;

	ArrayList<DosarParte> liss;

	Dosar dd;
	
	public String parseCal(Calendar c) {
		String s = c.get(Calendar.DAY_OF_MONTH) + "."
				+ (c.get(Calendar.MONTH) + 1) + "." + c.get(Calendar.YEAR);
		return s;
	}
	
	private class TaskGetDosare extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			HelperSqlFunctions h = new HelperSqlFunctions(
					getApplicationContext());
			liss = new ArrayList<DosarParte>();
			liss = h.getDosar(DosarActivity.id).getParti();

			dd = h.getDosar(DosarActivity.id);
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			lad = new AdapterForPartiSimple(ListaPartiActivity.this, liss);
			setListAdapter(lad);

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}
	}

	TextView tv1;
	TextView tv2;

	EditText edt1;

	int ok = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_list_parti);
		setTitle("");

		TaskGetDosare t = new TaskGetDosare();
		t.execute();

		// liss_search = new ArrayList<DosarSimplif>();

		ImageView img2 = (ImageView) findViewById(R.id.back_dosar);
		img2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");

	}
}
