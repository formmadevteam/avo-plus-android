package calendarmanager.activities;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import calendarmanager.helpers.EcrisUtils;
import calendarmanager.helpers.HelperSqlFunctions;
import calendarmanager.helpers.OverService;
import ecris.data.types.Dosar;

public class AddDosarActivity extends Activity {

	public boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		// should check null because in air plan mode it will be null
		if (netInfo != null && netInfo.isConnected()) {
			return true;
		}
		return false;
	}

	private class Task extends AsyncTask<Void, Void, Void> {

		String numar;
		int ok = 1;

		public Task(String numarDosar) {
			// TODO Auto-generated constructor stub
			this.numar = numarDosar;
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			EcrisUtils e = new EcrisUtils();
			HelperSqlFunctions h = new HelperSqlFunctions(
					getApplicationContext());

			if (isOnline(getApplicationContext())) {
				if (h.dosarExists(numar) != 0)
					ok = e.getFahrenheit(numar, getApplicationContext());
				else {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							Toast.makeText(AddDosarActivity.this,
									"Dosarul exista deja in lista!",
									Toast.LENGTH_LONG).show();
						}
					});
				}
			} else {
				ok = 2;
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					if (ok == 1) {
						Toast.makeText(getApplicationContext(),
								"Sincronizat cu succes", Toast.LENGTH_LONG)
								.show();

						Intent intentut = new Intent(getApplicationContext(),
								OverService.class);
						startService(intentut);

						dialog2.dismiss();
						finish();
					} else {
						if (ok == 2) {
							Toast.makeText(getApplicationContext(),
									"Este necesara conexiunea la internet!",
									Toast.LENGTH_LONG).show();

							
							Intent intentut = new Intent(
									getApplicationContext(), OverService.class);
							startService(intentut);

							dialog2.dismiss();

						} else {
							if (ok == 0) {
								Toast.makeText(getApplicationContext(),
										"Dosarul nu este compatibil",
										Toast.LENGTH_LONG).show();

								Intent intentut = new Intent(
										getApplicationContext(),
										OverService.class);
								startService(intentut);

								dialog2.dismiss();
							}
							// Toast.makeText(getApplicationContext(),
							// "A aparut o eroare", Toast.LENGTH_LONG).show();
						}
					}
				}
			});

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog2.setMessage("Sincronizare...");
			dialog2.setCancelable(false);
			dialog2.show();
		}

	}

	private ProgressDialog dialog;
	private ProgressDialog dialog2;

	private class TaskDeZbor extends AsyncTask<Void, Void, Void> {

		Dosar dosar;
		int i;

		public TaskDeZbor(Dosar dosar, int i) {
			// TODO Auto-generated constructor stub
			this.dosar = dosar;
			this.i = i;
		}

		@Override
		protected Void doInBackground(Void... params) {

			HelperSqlFunctions h = new HelperSqlFunctions(
					getApplicationContext());
			h.insertDosar(dosar);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("finish", "Finish thread: " + i);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// dialog2.setMessage("Progress start");
			// dialog2.show();
		}

	}

	private class TaskSpatial extends AsyncTask<Void, Void, Void> {

		String numar;

		public TaskSpatial(String numarDosar) {
			// TODO Auto-generated constructor stub
			this.numar = numarDosar;

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			EcrisUtils e = new EcrisUtils();

			ArrayList<Dosar> dosare = e.getFahrenheitByName(numar,
					getApplicationContext());

			// Log.e("ad", dosare.get(0).getNumar() + " <- numar");
			Log.e("ad", dosare.size() + " <- dosare");

			int i = 1;
			for (Dosar dosar : dosare) {

				TaskDeZbor t = new TaskDeZbor(dosar, i);
				t.execute();
				i++;
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					Toast.makeText(getApplicationContext(),
							"Sincronizat cu succes", Toast.LENGTH_LONG).show();

					finish();
				}
			});
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			dialog.setMessage("Progress start");
			dialog.show();
		}

	}

	EditText edt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_dosare);

		dialog2 = new ProgressDialog(AddDosarActivity.this);

		edt = (EditText) findViewById(R.id.editText1);

		Button b = (Button) findViewById(R.id.button1);
		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Task t = new Task(edt.getText() + "");
				t.execute();

			}
		});

		TextView tv = (TextView) findViewById(R.id.title_list_dos);
		TextView tv2 = (TextView) findViewById(R.id.info_gen_nr);

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");
		tv.setTypeface(tf);
		tv2.setTypeface(tf);
		b.setTypeface(tf);
		edt.setTypeface(tf);

		ImageView im = (ImageView) findViewById(R.id.back_dosar);
		im.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

}
