package calendarmanager.activities;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ListView;

public class ExpandedListView extends ListView {

	private android.view.ViewGroup.LayoutParams params;
	private int old_count = 0;

	public ExpandedListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onDraw(Canvas canvas) {

//		if (getCount() != old_count) {
//            old_count = getCount();
//            params = getLayoutParams();
//            params.height = getCount() * (old_count > 0 ? getChildAt(0).getHeight() : 0);
//            setLayoutParams(params);
//        }


//		if (getCount() > old_count) {
//            old_count = getCount();
//            params = getLayoutParams();
//            params.height = getCount() * (old_count > 0 ? getChildAt(old_count).getHeight() : 0);
//            setLayoutParams(params);
//        }
		

	    if (getCount() != old_count) {
	        params = getLayoutParams();
	        old_count = getCount();
	        int totalHeight = 0;
	        for (int i = 0; i < getCount(); i++) {
	            this.measure(0, 0);
	            totalHeight += getMeasuredHeight();
	        }
	        params = getLayoutParams();
	        params.height = totalHeight + (getDividerHeight() * (getCount() - 1));
	        setLayoutParams(params);
	    }

		
		// if (getCount() != old_count) {
		// old_count = getCount();

		// params = getLayoutParams();
		// // if (old_count > 1) {
		// int height = 0;
		// for (int i = 0; i < getChildCount(); i++) {
		// height += getChildAt(i).getMeasuredHeight();
		// height += getDividerHeight();
		// }
		//
		// params.height = height;
		//
		// // } else
		// // params.height = 0;
		//
		 
		// }

		super.onDraw(canvas);
	}
}