package calendarmanager.activities;

import java.util.ArrayList;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import calendaradapter.structures.DosarSimplif;
import calendarmanager.adapters.AdapterForListaDosare;
import calendarmanager.helpers.HelperSqlFunctions;
import ecris.data.types.DosarParte;

public class ListaDosareActivity extends ListActivity {

	AdapterForListaDosare lad;

	ArrayList<DosarSimplif> liss;
	ArrayList<DosarSimplif> liss_search;

	private ProgressDialog dialog2;
	
	private class TaskGetDosare extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			HelperSqlFunctions h = new HelperSqlFunctions(
					getApplicationContext());

			liss = new ArrayList<DosarSimplif>();
			liss = h.getListaDosareSimplif();

			liss_search = new ArrayList<DosarSimplif>();

			liss_search.addAll(liss);
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			dialog2.dismiss();
			
			lad = new AdapterForListaDosare(ListaDosareActivity.this, liss);
			setListAdapter(lad);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			dialog2.setMessage("Se incarca datele...");
			dialog2.setCancelable(false);
			dialog2.show();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		liss = new ArrayList<DosarSimplif>();
		liss.clear();
		TaskGetDosare t = new TaskGetDosare();
		t.execute();

	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		
		Intent myIntent = new Intent(ListaDosareActivity.this,
				DosarActivity.class);

		DosarActivity.del = 0;

		DosarActivity.id = liss_search.get(position).getId();

		ListaDosareActivity.this.startActivity(myIntent);

		edt1.setText("");

	}

	TextView tv1;
	TextView tv2;

	EditText edt1;

	static int ok = 0;

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_list_dosare);
		setTitle("");
		dialog2 = new ProgressDialog(ListaDosareActivity.this);
		liss = new ArrayList<DosarSimplif>();
		liss_search = new ArrayList<DosarSimplif>();

		ImageView img = (ImageView) findViewById(R.id.plus_dosar);
		img.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(),
						AddDosarActivity.class);
				ListaDosareActivity.this.startActivity(i);
			}
		});

		ImageView img2 = (ImageView) findViewById(R.id.back_dosar);
		img2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");

		tv1 = (TextView) findViewById(R.id.text_cauta_lista_dos);
		tv2 = (TextView) findViewById(R.id.title_list_dos);
		edt1 = (EditText) findViewById(R.id.edt_cauta_lista_dosare);

		tv1.setTypeface(tf);
		tv2.setTypeface(tf);
		edt1.setTypeface(tf);

		edt1.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				// Log.e("debug", "liss:"+liss.size());
				// Log.e("debug", "liss_search:"+liss_search.size());
				//
				ok = 1;

				liss_search.clear();
				// if (edt1.getText().length() != 0) {
				String ss = edt1.getText().toString().toLowerCase();
				for (int i = 0; i < liss.size(); i++) {
					String numar = liss.get(i).getNumar().toLowerCase();
					if (numar.contains(ss)) {
						liss_search.add(liss.get(i));
					} else {
						
						System.out.println("aci ii: "+liss.get(i)
								.getLista_parti().size());
						
						for (DosarParte dosarParte : liss.get(i)
								.getLista_parti()) {
							
							System.out.println(dosarParte.getNume()+" <-nume");
							
							if (dosarParte.getNume().toLowerCase().contains(ss.toLowerCase())) {
								liss_search.add(liss.get(i));
							}
						}
					}

				}
				lad = new AdapterForListaDosare(ListaDosareActivity.this,
						liss_search);
				setListAdapter(lad);

			}
		});

	}
}
