package calendarmanager.activities;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import calendarmanager.helpers.EcrisUtils;
import calendarmanager.helpers.HelperSqlFunctions;
import ecris.data.types.Dosar;

public class ActivityBackup extends Activity {

	public TextView tv;
	public TextView tv1;
	public Button btn1;
	public Button btn2;
	
	
	DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	    @Override
	    public void onClick(DialogInterface dialog, int which) {
	        switch (which){
	        case DialogInterface.BUTTON_POSITIVE:
	            //Yes button clicked
				TaskBackup t = new TaskBackup();
				t.execute();
	            break;

	        case DialogInterface.BUTTON_NEGATIVE:
	            //No button clicked
	            break;
	        }
	    }
	};
	
	DialogInterface.OnClickListener dialogClickListener2 = new DialogInterface.OnClickListener() {
	    @Override
	    public void onClick(DialogInterface dialog, int which) {
	        switch (which){
	        case DialogInterface.BUTTON_POSITIVE:
	        	TaskGetBackup t = new TaskGetBackup();
				t.execute();
	            break;
	        case DialogInterface.BUTTON_NEGATIVE:
	            //No button clicked
	            break;
	        }
	    }
	};

	

	

	public void upload_command() {

//		String result;
//		final String js = json.toString();

		try {

			SharedPreferences prefs = getApplicationContext()
					.getSharedPreferences("com.example.app",
							Context.MODE_PRIVATE);
			int licenta = prefs.getInt("cont", 0);
			
			HttpPost httppost = new HttpPost("http://boghislastuf.ro/sistemlicentiere/download.php?id_licenta="+licenta);

//			HttpParams myParams = new BasicHttpParams();
//			HttpConnectionParams.setConnectionTimeout(myParams, 10000);
//			HttpConnectionParams.setSoTimeout(myParams, 60000);
//			HttpConnectionParams.setTcpNoDelay(myParams, true);

			httppost.setHeader("Content-type", "application/json");
			HttpClient httpclient = new DefaultHttpClient();

			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			InputStream is = entity.getContent();

			String res = null;

			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader reader = new BufferedReader(isr, 8);

			StringBuilder sb = new StringBuilder();
			String line = null;

			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}

			is.close();
			res = sb.toString();

//			System.out.println(res+" -gica");
			
			JSONArray jArray = new JSONArray(res);

			EcrisUtils helper = new EcrisUtils();
			
			HelperSqlFunctions h = new HelperSqlFunctions(getApplicationContext());
			h.deleteDatabase();
			
			for (int i = 0; i < jArray.length(); i++) {
				JSONObject json_data = jArray.getJSONObject(i);
	
				helper.getFahrenheit(json_data.getString("id_dosar"), getApplicationContext());
				
			}
			
		} catch (Exception e) {
		}

		return;

	}

	

	private class TaskGetBackup extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			upload_command();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			dialog2.dismiss();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog2.setMessage("Se salveaza datele...");
			dialog2.setCancelable(false);
			dialog2.show();
		}
	}

	
	
	

	private class TaskBackup extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			HelperSqlFunctions helperSqlFunctions = new HelperSqlFunctions(
					getApplicationContext());
			ArrayList<Dosar> list = helperSqlFunctions.getListaDosare();

			for (int i = 0; i < list.size() - 1; i++) {
				for (int j = i + 1; j < list.size(); j++) {
					if (list.get(i).getNumar()
							.compareTo(list.get(j).getNumar()) == 0) {
						list.remove(j);
					}
				}
			}

			SharedPreferences prefs = getApplicationContext()
					.getSharedPreferences("com.example.app",
							Context.MODE_PRIVATE);

			// $json = '[{"id_licenta": 12345, "id_dosar":
			// "4242/423/2355"},{"id_licenta": 12345, "id_dosar":
			// "4242/423/2355"}]';
			String s = "[";
			int licenta = prefs.getInt("cont", 0);
			if (licenta != 0) {
				for (int i = 0; i < list.size(); i++) {
					Dosar dosar = list.get(i);
					dosar.getDosarObservatii().setObservatii(
							dosar.getDosarObservatii().getObservatii()
									.replaceAll("\n", "<br/>"));
					if (i < list.size() - 1) {
						s += "{" + '"' + "id_licenta" + '"' + ": " + '"' + ""
								+ licenta + "" + '"' + ", " + '"' + "id_dosar"
								+ '"' + ": " + '"' + "" + dosar.getNumar() + ""
								+ '"' + ", " + '"' + "nume" + '"' + ": " + '"'
								+ "" + dosar.getDosarObservatii().getNume()
								+ "" + '"' + ", " + '"' + "observatii" + '"'
								+ ": " + '"' + ""
								+ dosar.getDosarObservatii().getObservatii()
								+ "" + '"' + "},";
					} else {
						s += "{" + '"' + "id_licenta" + '"' + ": " + '"' + ""
								+ licenta + "" + '"' + ", " + '"' + "id_dosar"
								+ '"' + ": " + '"' + "" + dosar.getNumar() + ""
								+ '"' + ", " + '"' + "nume" + '"' + ": " + '"'
								+ "" + dosar.getDosarObservatii().getNume()
								+ "" + '"' + ", " + '"' + "observatii" + '"'
								+ ": " + '"' + ""
								+ dosar.getDosarObservatii().getObservatii()
								+ "" + '"' + "}";
					}
				}
				s += "]";
			}

			System.out.println(s);

			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(
					"http://boghislastuf.ro/sistemlicentiere/upload.php");

			System.out.println("ble");
			
			List<NameValuePair> pairs = new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair("id_licenta", licenta + ""));
			pairs.add(new BasicNameValuePair("dosare", s));

			HttpResponse response = null;
			try {
				post.setEntity(new UrlEncodedFormEntity(pairs));

				response = client.execute(post);

			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);

			}
			String responseText = null;
			try {
				responseText = EntityUtils.toString(response.getEntity());
			} catch (Exception e) {

			}

			System.out.println(responseText);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			dialog2.dismiss();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog2.setMessage("Se salveaza datele...");
			dialog2.setCancelable(false);
			dialog2.show();
		}
	}

	

	public boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		// should check null because in air plan mode it will be null
		if (netInfo != null && netInfo.isConnected()) {
			return true;
		}
		return false;
	}

	
	private ProgressDialog dialog2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_backup);

		tv = (TextView) findViewById(R.id.dataDosarId);
		tv1 = (TextView) findViewById(R.id.info_gen);

		btn1 = (Button) findViewById(R.id.btn_har_put);
		btn2 = (Button) findViewById(R.id.btn_har_dow);

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");

		dialog2 = new ProgressDialog(ActivityBackup.this);
		
		tv.setTypeface(tf);
		tv1.setTypeface(tf);
		btn1.setTypeface(tf);
		btn2.setTypeface(tf);

//		if (isOnline(getApplicationContext())) {
//			TaskGetBackup t = new TaskGetBackup();
//			t.execute();
//		} else {
//			Toast.makeText(getApplicationContext(),
//					"Este necesara conexiunea la internet",
//					Toast.LENGTH_LONG).show();
//		}
		
		btn1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (isOnline(getApplicationContext())) {

					AlertDialog.Builder builder = new AlertDialog.Builder(ActivityBackup.this);
					builder.setMessage("Sunteti sigur ca doriti sa suprascrieti ultima varianta stocata online?").setPositiveButton("Da", dialogClickListener)
					    .setNegativeButton("Nu", dialogClickListener).show();
					
				} else {
					Toast.makeText(getApplicationContext(),
							"Este necesara conexiunea la internet",
							Toast.LENGTH_LONG).show();
				}
				
				
			}
		});

		btn2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isOnline(getApplicationContext())) {
					AlertDialog.Builder builder = new AlertDialog.Builder(ActivityBackup.this);
					builder.setMessage("Sunteti sigur ca doriti sa suprascrieti ultima varianta stocata in telefon?").setPositiveButton("Da", dialogClickListener2)
				    .setNegativeButton("Nu", dialogClickListener2).show();
				
				} else {
					Toast.makeText(getApplicationContext(),
							"Este necesara conexiunea la internet",
							Toast.LENGTH_LONG).show();
				}
				
				

			}
		});

		ImageView img = (ImageView) findViewById(R.id.back_dosar_pic);
		img.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub

				finish();
				return false;
			}
		});

	}

}
