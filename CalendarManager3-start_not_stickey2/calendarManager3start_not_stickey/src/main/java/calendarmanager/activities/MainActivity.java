package calendarmanager.activities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ListActivity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import calendaradapter.structures.SedintaCurenta;
import calendarmanager.adapters.AdapterForListActivityAdaugaComandaNoua;
import calendarmanager.adapters.AdapterForMenu;
import calendarmanager.helpers.AlarmDailyService;
import calendarmanager.helpers.EcrisUtils;
import calendarmanager.helpers.HelperSqlFunctions;

import com.candroidsample.CaldroidSampleActivity;

import ecris.data.types.Dosar;

public class MainActivity extends ListActivity implements
		DialogInterface.OnClickListener {
	
	public static Calendar currentDate;
	
	private ImageButton back;
	private ImageButton next;

	private Button del;
	private Button insert;
	private EditText editInsert;

	public TextView date;
	public TextView datemy;
	public TextView tv;

	int i = 10;

//	HelperSql sqlHelper;
//	SQLiteDatabase sqlTable;
	
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private String[] mPlanetTitles;

	ArrayList<SedintaCurenta> liss;

	public static final String EXTRAS_TODAY = "EXTRAS_TODAY";
	public static final String EXTRAS_MILLIS = "EXTRAS_MILLIS";
	public static final String EXTRAS_SYNC = "EXTRAS_SYNC";

	
	private AlarmManager alarmMgr;
	private PendingIntent alarmIntent;

	
	void test_optimizari(){
		EcrisUtils e = new EcrisUtils();
		ArrayList<String> a = new ArrayList<String>();
		
		int i=0;
		for (SedintaCurenta elem : liss) {
			a.add(elem.getDosar());
			Log.e("debee", "Dos: "+elem.getDosar());
			i++;
		}
		Log.e("debee", "Cate: "+i);
		
		ArrayList<Dosar> lid =  e.getFahrenheitListForList(a, getApplicationContext());
		
		if(lid!=null){
			Log.e("debee", "Cate-primite: "+lid.size());
			
			for(Dosar d : lid){
				Log.e("debee", "Dos-primit: "+d.getNumar());
			}
		}
		else{
			Log.e("de", "pulaaa");
		}
		
	}
	
	
	@Override
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
		switch (which) {
		case DialogInterface.BUTTON_POSITIVE:

			SharedPreferences prefs = getApplicationContext()
					.getSharedPreferences("com.example.app",	
							Context.MODE_PRIVATE);
			editor = prefs.edit();
			editor.putInt("prima", 4322);
			editor.apply();

			break;
		case DialogInterface.BUTTON_NEGATIVE: // no
			finish();
			break;
		default:
			finish();
			break;
		}
	}

	private class TaskVerify extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {

			SharedPreferences prefs = getApplicationContext()
					.getSharedPreferences("com.example.app",
							Context.MODE_PRIVATE);

			editor = prefs.edit();

			int licenta = prefs.getInt("cont", 0);

			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(
					"http://boghislastuf.ro/sistemlicentiere/verif_licenta.php?id="
							+ licenta);

			HttpResponse response = null;

			try {

				response = client.execute(post);

			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);

			}
			String responseText = null;
			try {
				responseText = EntityUtils.toString(response.getEntity());
			} catch (Exception e) {
				responseText = "0";
			}

			if (android.text.TextUtils.isDigitsOnly(responseText))
				if (Integer.valueOf(responseText) == 1) {
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							Toast.makeText(getApplicationContext(),
									"Aplicatia este bloacata!",
									Toast.LENGTH_LONG).show();
						}
					});

					prefs = getApplicationContext().getSharedPreferences(
							"com.example.app", Context.MODE_PRIVATE);
					editor = prefs.edit();
					editor.putInt("blocat", 1);
					editor.apply();

					finish();

				} else {

					prefs = getApplicationContext().getSharedPreferences(
							"com.example.app", Context.MODE_PRIVATE);
					editor = prefs.edit();
					editor.putInt("blocat", 0);
					editor.apply();

					// finish();
				}

			// System.out.println(responseText);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	private void downloadapk() {
		try {

			URL url = new URL(
					"http://boghislastuf.ro/sistemlicentiere/CalendarManager3-.apk");
			HttpURLConnection urlConnection = (HttpURLConnection) url
					.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.setDoOutput(true);
			urlConnection.connect();

			File sdcard = Environment.getExternalStorageDirectory();
			File file = new File(sdcard, "CalendarManager.apk");

			FileOutputStream fileOutput = new FileOutputStream(file);
			InputStream inputStream = urlConnection.getInputStream();

			byte[] buffer = new byte[1024];
			int bufferLength = 0;

			while ((bufferLength = inputStream.read(buffer)) > 0) {
				fileOutput.write(buffer, 0, bufferLength);
			}
			fileOutput.close();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Intent intent = new Intent(Intent.ACTION_VIEW);
		Uri uri = Uri.fromFile(new File("/sdcard/CalendarManager.apk"));
		intent.setDataAndType(uri, "application/vnd.android.package-archive");
		startActivity(intent);
	}

	private class TaskBackup extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			HelperSqlFunctions helperSqlFunctions = new HelperSqlFunctions(
					getApplicationContext());
			ArrayList<Dosar> list = helperSqlFunctions.getListaDosare();

			for (int i = 0; i < list.size() - 1; i++) {
				for (int j = i + 1; j < list.size(); j++) {
					if (list.get(i).getNumar()
							.compareTo(list.get(j).getNumar()) == 0) {
						list.remove(j);
					}
				}
			}

			SharedPreferences prefs = getApplicationContext()
					.getSharedPreferences("com.example.app",
							Context.MODE_PRIVATE);

			// $json = '[{"id_licenta": 12345, "id_dosar":
			// "4242/423/2355"},{"id_licenta": 12345, "id_dosar":
			// "4242/423/2355"}]';
			String s = "[";
			int licenta = prefs.getInt("cont", 0);
			if (licenta != 0) {
				for (int i = 0; i < list.size(); i++) {
					Dosar dosar = list.get(i);
					dosar.getDosarObservatii().setObservatii(
							dosar.getDosarObservatii().getObservatii()
									.replaceAll("\n", "<br/>"));
					if (i < list.size() - 1) {
						s += "{" + '"' + "id_licenta" + '"' + ": " + '"' + ""
								+ licenta + "" + '"' + ", " + '"' + "id_dosar"
								+ '"' + ": " + '"' + "" + dosar.getNumar() + ""
								+ '"' + ", " + '"' + "nume" + '"' + ": " + '"'
								+ "" + dosar.getDosarObservatii().getNume()
								+ "" + '"' + ", " + '"' + "observatii" + '"'
								+ ": " + '"' + ""
								+ dosar.getDosarObservatii().getObservatii()
								+ "" + '"' + "},";
					} else {
						s += "{" + '"' + "id_licenta" + '"' + ": " + '"' + ""
								+ licenta + "" + '"' + ", " + '"' + "id_dosar"
								+ '"' + ": " + '"' + "" + dosar.getNumar() + ""
								+ '"' + ", " + '"' + "nume" + '"' + ": " + '"'
								+ "" + dosar.getDosarObservatii().getNume()
								+ "" + '"' + ", " + '"' + "observatii" + '"'
								+ ": " + '"' + ""
								+ dosar.getDosarObservatii().getObservatii()
								+ "" + '"' + "}";
					}
				}
				s += "]";
			}

			System.out.println(s);

			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(
					"http://boghislastuf.ro/sistemlicentiere/upload.php");

			System.out.println("ble");

			List<NameValuePair> pairs = new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair("id_licenta", licenta + ""));
			pairs.add(new BasicNameValuePair("dosare", s));

			HttpResponse response = null;
			try {
				post.setEntity(new UrlEncodedFormEntity(pairs));

				response = client.execute(post);

			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);

			}
			String responseText = null;
			try {
				responseText = EntityUtils.toString(response.getEntity());
			} catch (Exception e) {

			}

			System.out.println(responseText);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			//
			// dialog2.dismiss();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// dialog2.setMessage("Se salveaza datele...");
			// dialog2.show();
		}
	}

	private ProgressDialog dialog2;

	public int vers;

	public SharedPreferences.Editor editor;

	DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				// Yes button clicked

				TaskBackup t = new TaskBackup();
				t.execute();

				downloadapk();

				break;

			case DialogInterface.BUTTON_NEGATIVE:
				// No button clicked

				editor.putInt("version_no", vers);
				editor.apply();

				break;
			}
		}
	};

	private class TaskGetUpdate extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			SharedPreferences prefs = getApplicationContext()
					.getSharedPreferences("com.example.app",
							Context.MODE_PRIVATE);

			editor = prefs.edit();

			int cur_ver = prefs.getInt("version", 1);
			int ver_no = prefs.getInt("version_no", 1);

			vers = getVersion();
			if (cur_ver < vers && ver_no != vers) {

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						AlertDialog.Builder builder = new AlertDialog.Builder(
								MainActivity.this);
						builder.setMessage("Doriti sa actualizati aplicatia?")
								.setPositiveButton("Da", dialogClickListener)
								.setNegativeButton("Nu", dialogClickListener)
								.show();
					}
				});

			} else {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {

						// TODO Auto-generated method stub
						// Toast.makeText(getApplicationContext(),
						// "Nu exista versiuni noi!", Toast.LENGTH_LONG)
						// .show();
					}
				});
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
	}

	public int getVersion() {

		int version = 1;

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(
				"http://boghislastuf.ro/sistemlicentiere/version.php");

		HttpResponse response = null;
		try {

			response = client.execute(post);

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);

		}
		String responseText = null;
		try {
			responseText = EntityUtils.toString(response.getEntity());
		} catch (Exception e) {

		}

		// System.out.println(responseText);
		if (responseText != null) {
			if (android.text.TextUtils.isDigitsOnly(responseText))
				if (responseText != null)
					// if (Integer.valueOf(responseText).toString() ==
					// responseText)
					version = Integer.valueOf(responseText);

				else
					version = 1;
			else
				version = 1;
		}

		return version;
	}

	public void alarma(Context context) {
		Intent myIntent = new Intent(context, AlarmDailyService.class);

		alarmMgr = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);

		alarmMgr = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);

		Intent intent = new Intent(MainActivity.this, AlarmDailyService.class);

		alarmIntent = PendingIntent.getService(context, 0, myIntent, 0);

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 36);
		calendar.set(Calendar.SECOND, 0);

		AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

		// if (alarmManager!= null) {
		// alarmManager.cancel(alarmIntent);
		// }

		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				calendar.getTimeInMillis(), 1000, alarmIntent);

		// alarmIntent = PendingIntent.getBroadcast(context, 0, myIntent, 0);
		//
		// Calendar calendar = Calendar.getInstance();
		// // calendar.setTimeInMillis(System.currentTimeMillis());
		// calendar.set(Calendar.HOUR_OF_DAY, 23);
		// calendar.set(Calendar.MINUTE, 0);
		// calendar.set(Calendar.SECOND, 0);
		//
		// // setRepeating() lets you specify a precise custom interval--in this
		// // case,
		// // 20 minutes.
		// // alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP,
		// // calendar.getTimeInMillis(),
		// // AlarmManager.INTERVAL_DAY, alarmIntent);
		// // alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP,
		// // calendar.getTimeInMillis(),
		// // 1000, alarmIntent);
		// alarmMgr.set(AlarmManager.RTC_WAKEUP,
		// calendar.getTimeInMillis() + 10000, alarmIntent);

	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);

		Intent myIntent = new Intent(MainActivity.this, DosarActivity.class);
		DosarActivity.id = liss.get(position).getId_dosar();
		DosarActivity.del = 0;

		System.out.println(" id : " + liss.get(position).getId_dosar());
		myIntent.putExtra("id_dos", liss.get(position).getId_dosar());
		MainActivity.this.startActivity(myIntent);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content
		// view
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		// menu.findItem(R.id.action_set).setVisible(true);
		// menu.findItem(R.id.action_search).setVisible(true);
		// menu.findItem(id)
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action buttons
		switch (item.getItemId()) {
		// case R.id.action_websearch:
		// // create intent to perform web search for this planet
		// Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
		// intent.putExtra(SearchManager.QUERY, getActionBar().getTitle());
		// // catch event that there's no activity to handle intent
		// if (intent.resolveActivity(getPackageManager()) != null) {
		// startActivity(intent);
		// } else {
		// Toast.makeText(this, R.string.app_not_available,
		// Toast.LENGTH_LONG).show();
		// }
		// return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);
			if (position == 0) {
				Intent intent = new Intent(getApplicationContext(),
						CaldroidSampleActivity.class);
				startActivity(intent);
			}
			if (position == 1) {
				Intent intent = new Intent(getApplicationContext(),
						ListaDosareActivity.class);
				startActivity(intent);
			}
			if (position == 2) {
				// TaskBackup b = new TaskBackup();
				// b.execute();
				Intent intent = new Intent(getApplicationContext(),
						ActivityBackup.class);
				startActivity(intent);

			}
			if (position == 3) {
				Intent intent = new Intent(getApplicationContext(),
						SetariActivity.class);
				startActivity(intent);
			}

		}
	}

	private void selectItem(int position) {

		// update the main content by replacing fragments
		// Fragment fragment = new PlanetFragment();
		// Bundle args = new Bundle();
		// args.putInt(PlanetFragment.ARG_PLANET_NUMBER, position);
		// fragment.setArguments(args);

		FragmentManager fragmentManager = getFragmentManager();
		// fragmentManager.beginTransaction().replace(R.id.content_frame,
		// fragment).commit();

		// update selected item and title, then close the drawer
		// mDrawerList.setItemChecked(position, true);
		setTitle("");
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = "";
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	/**
	 * Fragment that appears in the "content_frame", shows a planet
	 */
	public static class PlanetFragment extends Fragment {
		public static final String ARG_PLANET_NUMBER = "planet_number";

		public PlanetFragment() {
			// Empty constructor required for fragment subclasses
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_planet,
					container, false);

			int i = getArguments().getInt(ARG_PLANET_NUMBER);
			String planet = getResources()
					.getStringArray(R.array.planets_array)[i];

			int imageId = getResources().getIdentifier(
					planet.toLowerCase(Locale.getDefault()), "drawable",
					getActivity().getPackageName());

			// ((ImageView) rootView.findViewById(R.id.image))
			// .setImageResource(R.drawable.meniu);

			getActivity().setTitle(planet);
			return rootView;
		}
	}

	public void drawer(Bundle savedInstanceState) {

		mTitle = mDrawerTitle = getTitle();
		mPlanetTitles = getResources().getStringArray(R.array.planets_array);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		// set a custom shadow that overlays the main content when the drawer
		// opens
		// mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
		// GravityCompat.START);
		// set up the drawer's list view with items and click listener

		AdapterForMenu custom = new AdapterForMenu(this);

		mDrawerList.setAdapter(custom);

		//
		// mDrawerList.setAdapter(new ArrayAdapter<String>(this,
		// R.layout.drawer_list_item, mPlanetTitles));

		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.gol, /* nav drawer image to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description for accessibility */
		R.string.drawer_close /* "close drawer" description for accessibility */
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle("");
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle("");
				invalidateOptionsMenu(); // creates call to
				drawerView.setBackgroundColor(Color.argb(180, 0, 0, 0));
			}

			@Override
			public void onConfigurationChanged(Configuration newConfig) {
				// TODO Auto-generated method stub
				super.onConfigurationChanged(newConfig);
				getActionBar().setTitle("");
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		setTitle("");
		// if (savedInstanceState == null) {
		// selectItem(0);
		// }
	}

	//
	// private class Task extends AsyncTask<Void, Void, Void> {
	//
	// @Override
	// protected Void doInBackground(Void... params) {
	// // TODO Auto-generated method stub
	// EcrisUtils e = new EcrisUtils();
	//
	// ArrayList<Dosar> dosare = e.getFahrenheit("268/122/2014",
	// getApplicationContext());
	//
	// HelperSqlFunctions h = new HelperSqlFunctions(
	// getApplicationContext());
	//
	// // for (Dosar dosar : dosare) {
	// // int id = h.insertDosar(dosar);
	//
	// //
	// // Log.e("much", "da -> "+id);
	// //
	// // Dosar d = h.getDosar(id);
	// //
	// // e.afisareDosar(dosar);
	// //
	// // Log.e("debug",
	// // "" + d.getData().compareTo(Calendar.getInstance()));
	//
	// // e.afisareDosar(d);
	// // }
	//
	// return null;
	// }
	//
	// @Override
	// protected void onPostExecute(Void result) {
	// // TODO Auto-generated method stub
	// super.onPostExecute(result);
	//
	// runOnUiThread(new Runnable() {
	//
	// @Override
	// public void run() {
	// // TODO Auto-generated method stub
	// Toast.makeText(getApplicationContext(), "Gata",
	// Toast.LENGTH_LONG).show();
	// }
	// });
	//
	// TaskGetDosare t = new TaskGetDosare();
	// t.execute();
	//
	// }
	//
	// @Override
	// protected void onPreExecute() {
	// // TODO Auto-generated method stub
	// super.onPreExecute();
	// }

	//
	// }

	public Calendar convertTimeToDate(String cal) {
		Calendar cale = Calendar.getInstance();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			cale.setTime(sdf.parse(cal));
		} catch (Exception e) {
		}

		return cale;
	}

	private class TaskGetDosare extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			Log.e("debug", "Intru in taskgetdosare");
			
			liss = new ArrayList<SedintaCurenta>();
			liss.clear();

			HelperSqlFunctions h = new HelperSqlFunctions(
					getApplicationContext());
			liss.addAll(h.getListaSedinteByDate(currentDate));
			
			SedintaCurenta s = new SedintaCurenta();

			for (int i = 0; i < liss.size() - 1; i++) {
				for (int j = i + 1; j < liss.size(); j++) {
					if (convertTimeToDate(
							liss.get(i).getDosarSedinta().getOra()).after(
							convertTimeToDate(liss.get(j).getDosarSedinta()
									.getOra()))) {
						s = liss.get(i);
						liss.set(i, liss.get(j));
						liss.set(j, s);
					}
				}
			}

			for (int i = 0; i < liss.size() - 1; i++) {
				for (int j = i + 1; j < liss.size(); j++) {
					if (liss.get(i).getDosarSedinta().getOra()
							.compareTo(liss.get(j).getDosarSedinta().getOra()) == 0) {
						liss.get(i).setBackground(1);
						liss.get(j).setBackground(1);
					}
				}
			}

			for (int i = 0; i < liss.size() - 1; i++) {
				for (int j = i + 1; j < liss.size(); j++) {

					if (liss.get(i).getInstitutie()
							.compareTo(liss.get(j).getInstitutie()) != 0) {
						liss.get(i).setBackground(1);
						liss.get(j).setBackground(1);
					}
				}
			}
			

//			test_optimizari();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			Log.e("debug", "Size - lista: " + liss.size());

			if (liss.size() > 0) {
				lad = new AdapterForListActivityAdaugaComandaNoua(
						MainActivity.this, i, liss);
				setListAdapter(lad);
				lad.notifyDataSetChanged();
				
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						TextView t = (TextView) findViewById(R.id.text_no_exista);
						t.setVisibility(View.INVISIBLE);
						
//						Log.e("dem test", "Testul suprem: "+test_optimizari().size());
						
					}
				});

			} else {
				liss.clear();
				lad = new AdapterForListActivityAdaugaComandaNoua(
						MainActivity.this, i, liss);
				setListAdapter(lad);
				lad.notifyDataSetChanged();

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						TextView t = (TextView) findViewById(R.id.text_no_exista);
						t.setVisibility(View.VISIBLE);
					}
				});

			}
			
			dialog2.dismiss();
			
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog2.setMessage("Se incarca datele...");
			dialog2.setCancelable(false);
			dialog2.show();
		}
	}


	AdapterForListActivityAdaugaComandaNoua lad;

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		Log.e("debug",  "Sunt aici");
		
		Intent inte = getIntent();

		if (inte.getIntExtra("id_dos", -5) != -5) {
			int ide = inte.getIntExtra("id_dos", -5);
			System.out.println("butu: " + ide);
		} else {
			System.out.println("buuut ma");
		}

		date.setText(currentDate.get(Calendar.DAY_OF_MONTH) + "");
		datemy.setText(EcrisUtils.getMonthByCalendar(currentDate) + " "
				+ currentDate.get(Calendar.YEAR));

		tv.setText(EcrisUtils.getDayByCalendar(currentDate));

		TaskGetDosare t = new TaskGetDosare();
		t.execute();

	}


	public boolean isOnline(Context context) {

		ConnectionDetector cd = new ConnectionDetector(getApplicationContext());

		Boolean isInternetPresent = cd.isConnectingToInternet();

		return isInternetPresent;

	}

	public Calendar convertStringToDate(String cal) {
		Calendar cale = Calendar.getInstance();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
			cale.setTime(sdf.parse(cal));
		} catch (Exception e) {
		}

		return cale;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		setContentView(R.layout.activity_main);
		drawer(savedInstanceState);
		
		Log.e("eroare", "baga mare");

		dialog2 = new ProgressDialog(MainActivity.this);

		Context context = getApplicationContext();

		SharedPreferences prefs = this.getSharedPreferences("com.example.app",
				Context.MODE_PRIVATE);

		int prima = prefs.getInt("prima", 0);
		if (prima == 0) {
			AlertDialog ad = new AlertDialog.Builder(this)
					.setMessage(R.string.terms)
					.setIcon(R.drawable.icon_avoapp)
					.setTitle("Termeni de utilizare")
					.setPositiveButton("Accepta", this)
					.setNegativeButton("Refuza", this)
					.setCancelable(false)
					.create();
			ad.show();
		}

		Log.e("de", "gioan1");

		if (isOnline(context)) {

			Log.e("de", "gioan2");

//			TaskGetUpdate tre = new TaskGetUpdate();
//			tre.execute();
		}

		Log.e("de", "gioan3");

		if (isOnline(context)) {
			TaskVerify tre = new TaskVerify();
			tre.execute();
		} else {
			if (prefs.getInt("blocat", 0) == 1) {
				Toast.makeText(getApplicationContext(),
						"Aplicatia este blocata!", Toast.LENGTH_LONG).show();
				finish();
			}
		}
		
//		Intent intent = new Intent(context, BackupService.class);
//
//		if (isOnline(context)) {
//			stopService(intent);
//			if (startService(intent) == null) {
//				context.startService(intent);
//				Log.d("service started", "service startedMain");
//			}
//
//		} else {
//			Log.d("de", "stop service");
//			context.stopService(intent);
//		}

		Intent intentel = getIntent();
		if (intentel.getStringExtra("data") != null) {
			currentDate = Calendar.getInstance();
			currentDate.setTimeInMillis(convertStringToDate(
					intentel.getStringExtra("data")).getTimeInMillis());
			currentDate.set(Calendar.HOUR_OF_DAY,
					currentDate.getActualMinimum(Calendar.HOUR_OF_DAY));
			currentDate.set(Calendar.MINUTE,
					currentDate.getActualMinimum(Calendar.MINUTE));
			currentDate.set(Calendar.SECOND,
					currentDate.getActualMinimum(Calendar.SECOND));
			currentDate.set(Calendar.MILLISECOND,
					currentDate.getActualMinimum(Calendar.MILLISECOND));

		} else {
			currentDate = Calendar.getInstance();
			currentDate.set(Calendar.HOUR_OF_DAY,
					currentDate.getActualMinimum(Calendar.HOUR_OF_DAY));
			currentDate.set(Calendar.MINUTE,
					currentDate.getActualMinimum(Calendar.MINUTE));
			currentDate.set(Calendar.SECOND,
					currentDate.getActualMinimum(Calendar.SECOND));
			currentDate.set(Calendar.MILLISECOND,
					currentDate.getActualMinimum(Calendar.MILLISECOND));
		}

		
		HelperSqlFunctions h = new HelperSqlFunctions(getApplicationContext());

		
		Calendar c = Calendar.getInstance();

		ImageButton img = (ImageButton) findViewById(R.id.addNew);

		img.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("debu", "E");
				Intent myIntent = new Intent(MainActivity.this,
						PlusActivity.class);
				MainActivity.this.startActivity(myIntent);
			}
		});

//		TaskGetDosare t = new TaskGetDosare();
//		t.execute();

		
		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");
		tv = (TextView) findViewById(R.id.dayofweek);
		tv.setTypeface(tf);
		tv.setText(EcrisUtils.getDayByCalendar(currentDate));

		tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("deb", "bus");
			}
		});

//		sqlHelper = new HelperSql(getApplicationContext());
//		sqlTable = sqlHelper.getWritableDatabase();

		date = (TextView) findViewById(R.id.data);
		datemy = (TextView) findViewById(R.id.tvmonthyear);
		datemy.setText(EcrisUtils.getMonthByCalendar(currentDate) + " "
				+ currentDate.get(Calendar.YEAR));

		date.setTypeface(tf);
		datemy.setTypeface(tf);

		date.setText(currentDate.get(Calendar.DAY_OF_MONTH) + "");

		back = (ImageButton) findViewById(R.id.backDate);
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				currentDate.add(Calendar.DAY_OF_YEAR, -1);

				i--;

				date.setText(currentDate.get(Calendar.DAY_OF_MONTH) + "");
				datemy.setText(EcrisUtils.getMonthByCalendar(currentDate) + " "
						+ currentDate.get(Calendar.YEAR));
				tv.setText(EcrisUtils.getDayByCalendar(currentDate));

				TaskGetDosare t = new TaskGetDosare();
				t.execute();
			}
		});

		next = (ImageButton) findViewById(R.id.nextDate);
		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				currentDate.add(Calendar.DAY_OF_YEAR, 1);

				i++;

				date.setText(currentDate.get(Calendar.DAY_OF_MONTH) + "");
				datemy.setText(EcrisUtils.getMonthByCalendar(currentDate) + " "
						+ currentDate.get(Calendar.YEAR));
				tv.setText(EcrisUtils.getDayByCalendar(currentDate));

				TaskGetDosare t = new TaskGetDosare();
				t.execute();
			}
		});

	}

	@Override
	public void onContentChanged() {
		// TODO Auto-generated method stub
		super.onContentChanged();

	}

	private float x1, x2;
	static final int MIN_DISTANCE = 150;

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		Log.d("onTouch", "Touch");
		// TODO Auto-generated method stub
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			x1 = event.getX();
			break;
		case MotionEvent.ACTION_UP:
			x2 = event.getX();
			float deltaX = x2 - x1;

			if (Math.abs(deltaX) > MIN_DISTANCE) {
				// Left to Right swipe action
				if (x2 > x1) {
					i++;

					// lad = new AdapterForListActivityAdaugaComandaNoua(
					// MainActivity.this, i);
					// setListAdapter(lad);

					date.setText(i + "");
				}

				// Right to left swipe action
				else {
					i--;
					// lad = new AdapterForListActivityAdaugaComandaNoua(
					// MainActivity.this, i);
					// setListAdapter(lad);

					date.setText(i + "");
				}

			} else {
				// consider as something else - a screen tap for example
			}
			break;
		}
		return super.onTouchEvent(event);

	}

}
