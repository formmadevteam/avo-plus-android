package calendarmanager.activities;

import java.util.Calendar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import calendaradapter.structures.DosarObservatii;
import calendarmanager.helpers.EcrisUtils;
import calendarmanager.helpers.HelperSqlFunctions;
import ecris.data.types.Dosar;

public class EditareDosarActivity extends Activity {

	public TextView tv;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.e("er", "Bum");
		return super.onOptionsItemSelected(item);
	}

	public String parseCal(Calendar c) {
		String s = c.get(Calendar.DAY_OF_MONTH) + "."
				+ (c.get(Calendar.MONTH) + 1) + "." + c.get(Calendar.YEAR);
		return s;
	}

	private class TaskDelete extends AsyncTask<Void, Void, Void> {

		int idd;

		Dosar dd;

		public TaskDelete(int id) {
			idd = id;
		}

		@Override
		protected Void doInBackground(Void... params) {
			EcrisUtils e = new EcrisUtils();
			HelperSqlFunctions h = new HelperSqlFunctions(
					EditareDosarActivity.this);
			h.deleteDosar(idd);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					
					Toast.makeText(getApplicationContext(),
							"Dosarul a fost sters!", Toast.LENGTH_LONG).show();
					DosarActivity.del=1;
					
					finish();
				}
			});

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

	}

	public Dosar dd;
	public HelperSqlFunctions h;
	public EditText edt1;
	public EditText edt2;

	ProgressDialog mProgressDialog;
	
	public static int ok = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_editare_dosar);

		ImageView im = (ImageView) findViewById(R.id.back_dosar);
		im.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub

				finish();

				return false;
			}
		});
		
		h = new HelperSqlFunctions(EditareDosarActivity.this);
		DosarObservatii dobs = h.getObservatii(DosarActivity.id);

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");

		TextView tv = (TextView) findViewById(R.id.title_list_dos);
		tv.setTypeface(tf);

		TextView tv2 = (TextView) findViewById(R.id.info_gen_red);
		tv2.setTypeface(tf);

		TextView tv3 = (TextView) findViewById(R.id.info_gen_obs);
		tv3.setTypeface(tf);

		edt1 = (EditText) findViewById(R.id.editTextNume);
		edt2 = (EditText) findViewById(R.id.editTextObs);

		edt1.setTypeface(tf);
		edt2.setTypeface(tf);

		edt1.setText(dobs.getNume());
		edt2.setText(dobs.getObservatii());

		Button btn = (Button) findViewById(R.id.btn_save_edit);
		btn.setTypeface(tf);

		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// Log.e("debug", "zbum");

				h.putObservatii(edt1.getText().toString(), edt2.getText()
						.toString(), DosarActivity.id);

				finish();
			}
		});

		Button btn2 = (Button) findViewById(R.id.btn_del_edit);
		btn2.setTypeface(tf);

		btn2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// Log.e("debug", "zbum");

				Intent i = getIntent();
				int id = i.getIntExtra("id_dosar", 0);

				TaskDelete t = new TaskDelete(id);
				t.execute();

				DosarActivity.del=1;
				
				finish();

			}
		});

		// dd = new Dosar();
		setTitle("");
		// Log.e("id_dosar", "Id dosar: " + id);

	}
}
