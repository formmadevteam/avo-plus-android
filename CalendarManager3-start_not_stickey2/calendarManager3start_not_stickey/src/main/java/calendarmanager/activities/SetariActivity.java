package calendarmanager.activities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import calendarmanager.helpers.HelperSqlFunctions;
import ecris.data.types.Dosar;

public class SetariActivity extends Activity implements
		DialogInterface.OnClickListener {

	// TODO Licenta
	// Contact
	// Notificari
	//

	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// // TODO Auto-generated method stub
	//
	// }
	
	
	public AlertDialog ad;
	
	@Override
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
		
	}

	private class TaskBackup extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			HelperSqlFunctions helperSqlFunctions = new HelperSqlFunctions(
					getApplicationContext());
			ArrayList<Dosar> list = helperSqlFunctions.getListaDosare();

			for (int i = 0; i < list.size() - 1; i++) {
				for (int j = i + 1; j < list.size(); j++) {
					if (list.get(i).getNumar()
							.compareTo(list.get(j).getNumar()) == 0) {
						list.remove(j);
					}
				}
			}

			SharedPreferences prefs = getApplicationContext()
					.getSharedPreferences("com.example.app",
							Context.MODE_PRIVATE);

			// $json = '[{"id_licenta": 12345, "id_dosar":
			// "4242/423/2355"},{"id_licenta": 12345, "id_dosar":
			// "4242/423/2355"}]';
			String s = "[";
			int licenta = prefs.getInt("cont", 0);
			if (licenta != 0) {
				for (int i = 0; i < list.size(); i++) {
					Dosar dosar = list.get(i);
					dosar.getDosarObservatii().setObservatii(
							dosar.getDosarObservatii().getObservatii()
									.replaceAll("\n", "<br/>"));
					if (i < list.size() - 1) {
						s += "{" + '"' + "id_licenta" + '"' + ": " + '"' + ""
								+ licenta + "" + '"' + ", " + '"' + "id_dosar"
								+ '"' + ": " + '"' + "" + dosar.getNumar() + ""
								+ '"' + ", " + '"' + "nume" + '"' + ": " + '"'
								+ "" + dosar.getDosarObservatii().getNume()
								+ "" + '"' + ", " + '"' + "observatii" + '"'
								+ ": " + '"' + ""
								+ dosar.getDosarObservatii().getObservatii()
								+ "" + '"' + "},";
					} else {
						s += "{" + '"' + "id_licenta" + '"' + ": " + '"' + ""
								+ licenta + "" + '"' + ", " + '"' + "id_dosar"
								+ '"' + ": " + '"' + "" + dosar.getNumar() + ""
								+ '"' + ", " + '"' + "nume" + '"' + ": " + '"'
								+ "" + dosar.getDosarObservatii().getNume()
								+ "" + '"' + ", " + '"' + "observatii" + '"'
								+ ": " + '"' + ""
								+ dosar.getDosarObservatii().getObservatii()
								+ "" + '"' + "}";
					}
				}
				s += "]";
			}

			System.out.println(s);

			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(
					"http://boghislastuf.ro/sistemlicentiere/upload.php");

			System.out.println("ble");

			List<NameValuePair> pairs = new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair("id_licenta", licenta + ""));
			pairs.add(new BasicNameValuePair("dosare", s));

			HttpResponse response = null;
			try {
				post.setEntity(new UrlEncodedFormEntity(pairs));

				response = client.execute(post);

			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);

			}
			String responseText = null;
			try {
				responseText = EntityUtils.toString(response.getEntity());
			} catch (Exception e) {

			}

			System.out.println(responseText);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			//
			// dialog2.dismiss();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// dialog2.setMessage("Se salveaza datele...");
			// dialog2.show();
		}
	}

	private void downloadapk() {
		try {

			URL url = new URL(
					"http://boghislastuf.ro/sistemlicentiere/CalendarManager3-.apk");
			HttpURLConnection urlConnection = (HttpURLConnection) url
					.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.setDoOutput(true);
			urlConnection.connect();

			File sdcard = Environment.getExternalStorageDirectory();
			File file = new File(sdcard, "CalendarManager.apk");

			FileOutputStream fileOutput = new FileOutputStream(file);
			InputStream inputStream = urlConnection.getInputStream();

			byte[] buffer = new byte[1024];
			int bufferLength = 0;

			while ((bufferLength = inputStream.read(buffer)) > 0) {
				fileOutput.write(buffer, 0, bufferLength);
			}
			fileOutput.close();
			// this.checkUnknownSourceEnability();
			// this.initiateInstallation();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Intent intent = new Intent(Intent.ACTION_VIEW);
		Uri uri = Uri.fromFile(new File("/sdcard/CalendarManager.apk"));
		intent.setDataAndType(uri, "application/vnd.android.package-archive");
		startActivity(intent);
	}

	private ProgressDialog dialog2;

	private class TaskGetBackup extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			SharedPreferences prefs = getApplicationContext()
					.getSharedPreferences("com.example.app",
							Context.MODE_PRIVATE);
			int cur_ver = prefs.getInt("version", 1);

			if (cur_ver < getVersion()) {
				TaskBackup t = new TaskBackup();
				t.execute();

				downloadapk();
			} else {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						Toast.makeText(getApplicationContext(),
								"Nu exista versiuni noi!", Toast.LENGTH_LONG)
								.show();
					}
				});
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			dialog2.dismiss();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog2.setMessage("Se slveaza datele...");
			dialog2.show();
		}
	}

	public int getVersion() {

		int version = 1;

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(
				"http://boghislastuf.ro/sistemlicentiere/version.php");

		HttpResponse response = null;
		try {

			response = client.execute(post);

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);

		}
		String responseText = null;
		try {
			responseText = EntityUtils.toString(response.getEntity());
		} catch (Exception e) {

		}

		System.out.println(responseText);

		version = Integer.valueOf(responseText);

		// return 1;
		return version;
	}

	public boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		// should check null because in air plan mode it will be null
		if (netInfo != null && netInfo.isConnected()) {
			return true;
		}
		return false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_setari);

		dialog2 = new ProgressDialog(SetariActivity.this);

		ImageView imm = (ImageView) findViewById(R.id.back_dosar_pic);
		imm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		TextView tv = (TextView) findViewById(R.id.dataDosarId);
		TextView tv2 = (TextView) findViewById(R.id.info_gen);

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");

		tv.setTypeface(tf);
		tv2.setTypeface(tf);

		Button btn1 = (Button) findViewById(R.id.btn_set_licent);
		btn1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i = new Intent(SetariActivity.this,
						LicentaActivity.class);
				startActivity(i);

			}
		});

		btn1.setTypeface(tf);

		Button btn2 = (Button) findViewById(R.id.btn_set_notif);
		btn2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SetariActivity.this,
						SetariNotificariActivity.class);
				startActivity(i);

			}
		});

		btn2.setTypeface(tf);

		Button btn3 = (Button) findViewById(R.id.btn_set_contact);
		btn3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SetariActivity.this,
						ActivityContact.class);
				startActivity(i);

			}
		});

		btn3.setTypeface(tf);

		
		ad = new AlertDialog.Builder(
				this).setMessage(R.string.terms)
				.setIcon(R.drawable.icon_avoapp)
				.setTitle("Termeni de utilizare")
				.setPositiveButton("Inchide", this)
				.setCancelable(false).create();
		
		Button btn5 = (Button) findViewById(R.id.btn_set_acord);
		btn5.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				ad.show();
				
			}
		});
		btn5.setTypeface(tf);

		Button btn4 = (Button) findViewById(R.id.btn_set_act);
		btn4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isOnline(getApplicationContext())) {
					TaskGetBackup t = new TaskGetBackup();
					t.execute();
				} else {
					Toast.makeText(getApplicationContext(),
							"Este necesara conexiunea la internet",
							Toast.LENGTH_LONG).show();
				}

			}
		});

		btn4.setTypeface(tf);

	}

}
