package calendarmanager.activities;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import calendarmanager.adapters.AdapterForSedinteSimple;
import calendarmanager.helpers.HelperSqlFunctions;
import ecris.data.types.Dosar;
import ecris.data.types.DosarSedinta;

public class ListaSedinteActivity extends ListActivity {

	AdapterForSedinteSimple lad;

	ArrayList<DosarSedinta> liss;

	Dosar dd;
	
	public String parseCal(Calendar c) {
		String s = c.get(Calendar.DAY_OF_MONTH) + "."
				+ (c.get(Calendar.MONTH) + 1) + "." + c.get(Calendar.YEAR);
		return s;
	}
	
	private class TaskGetDosare extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			HelperSqlFunctions h = new HelperSqlFunctions(
					getApplicationContext());
			liss = new ArrayList<DosarSedinta>();
			liss = h.getDosar(DosarActivity.id).getSedinte();

			dd = h.getDosar(DosarActivity.id);
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			lad = new AdapterForSedinteSimple(ListaSedinteActivity.this, liss);
			setListAdapter(lad);

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int arg2, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, arg2, id);
		// Log.e("de buta", "Arg: "+position+" - "+liss.get(position).getId());
		// Intent myIntent = new Intent(ListaSedinteActivity.this,
		// DosarActivity.class);
		//

		// if (ok != 0)
		// DosarActivity.id = liss_search.get(position).getId();
		// else
		// DosarActivity.id = liss.get(position).getId();
		//
		// ListaSedinteActivity.this.startActivity(myIntent);
		//
		// edt1.setText("");

		Intent i = new Intent(getApplicationContext(), SedintaActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		if (dd.getSedinte().get(arg2).getDocumentSedinta() == null
				|| dd.getSedinte().get(arg2).getDocumentSedinta() == "anyType{}") {
			i.putExtra("doc", parseCal(dd.getSedinte().get(arg2)
					.getDataDocument()));
		} else {
			i.putExtra("doc", dd.getSedinte().get(arg2).getDocumentSedinta()
					+ " "
					+ parseCal(dd.getSedinte().get(arg2).getDataDocument()));
		}

		
		if (dd.getSedinte().get(arg2).getSolutie() == null
				|| dd.getSedinte().get(arg2).getSolutie()
						.compareTo("anyType{}") == 0) {
			i.putExtra("tip", "-");
		} else {
			i.putExtra("tip", dd.getSedinte().get(arg2).getSolutie());
		}

		if (dd.getSedinte().get(arg2).getSolutieSumar() == "null"
				|| dd.getSedinte().get(arg2).getSolutieSumar()
						.compareTo("anyType{}") == 0) {

			i.putExtra("sol", "-");

		} else {
			i.putExtra("sol", dd.getSedinte().get(arg2).getSolutieSumar());
		}

		i.putExtra("data", parseCal(dd.getSedinte().get(arg2).getData()));
		i.putExtra("ora", dd.getSedinte().get(arg2).getOra());
		i.putExtra("complet", dd.getSedinte().get(arg2).getComplet());

		getApplicationContext().startActivity(i);

	}

	TextView tv1;
	TextView tv2;

	EditText edt1;

	int ok = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_list_sedinte);
		setTitle("");

		TaskGetDosare t = new TaskGetDosare();
		t.execute();

		// liss_search = new ArrayList<DosarSimplif>();

		ImageView img2 = (ImageView) findViewById(R.id.back_dosar);
		img2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");

		// tv1 = (TextView) findViewById(R.id.text_cauta_lista_dos);
		// tv2 = (TextView) findViewById(R.id.title_list_dos);
		// tv1.setTypeface(tf);
		// tv2.setTypeface(tf);

		// lad = new AdapterForSedinte(
		// ListaSedinteActivity.this, );
		// setListAdapter(lad);

	}
}
