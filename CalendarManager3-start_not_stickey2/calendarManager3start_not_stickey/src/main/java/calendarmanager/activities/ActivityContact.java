package calendarmanager.activities;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class ActivityContact extends Activity {

	public TextView tv;
	public TextView tv1;
	public TextView tv2;
	public TextView tv3;
	public TextView tv4;	

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_screen);

		tv = (TextView) findViewById(R.id.dataDosarId);
		tv1 = (TextView) findViewById(R.id.num);
		tv2 = (TextView) findViewById(R.id.num2);
		tv3 = (TextView) findViewById(R.id.num3);
		tv4 = (TextView) findViewById(R.id.num4);
		

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");

		tv.setTypeface(tf);
		tv1.setTypeface(tf);
		tv2.setTypeface(tf);
		tv3.setTypeface(tf);
		tv4.setTypeface(tf);

		

		ImageView img = (ImageView)findViewById(R.id.back_dosar_pic);
		img.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
			
				finish();
				return false;
			}
		});
		
	
	}


}
