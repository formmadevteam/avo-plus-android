package calendarmanager.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;

public class ActivitySplash extends Activity {

	public TextView tv;
	public TextView tv1;
	public TextView tv2;
	public TextView tv3;
	public TextView tv4;

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);

		Log.e("hai", "Hai odata in plm!");

		System.out.println("Pula mee");
		// tv = (TextView) findViewById(R.id.textView1g);

		tv1 = (TextView) findViewById(R.id.num);
		tv2 = (TextView) findViewById(R.id.num2);
		tv3 = (TextView) findViewById(R.id.num3);
		tv4 = (TextView) findViewById(R.id.num4);

		Typeface tf = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Light.ttf");

		
		// tv.setTypeface(tf);
		tv1.setTypeface(tf);
		tv2.setTypeface(tf);
		tv3.setTypeface(tf);
		tv4.setTypeface(tf);

		prefs = this.getSharedPreferences("com.example.app",
				Context.MODE_PRIVATE);
		editor = prefs.edit();

		// VerifyVersion v = new VerifyVersion(this);
		// v.execute();

		if (prefs.getInt("cont", 0) == 0) {

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						public void run() {
							Intent start = new Intent(getApplicationContext(),
									LicentaActivity.class);
							start.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							getApplicationContext().startActivity(start);

							finish();

						}
					}, 2000);

				}
			});

		} else {

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						public void run() {
							Intent start = new Intent(getApplicationContext(),
									MainActivity.class);
							start.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							getApplicationContext().startActivity(start);

							finish();

						}
					}, 2500);

				}
			});
		}

	}

	SharedPreferences prefs;
	SharedPreferences.Editor editor;



}
