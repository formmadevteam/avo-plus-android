package calendarmanager.loaders;

import java.util.ArrayList;

import android.content.Context;
import calendaradapter.structures.DosarSimplif;
import calendarmanager.helpers.HelperSqlFunctions;

public class AllDosareLoader extends BaseLoader {

	public AllDosareLoader(Context context) {
		super(context);
	}
	
	ArrayList liss, liss_search;
	
	@Override
	protected Void doInBackground(Void... params) {
		
		HelperSqlFunctions h = new HelperSqlFunctions(
				context);

		liss = new ArrayList<DosarSimplif>();
		liss = h.getListaDosareSimplif();

		liss_search = new ArrayList<DosarSimplif>();

		liss_search.addAll(liss);
//		return liss;
		return null;
		
	}
	

}
