package calendarmanager.loaders;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class BaseLoader extends AsyncTask<Void, Void, Void> {

	/**
	 * Dialog wich blocks the UI waiting for doInBackground to finish
	 */
	ProgressDialog pbar;
	
	/**
	 * Context for BaseLoader 
	 */
	Context context;
	
	public BaseLoader(Context context) {
		this.context = context;
	}
	
	@Override
	protected void onPreExecute() {
		pbar = new ProgressDialog(context);
		pbar.setMessage("Se incarca...");
		pbar.setCancelable(false);
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		pbar.dismiss();
	}

}
