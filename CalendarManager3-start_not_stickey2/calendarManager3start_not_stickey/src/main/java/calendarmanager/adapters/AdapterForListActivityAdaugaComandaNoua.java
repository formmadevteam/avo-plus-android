package calendarmanager.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import calendaradapter.structures.SedintaCurenta;
import calendarmanager.activities.R;

public class AdapterForListActivityAdaugaComandaNoua extends BaseAdapter {

	private Activity context;
	private int id;
	ListView mListView;
	private ArrayList<SedintaCurenta> lista;

	public AdapterForListActivityAdaugaComandaNoua(Activity activity, int id,
			ArrayList<SedintaCurenta> lista) {
		this.context = activity;
		this.id = id;
		this.lista = lista;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final int p = position;

		View element = null;

		LayoutInflater inflater = context.getLayoutInflater();

		element = (View) inflater.inflate(R.layout.casuta_event, null);
		TextView ora = (TextView) element.findViewById(R.id.lista_simpla_nume);
		TextView nume = (TextView) element.findViewById(R.id.textView1);
		TextView descriere = (TextView) element.findViewById(R.id.textView2);
		
		ImageView im = (ImageView)element.findViewById(R.id.img_dos);

		Typeface tf = Typeface.createFromAsset(context.getAssets(),
				"fonts/OpenSans-Light.ttf");
		Typeface tf2 = Typeface.createFromAsset(context.getAssets(),
				"fonts/OpenSans-Regular.ttf");

		descriere.setTypeface(tf);
		nume.setTypeface(tf2);
		ora.setTypeface(tf);

		RelativeLayout r = (RelativeLayout) element
				.findViewById(R.id.layout_casuta);
		// if (lista.get(position).getBackground() != 0) {
		// r.setBackgroundResource(R.drawable.background_casuta_danger);
		// }
		//
		// else {

		if (lista.get(position).getBackground()!=0) {
			im.setBackgroundResource(R.drawable.icon_dosar_single_alert);
		}
		else{
			im.setBackgroundResource(R.drawable.icon_dosar_single);
		}
		
		if (p % 2 == 1) {
			r.setBackgroundResource(R.drawable.background_casuta);
		}

		else {
			r.setBackgroundResource(R.drawable.background_casuta2);
		}
		// }

		descriere.setText(lista.get(position).getDosarSedinta().getOra());

		if (lista.get(position).getNume().length() == 0) {
			nume.setText("Dosar #" + lista.get(position).getDosar());
		} else {
			nume.setText(lista.get(position).getNume() + "");
		}

		ora.setText(lista.get(position).getInstitutie());

		return element;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void update_discount(int j) {
		//
		// HelperSql sqlHelper = new HelperSql(context);
		// SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();
		//
		// int cat_pret = Integer.valueOf(list.get(0).cat_pret);
		//
		// String[] COLUMNS = { "pret_vanz1", "pret_vanz2", "pret_vanz3",
		// "cod_prod", "den_produs" };
		// Cursor cursor = sqlTable.query("PRODUS", COLUMNS,
		// "cod_prod = '" + list.get(j).id + "'", null, null, null, null);
		//
		// if (cursor != null && cursor.moveToFirst()) {
		//
		// }
		// sqlTable.close();
		// sqlHelper.close();
		//
	}

}
