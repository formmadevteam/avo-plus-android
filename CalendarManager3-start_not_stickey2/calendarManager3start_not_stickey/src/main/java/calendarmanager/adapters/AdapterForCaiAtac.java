package calendarmanager.adapters;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import calendarmanager.activities.R;
import ecris.data.types.DosarCaleAtac;

public class AdapterForCaiAtac extends BaseAdapter {
	private Activity context;
	ListView mListView;
	private ArrayList<DosarCaleAtac> lista;

	public AdapterForCaiAtac(Activity activity, ArrayList<DosarCaleAtac> lista) {
		this.context = activity;
		this.lista = lista;
	}

	@Override
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		return super.areAllItemsEnabled();

	}

	public String parseCal(Calendar c) {
		String s = c.get(Calendar.DAY_OF_MONTH) + "."
				+ (c.get(Calendar.MONTH) + 1) + "." + c.get(Calendar.YEAR);
		return s;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final int p = position;

		View element = null;
		
		if (lista.size() <= 5) {

			LayoutInflater inflater = context.getLayoutInflater();

			element = (View) inflater.inflate(R.layout.casuta_cale_atac, null);

			if (p % 2 == 1) {
				element.setBackgroundResource(R.drawable.background_casuta);
			}

			else {
				element.setBackgroundResource(R.drawable.background_casuta2);
			}

			TextView nume = (TextView) element.findViewById(R.id.d_c_nume);

			TextView cal = (TextView) element.findViewById(R.id.d_c_cale);

			TextView data = (TextView) element.findViewById(R.id.d_c_data);

			Typeface tf = Typeface.createFromAsset(context.getAssets(),
					"fonts/OpenSans-Light.ttf");
			Typeface tf2 = Typeface.createFromAsset(context.getAssets(),
					"fonts/OpenSans-Regular.ttf");

			nume.setTypeface(tf);
			cal.setTypeface(tf);
			data.setTypeface(tf2);

			nume.setText(lista.get(position).getParteDeclaratoare() + "");
			data.setText(parseCal(lista.get(position).getDataDeclarare()) + "");
			cal.setText(lista.get(position).getTipCaleAtac() + "");
			
			if (lista.get(position).getParteDeclaratoare().compareTo("anyType{}") != 0)
				nume.setText(lista.get(position).getParteDeclaratoare() + "");
			else
				nume.setText("-");
			
		} else {
			if (position != 5) {

				LayoutInflater inflater = context.getLayoutInflater();

				element = (View) inflater.inflate(R.layout.casuta_cale_atac, null);

				if (p % 2 == 1) {
					element.setBackgroundResource(R.drawable.background_casuta);
				}

				else {
					element.setBackgroundResource(R.drawable.background_casuta2);
				}

				TextView nume = (TextView) element.findViewById(R.id.d_c_nume);

				TextView cal = (TextView) element.findViewById(R.id.d_c_cale);

				TextView data = (TextView) element.findViewById(R.id.d_c_data);

				Typeface tf = Typeface.createFromAsset(context.getAssets(),
						"fonts/OpenSans-Light.ttf");
				Typeface tf2 = Typeface.createFromAsset(context.getAssets(),
						"fonts/OpenSans-Regular.ttf");

				nume.setTypeface(tf);
				cal.setTypeface(tf);
				data.setTypeface(tf2);

				nume.setText(lista.get(position).getParteDeclaratoare() + "");
				data.setText(parseCal(lista.get(position).getDataDeclarare()) + "");
				cal.setText(lista.get(position).getTipCaleAtac() + "");
				

				if (lista.get(position).getParteDeclaratoare().compareTo("anyType{}") != 0)
					nume.setText(lista.get(position).getParteDeclaratoare() + "");
				else
					nume.setText("-");
			}
			else{

				LayoutInflater inflater = context.getLayoutInflater();

				element = (View) inflater.inflate(R.layout.casuta_sedint_min_all, null);

				if (p % 2 == 1) {
					element.setBackgroundResource(R.drawable.background_casuta);
				}

				else {
					element.setBackgroundResource(R.drawable.background_casuta2);
				}
			}
		}

		return element;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if (lista.size() > 5)
			return 6;
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
