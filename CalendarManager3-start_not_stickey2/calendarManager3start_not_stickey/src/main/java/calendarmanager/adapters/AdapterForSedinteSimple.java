package calendarmanager.adapters;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import calendarmanager.activities.R;
import ecris.data.types.DosarSedinta;

public class AdapterForSedinteSimple extends BaseAdapter {
	private Activity context;
	ListView mListView;
	private ArrayList<DosarSedinta> lista;

	public String parseCal(Calendar c) {
		String s = c.get(Calendar.DAY_OF_MONTH) + "."
				+ (c.get(Calendar.MONTH) + 1) + "." + c.get(Calendar.YEAR);
		return s;
	}

	public AdapterForSedinteSimple(Activity activity, ArrayList<DosarSedinta> lista) {
		this.context = activity;
		this.lista = lista;
	}

	@Override
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		return super.areAllItemsEnabled();

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final int p = position;
		
		View element = null;

		
			LayoutInflater inflater = context.getLayoutInflater();

			element = (View) inflater.inflate(R.layout.casuta_sedint_min, null);
			if (p % 2 == 1) {
				element.setBackgroundResource(R.drawable.background_casuta);
			}

			else {
				element.setBackgroundResource(R.drawable.background_casuta2);
			}

			TextView data = (TextView) element.findViewById(R.id.d_sed_data);
			TextView tip_sol = (TextView) element
					.findViewById(R.id.d_sed_tip_sol);


			Typeface tf = Typeface.createFromAsset(context.getAssets(),
					"fonts/OpenSans-Light.ttf");
			Typeface tf2 = Typeface.createFromAsset(context.getAssets(),
					"fonts/OpenSans-Regular.ttf");

			data.setTypeface(tf2);

			tip_sol.setTypeface(tf);
			data.setText(parseCal(lista.get(position).getData()) + "");
			if (lista.get(position).getSolutie().compareTo("anyType{}") != 0)
				tip_sol.setText(lista.get(position).getSolutie() + "");
			else
				tip_sol.setText("-");
		return element;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lista.size();

	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
