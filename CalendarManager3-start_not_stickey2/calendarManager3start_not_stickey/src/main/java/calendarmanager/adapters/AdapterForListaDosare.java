package calendarmanager.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import calendaradapter.structures.DosarSimplif;
import calendarmanager.activities.R;
import calendarmanager.helpers.TagListaDosare;

public class AdapterForListaDosare extends BaseAdapter {

	private Activity context;
	ListView mListView;
	private ArrayList<DosarSimplif> lista;
	private Typeface tf, tf2;

	public AdapterForListaDosare(Activity activity,
			ArrayList<DosarSimplif> lista) {
		this.context = activity;
		this.lista = lista;

		tf = Typeface.createFromAsset(context.getAssets(),
				"fonts/OpenSans-Light.ttf");
		tf2 = Typeface.createFromAsset(context.getAssets(),
				"fonts/OpenSans-Regular.ttf");
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

//		final int p = position;
		int p = position;
		View element;

		if (convertView == null) {

			LayoutInflater inflater = context.getLayoutInflater();
			element = (View) inflater.inflate(R.layout.casuta_dosar, null);

			TagListaDosare tagDosar = new TagListaDosare();
			tagDosar.nume = (TextView) element.findViewById(R.id.casuta_dosa);
			tagDosar.loc = (TextView) element.findViewById(R.id.casuta_loc);
			tagDosar.r = (RelativeLayout) element
					.findViewById(R.id.layout_casuta);
			

			tagDosar.nume.setTypeface(tf2);

			element.setTag(tagDosar);
		} else {
			element = convertView;
		}

		TagListaDosare tagDosar = (TagListaDosare) element.getTag();

		if (p % 2 == 1) {
			tagDosar.r.setBackgroundResource(R.drawable.background_casuta);
		}

		else {
			tagDosar.r.setBackgroundResource(R.drawable.background_casuta2);
		}

		if (lista.get(position).getNume().length() == 0) {
			tagDosar.nume.setText("Dosar #" + lista.get(position).getNumar());
		} else {
			tagDosar.nume.setText(lista.get(position).getNume() + "");
		}

		tagDosar.loc.setText(lista.get(position).getLocatie());

		return element;

		//
		// final int p = position;
		//
		// View element = null;
		//
		// LayoutInflater inflater = context.getLayoutInflater();
		//
		// element = (View) inflater.inflate(R.layout.casuta_dosar, null);
		// TextView nume = (TextView) element.findViewById(R.id.casuta_dosa);
		// TextView loc = (TextView) element.findViewById(R.id.casuta_loc);
		//
		// Typeface tf = Typeface.createFromAsset(context.getAssets(),
		// "fonts/OpenSans-Light.ttf");
		// Typeface tf2 = Typeface.createFromAsset(context.getAssets(),
		// "fonts/OpenSans-Regular.ttf");
		//
		// nume.setTypeface(tf2);
		//
		// RelativeLayout r = (RelativeLayout) element
		// .findViewById(R.id.layout_casuta);
		//
		// if (p % 2 == 1) {
		// r.setBackgroundResource(R.drawable.background_casuta);
		// }
		//
		// else {
		// r.setBackgroundResource(R.drawable.background_casuta2);
		// }
		//
		//
		// // nume.setText("Dosar #" + lista.get(position).getNumar());
		//
		// if (lista.get(position).getNume().length() == 0) {
		// nume.setText("Dosar #" + lista.get(position).getNumar());
		// } else {
		// nume.setText(lista.get(position).getNume()+"");
		// }
		//
		// loc.setText(lista.get(position).getLocatie());
		//
		//
		//
		// return element;
		//
		//
		//
		//
		//

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void update_discount(int j) {
		//
		// HelperSql sqlHelper = new HelperSql(context);
		// SQLiteDatabase sqlTable = sqlHelper.getReadableDatabase();
		//
		// int cat_pret = Integer.valueOf(list.get(0).cat_pret);
		//
		// String[] COLUMNS = { "pret_vanz1", "pret_vanz2", "pret_vanz3",
		// "cod_prod", "den_produs" };
		// Cursor cursor = sqlTable.query("PRODUS", COLUMNS,
		// "cod_prod = '" + list.get(j).id + "'", null, null, null, null);
		//
		// if (cursor != null && cursor.moveToFirst()) {
		//
		// }
		// sqlTable.close();
		// sqlHelper.close();
		//
	}

}
