package calendarmanager.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import calendarmanager.activities.R;
import ecris.data.types.DosarParte;

public class AdapterForPartiSimple extends BaseAdapter {
	private Activity context;
	ListView mListView;
	private ArrayList<DosarParte> lista;

	public AdapterForPartiSimple(Activity activity, ArrayList<DosarParte> lista) {
		this.context = activity;
		this.lista = lista;
	}

	@Override
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		return super.areAllItemsEnabled();

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final int p = position;

		View element = null;

		LayoutInflater inflater = context.getLayoutInflater();

		element = (View) inflater.inflate(R.layout.casuta_parte, null);

		if (p % 2 == 1) {
			element.setBackgroundResource(R.drawable.background_casuta);
		}

		else {
			element.setBackgroundResource(R.drawable.background_casuta2);
		}

		TextView nume = (TextView) element.findViewById(R.id.d_nume_parte);
		TextView cal = (TextView) element.findViewById(R.id.d_cal_parte);

		Typeface tf = Typeface.createFromAsset(context.getAssets(),
				"fonts/OpenSans-Light.ttf");
		Typeface tf2 = Typeface.createFromAsset(context.getAssets(),
				"fonts/OpenSans-Regular.ttf");

		nume.setTypeface(tf2);
		cal.setTypeface(tf);

		nume.setText(lista.get(position).getNume());
		cal.setText(lista.get(position).getCalitateParte());
		return element;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
