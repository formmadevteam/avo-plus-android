package calendarmanager.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import calendarmanager.activities.R;

public class AdapterForMenu extends BaseAdapter{
	private Activity context;
	ListView mListView;

	public AdapterForMenu(Activity activity) {
		this.context = activity;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final int p = position;

		View element = null;

		LayoutInflater inflater = context.getLayoutInflater();
		
		element = (View) inflater.inflate(
				R.layout.drawer_list_item, null);
		
		if(p==0){
			element.setBackgroundResource(R.drawable.buton_meniu_calendar);
		}
		
		if(p==1){
			element.setBackgroundResource(R.drawable.buton_meniu_dosare);
		}
		
		if(p==2){
			element.setBackgroundResource(R.drawable.buton_meniu_backup);
		}
		
		if(p==3){
			element.setBackgroundResource(R.drawable.buton_meniu_setari);
		}
	
		
		return element;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
//		return lista.size();
		return 4;
		
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
