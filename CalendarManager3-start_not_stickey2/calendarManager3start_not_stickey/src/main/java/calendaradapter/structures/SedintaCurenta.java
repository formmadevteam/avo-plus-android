package calendaradapter.structures;

import ecris.data.types.DosarSedinta;

public class SedintaCurenta {

	int id_dosar;
	String dosar;
	String institutie;
	DosarSedinta dosarSedinta;
	String nume;
	int background = 0;
	
	public int getBackground() {
		return background;
	}
	public void setBackground(int background) {
		this.background = background;
	}
	public int getId_dosar() {
		return id_dosar;
	}
	public void setId_dosar(int id_dosar) {
		this.id_dosar = id_dosar;
	}
	public String getDosar() {
		return dosar;
	}
	public void setDosar(String dosar) {
		this.dosar = dosar;
	}
	public DosarSedinta getDosarSedinta() {
		return dosarSedinta;
	}
	public void setDosarSedinta(DosarSedinta dosarSedinta) {
		this.dosarSedinta = dosarSedinta;
	}
	public String getInstitutie() {
		return institutie;
	}
	public void setInstitutie(String institutie) {
		this.institutie = institutie;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	
}
