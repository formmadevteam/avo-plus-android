package calendaradapter.structures;

import java.util.ArrayList;
import java.util.Calendar;

import ecris.data.types.DosarParte;

public class DosarSimplif {

	int id;
	String numar;
	String locatie;
	String nume;
	
	ArrayList<DosarParte> lista_parti;
	
	public ArrayList<DosarParte> getLista_parti() {
		return lista_parti;
	}
	public void setLista_parti(ArrayList<DosarParte> lista_parti) {
		this.lista_parti = lista_parti;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNumar() {
		return numar;
	}
	public void setNumar(String numar) {
		this.numar = numar;
	}
	public String getLocatie() {
		return locatie;
	}
	public void setLocatie(String locatie) {
		this.locatie = locatie;
	}
	
}
