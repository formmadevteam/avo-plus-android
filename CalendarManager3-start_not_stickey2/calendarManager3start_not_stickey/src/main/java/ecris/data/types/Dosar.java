package ecris.data.types;

import java.util.ArrayList;
import java.util.Calendar;

import calendaradapter.structures.DosarObservatii;

public class Dosar {
	private int id;
	private String numar; // da
 	private String numarVechi; // da
	private Calendar data; // da
	private String institutie; // da
	private String departament; // da
	private String categorieCaz; // da
	private String stadiuProcesual; // da
	private ArrayList<DosarParte> parti; // da
	private ArrayList<DosarSedinta> sedinte; // da
	private ArrayList<DosarCaleAtac> caiAtac; // da
	private String obiect;
	private Calendar dataModificare;
	private DosarObservatii dosarObservatii;
	
	
	public DosarObservatii getDosarObservatii() {
		return dosarObservatii;
	}
	public void setDosarObservatii(DosarObservatii dosarObservatii) {
		this.dosarObservatii = dosarObservatii;
	}
	public String getNumar() {
		return numar;
	}
	public void setNumar(String numar) {
		this.numar = numar;
	}
	public String getNumarVechi() {
		return numarVechi;
	}
	public void setNumarVechi(String numarVechi) {
		this.numarVechi = numarVechi;
	}
	public String getInstitutie() {
		return institutie;
	}
	public void setInstitutie(String institutie) {
		this.institutie = institutie;
	}
	public String getDepartament() {
		return departament;
	}
	public void setDepartament(String departament) {
		this.departament = departament;
	}
	public String getCategorieCaz() {
		return categorieCaz;
	}
	public void setCategorieCaz(String categorieCaz) {
		this.categorieCaz = categorieCaz;
	}
	public String getStadiuProcesual() {
		return stadiuProcesual;
	}
	public void setStadiuProcesual(String stadiuProcesual) {
		this.stadiuProcesual = stadiuProcesual;
	}
	public ArrayList<DosarParte> getParti() {
		return parti;
	}
	public void setParti(ArrayList<DosarParte> parti) {
		this.parti = parti;
	}
	public ArrayList<DosarSedinta> getSedinte() {
		return sedinte;
	}
	public void setSedinte(ArrayList<DosarSedinta> sedinte) {
		this.sedinte = sedinte;
	}
	public ArrayList<DosarCaleAtac> getCaiAtac() {
		return caiAtac;
	}
	public void setCaiAtac(ArrayList<DosarCaleAtac> caiAtac) {
		this.caiAtac = caiAtac;
	}
	public Calendar getData() {
		return data;
	}
	public void setData(Calendar data) {
		this.data = data;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getObiect() {
		return obiect;
	}
	public void setObiect(String obiect) {
		this.obiect = obiect;
	}
	public Calendar getDataModificare() {
		return dataModificare;
	}
	public void setDataModificare(Calendar dataModificare) {
		this.dataModificare = dataModificare;
	}
}
