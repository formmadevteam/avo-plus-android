package ecris.data.types;

import java.util.Calendar;


public class DosarCaleAtac {
	private Calendar dataDeclarare;
	private String parteDeclaratoare;
	private String tipCaleAtac;
	
	public String getParteDeclaratoare() {
		return parteDeclaratoare;
	}
	public void setParteDeclaratoare(String parteDeclaratoare) {
		this.parteDeclaratoare = parteDeclaratoare;
	}
	public String getTipCaleAtac() {
		return tipCaleAtac;
	}
	public void setTipCaleAtac(String tipCaleAtac) {
		this.tipCaleAtac = tipCaleAtac;
	}
	public Calendar getDataDeclarare() {
		return dataDeclarare;
	}
	public void setDataDeclarare(Calendar dataDeclarare) {
		this.dataDeclarare = dataDeclarare;
	}
}
