package ecris.data.types;

public class DosarParte {
	private String nume;
	private String calitateParte;
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getCalitateParte() {
		return calitateParte;
	}
	public void setCalitateParte(String calitateParte) {
		this.calitateParte = calitateParte;
	}
}
