package ecris.data.types;

import java.util.ArrayList;
import java.util.Calendar;
public class Sedinta {
	private String departament;
	private String complet;
	private Calendar data;
	private String ora;
	private ArrayList<SedintaDosar> dosare;
	public String getDepartament() {
		return departament;
	}
	public void setDepartament(String departament) {
		this.departament = departament;
	}
	public String getComplet() {
		return complet;
	}
	public void setComplet(String complet) {
		this.complet = complet;
	}
	public String getOra() {
		return ora;
	}
	public void setOra(String ora) {
		this.ora = ora;
	}
	public ArrayList<SedintaDosar> getDosare() {
		return dosare;
	}
	public void setDosare(ArrayList<SedintaDosar> dosare) {
		this.dosare = dosare;
	}
	public Calendar getData() {
		return data;
	}
	public void setData(Calendar data) {
		this.data = data;
	}
}
