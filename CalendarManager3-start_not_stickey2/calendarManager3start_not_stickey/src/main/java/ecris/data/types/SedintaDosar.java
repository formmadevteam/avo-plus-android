package ecris.data.types;

import java.util.Calendar;
public class SedintaDosar {
	private String numar;
	private String numarVechi;
	private Calendar data;
	private String ora;
	private String categorieCaz;
	private String stadiuProcesual;
	public String getNumar() {
		return numar;
	}
	public void setNumar(String numar) {
		this.numar = numar;
	}
	public String getNumarVechi() {
		return numarVechi;
	}
	public void setNumarVechi(String numarVechi) {
		this.numarVechi = numarVechi;
	}
	
	public String getOra() {
		return ora;
	}
	public void setOra(String ora) {
		this.ora = ora;
	}
	public String getCategorieCaz() {
		return categorieCaz;
	}
	public void setCategorieCaz(String categorieCaz) {
		this.categorieCaz = categorieCaz;
	}
	public String getStadiuProcesual() {
		return stadiuProcesual;
	}
	public void setStadiuProcesual(String stadiuProcesual) {
		this.stadiuProcesual = stadiuProcesual;
	}
	public Calendar getData() {
		return data;
	}
	public void setData(Calendar data) {
		this.data = data;
	}	
}
