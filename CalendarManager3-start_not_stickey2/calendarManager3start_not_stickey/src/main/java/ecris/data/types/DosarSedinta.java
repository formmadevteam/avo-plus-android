package ecris.data.types;

import java.util.Calendar;

public class DosarSedinta {
	private String complet;
	private Calendar data;
	private String ora;
	private String solutie;
	private String solutieSumar;
	private Calendar dataPronuntare;
	private String documentSedinta;
	private String numarDocument;
	private Calendar dataDocument;
	
	public String getComplet() {
		return complet;
	}
	public void setComplet(String complet) {
		this.complet = complet;
	}
	public String getOra() {
		return ora;
	}
	public void setOra(String ora) {
		this.ora = ora;
	}
	public String getSolutie() {
		return solutie;
	}
	public void setSolutie(String solutie) {
		this.solutie = solutie;
	}
	public String getSolutieSumar() {
		return solutieSumar;
	}
	public void setSolutieSumar(String solutieSumar) {
		this.solutieSumar = solutieSumar;
	}
	
	public String getDocumentSedinta() {
		return documentSedinta;
	}
	public void setDocumentSedinta(String documentSedinta) {
		this.documentSedinta = documentSedinta;
	}
	public String getNumarDocument() {
		return numarDocument;
	}
	public void setNumarDocument(String numarDocument) {
		this.numarDocument = numarDocument;
	}
	public Calendar getData() {
		return data;
	}
	public void setData(Calendar data) {
		this.data = data;
	}
	public Calendar getDataDocument() {
		return dataDocument;
	}
	public void setDataDocument(Calendar dataDocument) {
		this.dataDocument = dataDocument;
	}
	public Calendar getDataPronuntare() {
		return dataPronuntare;
	}
	public void setDataPronuntare(Calendar dataPronuntare) {
		this.dataPronuntare = dataPronuntare;
	}
}
